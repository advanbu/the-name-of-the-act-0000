<details><summary><B>Legislative History</B></summary>
<table>
  <tc>
    <th>Act ID</th>
    <th>Act No</th>
    <th>Act Year</th>
    <th>Act Nature</th>
    <th>Title</th>
    <th>Enactment Date</th>
    <th>Enforcement Date<sup><abbr title="Vide Notification No.L.3067, dated 24th February 1937, published in the Gazette of India, 1937, Pt I, p.303.">1</abbr></sup></th>
    <th>Publication</th>
  </tc>
  <tr>
    <td>Act 38 of 1982</td>
    <td>38</td>
    <td>1982</td>
    <td>Primary Act</td>
    <td><a href="url">The Payment of Wages Act, 1936</a></td>
    <td>00</td>
    <td><a href="url">28th March 1937</a></td>
    <td><abbr title="Vide Notification No.L.3067, dated 24th February 1937, published in the Gazette of India, 1937, Pt I, p.303."><a href="url">Notification</a></abbr></td>
  </tr>
</table>
</details>

# THE PAYMENT OF WAGES ACT, 1936
#### ACT NO. 4 OF 1936<sup><abbr title="Vide Notification No.L.3067, dated 24th February 1937, published in the Gazette of India, 1937, Pt I, p.303.">1</abbr></sup>
[23rd April,1936.]

#### ARRANGEMENT OF SECTIONS
**[Preamble]()**  
**[1. Short title, extent, commencement and application.](#1-short-title-extent-commencement-and-application-)**  
**[2. Definitions.](#2-definitions)**  
**[3. Responsibility for payment of wages.](#3-responsibility-for-payment-of-wages)**  
**[4. Fixation of wage-periods.]()**  
**[5. Time of payment of wages.]()**  
**[6. Wages to be paid in current coin or currency notes or by cheque or crediting in bank account.]()**  
**[7. Deductions which may be made from wages.]()**  
**[8. Fines.]()**  
**[9. Deductions for absence from duty.]()**  
**[10. Deductions for damage or loss.]()**  
**[11. Deductions for services rendered.]()**  
**[12. Deductions for recovery of advances.]()**  
**[12A. Deductions for recovery of loans.]()**  
**[13. Deductions for payments to co-operative societies and insurance schemes.]()**  
**[13A. Maintenance of registers and records.]()**  
**[14. Inspectors.]()**  
**[14A. Facilities to be afforded to Inspectors.]()**  
**[15. Claims arising out of deductions from wages or delay in payment of wages and penalty for malicious or vexatious claims.]()**  
**[16. Single application in respect of claims from unpaid group.]()**  
**[17. Appeal.]()**  
**[17A. Conditional attachment of property of employer or other person responsible for payment of wages.]()**  
**[18. Powers of authorities appointed under section 15.]()**  
**[19. [Omitted.].]()**  
**[20. Penalty for offences under the Act.]()**  
**[21. Procedure in trial of offences.]()**  
**[22. Bar of Suits.]()**  
**[22A. Protection of action taken in good faith.]()**  
**[23. Contracting out.]()**  
**[24. Delegation of powers.]()**  
**[25. Display by notice of abstracts of the Act.]()**   
**[25A. Payment of undisbursed wages in cases of death of employed person.]()**  
**[26. Rule-making power.]()**  

---
_[TOP](#the-payment-of-wages-act-1936)_

#### **Preamble**
An Act to regulate the payment of wages of certain classes to <sup>2</sup>[employed persons].

WHEREAS it is expedient to regulate the payment of wages to certain classes of <sup>2</sup>[employed persons].

It is hereby enacted as follows:—

#### **1. Short title, extent, commencement and application. —** 

( 1 ) This Act may be called the Payment of Wages Act, 1936.

<sup>3</sup>[( 2 ) It extends to the whole of India <sup>4</sup> **----**].

( 3 ) It shall come into force on such date<sup>5</sup> as the Central Government may, by notification in the Official Gazette, appoint.

( 4 ) It applies in the first instance to the payment of wages to persons employed in any <sup>6</sup>[factory, to persons] employed (otherwise than in a factory) upon any railway by a railway administration or, either directly or through a sub-contractor, by a person fulfilling a contract with a railway administration <sup>7</sup>[,and to persons employed in an industrial or other establishment specified in sub-clauses (a) to (g) of clause (ii) of section 2].

( 5 ) <sup>8</sup>[Appropriate Government] may, after giving three months’ notice of its intention of so doing, by notification in the Official Gazette, extend the provisions of <sup>9</sup> [this Act] or any of them to the payment of wages to any class of persons employed in 10 [any establishment or class of establishments specified by <sup>11</sup>[appropriate Government] under sub-clause (h) of clause (ii) of section 2]:

<sup>12</sup>[Provided that in relation to any such establishment owned by the Central Government no such notification shall be issued except with the concurrence of that Government.]

<sup>13</sup>( 6 ) This Act applies to wages payable to an employed person in respect of a wage period if such wages for that wage period do not exceed <sup>14</sup>[twenty four thousand rupees] per month or such other higher sum which, on the basis of figures of the Consumer Expenditure Survey published by the National Sample Survey Organisation, the Central Government may, after every five years, by notification in the Official Gazette, specify.]

_[TOP](#the-payment-of-wages-act-1936)_

<details><summary>See Section 1 - Footnotes</summary>

1. Subs. by the A.O. 1950, for sub-section (2).
2. The words "except the State of Jammu and Kashmir" omitted by Act 51 of 1970, s. 2 and the Schedule (w.e.f. 1-9-1971).
3. 28th March 1937, see Gazette of India, 1937, Pt. I, pg. 626.
4. Subs. by Act 38 of 1982, s. 3, for "factory and to persons" (w.e.f. 15-10-1982).
5. Ins. by s. 3, ibid. (w.e.f. 15-10-1982).
6. Subs. by Act 41 of 2005, s. 3, for "the State Government" (w.e.f. 9-11-2005).
7. Subs. by Act 68 of 1957, s. 2, for "the Act" (w.e.f. 1-4-1958.)
8. Subs. by Act 38 of 1982, s. 3, for "any industrial establishment or in any class or group of industrial establishments" (w.e.f. 15-10-1982).
9. Subs. by Act 41 of 2005, s. 3, for "the Central Government or a State Government" (w.e.f. 9-11-2005).
10. Subs. by Act 38 of 1982, s. 3, for the proviso (w.e.f. 15-10-1982).
11. Subs. by Act 41 of 2005, s. 2, for sub-section (6) (w.e.f. 9-11-2005).
12. Subs. by S.O. 2806 (E), dated 28-8-2017, for "eighteen thousand rupees", see Gazette of India, Extraordinary, Part II, s. 3(ii). </details>
---

#### **2. Definitions**
In this Act, unless there is anything repugnant in the subject or context,--

<sup>1</sup>[(i) **"appropriate Government"** means, in relation to railways, air transport services, mines and oilfields, the Central Government and, in relation to all other cases, the State Government;]

<sup>2</sup>[<sup>3</sup>[(ia)] **"employed person"** includes the legal representative of a deceased employed person;

<sup>3</sup>[(ib)] **"employer"** includes the legal representative of a deceased employer;

<sup>3</sup>[(ic)] **"factory"** means a factory as defined in clause (m) of section 2 of the Factories Act, 1948 (63 of 1948), and includes any place to which the provisions of that Act have been applied under sub-section (1) of section 85 thereof;]

(ii) <sup>4</sup>[**"industrial or other establishment"** means] any--

<sup>5</sup>[(a) tramway service, or motor transport service engaged in carrying passenger or goods or both by road for hire or reward;

(aa) air transport service other than such service belonging to, or exclusively employed in the military, naval or air forces of the Union or the Civil Aviation Department of the Government of India;]

(b) dock, wharf or jetty;

<sup>6</sup>[(c) inland vessel, mechanically propelled;]

(d) mine, quarry or oilfield;

(e) plantation;

(f) workshop or other establishment in which articles are produced, adapted or manufactured, with a view to their use, transport or sale;

<sup>7</sup>[(g) establishment in which any work relating to the construction, development or maintenance of buildings, roads, bridges or canals, or relating to operation connected with navigation, irrigation, development or maintenance of buildings, roads, bridges or mission and distribution of electricity or any other form of power is being carried on;]

<sup>8</sup>[(h) any other establishment or class of establishment which <sup>9</sup>[appropriate Government may, having regard to the nature thereof, the need for protection of persons employed therein and other relevant circumstances, specify, by notification in the Official Gazette;]

<sup>10</sup>[(iia) **"mine"** has the meaning assigned to it in clause (j) of sub-section (1) of section 2 of the Mines Act, 1952 (35 of 1952);]

<sup>11</sup>[(iii) **"plantation"** has the meaning assigned to it in clause (f) of section 2 of the Plantations Labour Act, 1951 (69 of 1951)]

(iv) **"prescribed"** means prescribed by rules made under this Act;

<sup>12</sup>["(v) railway administration has the meaning assigned to it in clause (32) of section 2 of the Railways Act, 1989 (24 of 1989)]

<sup>13</sup>[(vi) **"wages"** means all remuneration (whether by way of salary, allowances, or otherwise) expressed in terms of money or capable of being so expressed which would, if the terms of employment, express or implied, were fulfilled, be payable to a person employed in respect of his employment or of work done in such employment, 

and includes--

(a) any remuneration payable under any award or settlement between the parties or order of a Court;

(b) any remuneration to which the person employed is entitled in respect of overtime work or holidays or any leave period;

(c) any additional remuneration payable under the terms of employment (whether called a bonus or by any other name);

(d) any sum which by reason of the termination of employment of the person employed is payable under any law, contract or instrument which provides for the payment of such sum, whether with or without deductions, but does not provide for the time within which the payment is to be made;

(e) any sum to which the person employed is entitled under any scheme framed under any law for the time being in force, 

but does not include--

(1) any bonus (whether under a scheme of profit sharing or otherwise) which does not form part of the remuneration payable under the terms of employment or which is not payable under any award or settlement between the parties or order of a Court;

(2) the value of any house-accommodation, or of the supply of light, water, medical attendance or other amenity or of any service excluded from the computation of wages by a general or special order of <sup>14</sup>[appropriate Government];

(3) any contribution paid by the employer to any pension or provident fund, and the interest which may have accrued thereon;

(4) any travelling allowance or the value of any travelling concession;

(5) any sum paid to the employed person to defray special expenses entailed on him by the nature of his employment; or

(6) any gratuity payable on the termination of employment in cases other than those specified in sub-clause (d).]

**STATE AMENDMENT**

Maharashtra.--

Amendment of section 2 of Act IV of 1936.--In section 2 of the Payment of Wages Act, 1936, in the its application to the State of Bombay (hereinafter referred to as the said Act),--  
(a) after clause (ii) the following new clause shall be inserted, namely:--
"(iia) legal representative means the person who in law represents the estate of a decrease employed person;  
(b) for clause (iii) the following shall be substituted, namely:--  
"(iii) plantation means--  
(a) any estate which is maintained for the purpose of growing cinchona rubber, coffee or tea, or
(b) any farm which is maintained for the purpose of growing sugarcane and attached to a factory established or maintained for the manufacture of sugar:
Provide that on such, estate or farm twenty-five or more persons are engaged for the purpose;"  
[Vide Bombay Act XLVIII of 1955, s. 2]

_[TOP](#the-payment-of-wages-act-1936)_

<details><summary>See Section 2 - Footnotes</summary>

1. Ins. by s. 4, ibid. (w.e.f. 9-11-2005).
2. Subs. by Act 53 of 1964, s. 3, for clause (i) (w.e.f. 1-2-1965).
3. Clauses (i), (ia) and (ib) renumbered as clauses (ia), (ib) and (ic) thereof by Act 41 of 2005, s. 4 (w.e.f. 9-11-2005).
4. Subs. by Act 38 of 1982, s. 4, for "industrial establishment" means" (w.e.f. 15-10-1982).
5. Subs. by Act 53 of 1964, s. 3, for sub-clause (a) (w.e.f. 1-2-1965).
6. Subs. by Act 68 of 1957, s. 3, for item (c) (w.e.f. 1-4-1958.)
7. Ins. by s. 3, ibid. (w.e.f. 1-4-1958.)
8. Ins. by Act 38 of 1982, s. 4 (w.e.f. 15-10-1982).
9. Subs. by Act 41 of 2005, s. 3, for "the Central Government or a State Government" (w.e.f. 9-11-2005).
10. Ins. by Act 53 of 1964, s. 3 (w.e.f. 1-2-1965).
11. Subs. by s. 3, ibid., for clause (iii) (w.e.f. 1-2-1965).
12. Subs. by Act 41 of 2005, s. 4, for clause (v) (w.e.f. 9-11-2005).
13. Subs. by Act 68 of 1957, s. 3, for clause (vi) (w.e.f. 1-4-1958.)
14. Subs. by Act 41 of 2005, s. 3, for "the State Government" (w.e.f. 9-11-2005).
</details>

---
#### **3.   Responsibility for payment of wages.**

<sup>1</sup>[3. Responsibility for payment of wages. --

(1) Every employer shall be responsible for the payment of all wages required to be paid under this Act to persons employed by him and in case of persons employed,-- 

(a) in factories, if a person has been named as the manager of the factory under clause (f) of sub-section (1) of section 7 of the Factories Act, 1948 (63 of 1948);

(b) in industrial or other establishments, if there is a person responsible to the employer for the supervision and control of the industrial or other establishments;

(c) upon railways (other than in factories), if the employer is the railway administration and the railway administration has nominated a person in this behalf for the local area concerned; (d) in the case of contractor, a person designated by such contractor who is directly under his charge; and

(e) in any other case, a person designated by the employer as a person responsible for complying with the provisions of the Act, the person so named, the person responsible to the employer, the person so nominated or the person so designated, as the case may be, shall be responsible for such payment.

(2) Notwithstanding anything contained in sub-section (1), it shall be the responsibility of the employer to make payment of all wages required to be made under this Act in case the contractor or the person designated by the employer fails to make such payment.]

**STATE AMENDMENT**

Maharashtra.--

Amendment of section 3 of Act IV of 1936.--In section 3 of the payment of Wages Act, 1936 (hereinafter referred to as the said Act), for the proviso the following shall be substituted, namely:--

"Provided that, in the case of persons employed (otherwise than by a contractor)--

(a) in factories, if a person has been named as the manager of the factory under clause (f) of subsection (1) of section 7 of the Factories Act, 1948 (LXIII of 1948), then the person so named and the employed jointly and severally;

(b) in industrial establishment, if there is a person responsibility to the employer for the supervision and control of the industrial establishment, then the person so responsible and the employer jointly and severally;

(c) upon railways (otherwise than in factories), if the employer is the railway administration and the railway administration has nominated a person in this behalf for the local area concerned, then the person so nominated; Shall be responsible for such payment."
[Vide Bombay Act LXII of 1953, s. 2]

_[TOP](#the-payment-of-wages-act-1936)_

<details><summary>See Section 3 - Footnotes</summary>

1. Subs. by s. 5, ibid., for section 3 (w.e.f. 9-11-2005).
</details>

---



