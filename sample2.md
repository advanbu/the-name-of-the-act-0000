Employees' Compensation Act, 1923
=================================

ACTNO. 8 OF 1923

03 May, 1923

An Act to provide for the payment by certain classes of employers to their 4\[Employees\] of compensation for injury by accident

Whereas it is expedient to provide for the payment by certain classes of employers to their [5](#note_5 "5. Subs. for  workmen  by Act 45 of 2009, S. 3 (w.e.f. 18-1-2010).")\[employees\] of compensation for injury by accident; It is hereby enacted as follows:

#### **statement of objects and reasons of amendment act 11 of 2017**

The Employee's Compensation Act, 1923, provides for payment of compensation to the employees and their dependants in the case of injury by industrial accidents including certain occupational diseases arising out of and in the course of employment resulting in death or disablement.

2\. The Law Commission of India, in its 62nd Report of 1974 and 134th Report of 1989, recommended to review or amend or repeal various provisions of the Employee's Compensation Act, 1923. Some recommendations made by the Law Commission of India have already been implemented.

3\. Now, it has been decided to make the following further amendments to the Employee's Compensation Act, 1923, based on the other recommendations of the Law Commission contained in the above reports, namely

(_a_) making it obligatory on the employer to inform the employee of his rights to compensation under the Act, in writing as well as through electronic means;

(_b_) to enhance the penalty amount for various violations under the Act from the existing amount of five thousand rupees to fifty thousand rupees which may be extended to one lakh rupees;

(_c_) to make the employer liable to penalty for failure to inform the employee of his rights to compensation under the Act;

(_d_) to revise the minimum amount involved in the dispute for which appeal can be filed to the High Court, from the existing three hundred rupees to ten thousand rupees or such higher amount as the Central Government may, by notification, specify;

(_e_) to omit Section 30-A of the Act which empowers the Commissioner to withhold payment to an employee of any sum in deposit with him where an appeal is filed in the High Court by an employer. This omission will provide relief to the employees as the amount can now be withheld only when there is a stay or order to that effect by the High Court in cases where the appeal has been filed by the employer.

4\. The Bill seeks to achieve the above objects.

**Note** **Introduction**. The growing complexity of industry in this country, with the increasing use of machinery and consequent danger to workmen, along with the comparative poverty of the workmen themselves renders it advisable that they should be protected as far as possible, from hardship arising from accidents. A legislation of this kind helps to reduce the number of accidents in a manner that cannot be achieved by official inspection, and to mitigate the effect of accidents by provision for suitable medical treatment, thereby making industry more attractive to labour and increasing its efficiency. The Act provides for cheaper and quicker disposal of disputes relating to compensation through special tribunals than possible under the civil law. .

Chapter I

PRELIMINARY

Section 1. Short title, extent and commencement
-----------------------------------------------

(1) This Act may be called the [6](#note_6 "6. Subs. for  workmen's  by Act 45 of 2009, S. 4 (w.e.f. 18-1-2010).")\[Employees'\] Compensation Act, 1923.

[7](#note_7 "7. Subs. by the A.O. 1950, for the former sub-section.")\[(2) It extends to the whole of India [8](#note_8 "8. The words  except the State of Jammu & Kashmir  omitted by Act 51 of 1970.")\[\* \* \*\].

(3) It shall come into force on the first day of July, 1924.

**Notes** (1) The application of the Act has been extended to the State of Sikkim w.e.f 1-11-1986 _vide_ Noti. No. S.O. 803(E), dated 30-10-1986 (1987 CCL-III-211).

(2) In its application to Apprentices the Act is modified as per the Schedule read with S. 16 of the Apprentices Act, 1961. _See_ the Schedule to Apprentices Act, 1961.

Section 2. Definitions
----------------------

(1) In this Act unless there is anything repugnant in the subject or context

(_a_) [9](#note_9 "9. Cl. (a) omitted by Act 8 of 1959, S. 2 (w.e.f. 1-6-1959).")\[\* \* \*\]

(_b_) Commissioner means a Commissioner for [10](#note_10 "10. Subs. for  workmen's  by Act 45 of 2009, S. 5 (w.e.f. 18-1-2010).")\[Employees'\] Compensation appointed under Section 20;

(_c_) compensation means compensation as provided for by this Act;

[11](#note_11 "11. Subs. by Act 8 of 1959, S. 2, for the former cl. (w.e.f. 1-6-1959).")\[(_d_) dependant means any of the following relatives of a deceased [12](#note_12 "12. Subs. for  workman  by Act 45 of 2009, S. 5 (w.e.f. 18-1-2010).")\[employee\], namely

(_i_) a widow, a minor [13](#note_13 "13. Subs. by Act 30 of 1995 (w.e.f. 15-9-1995).")\[legitimate or adopted\] son, an unmarried [14](#note_14 "14. Subs. by Act 30 of 1995 (w.e.f. 15-9-1995).")\[legitimate or adopted\] daughter, or a widowed mother; and

(_ii_) if wholly dependent on the earnings of the [15](#note_15 "15. Subs. for  workman  by Act 45 of 2009, S. 5 (w.e.f. 18-1-2010).")\[employee\] at the time of his death, a son or a daughter who has attained the age of 18 years and who is infirm;

(_iii_) if wholly or in part dependent on the earnings of the [16](#note_16 "16. Subs. for  workman  by Act 45 of 2009, S. 5 (w.e.f. 18-1-2010).")\[employee\] at the time of his death

(_a_) a widower,

(_b_) a parent other than a widowed mother,

(_c_) a minor illegitimate son, an unmarried illegitimate daughter or a daughter [17](#note_17 "17. Subs. by Act 30 of 1995 (w.e.f. 15-9-1995).")\[legitimate or illegitimate or adopted\] if married and a minor or if widowed and a minor,

(_d_) a minor brother or an unmarried sister or a widowed sister if a minor,

(_e_) a widowed daughter-in-law,

(_f_) a minor child of a pre-deceased son,

(_g_) a minor child of a pre-deceased daughter where no parent of the child is alive, or

(_h_) a paternal grandparent if no parent of the [18](#note_18 "18. Subs. for  workman  by Act 45 of 2009, S. 5 (w.e.f. 18-1-2010).")\[employee\] is alive\];

[19](#note_19 "19. Ins. by Act 30 of 1995 (w.e.f. 15-9-1995).")\[_Explanation_. For the purposes of sub-clause (_ii_) and items (_f_) and (_g_) of sub-clause (_iii_), references to a son, daughter or child include an adopted son, daughter or child respectively\];

[20](#note_20 "20. Ins. by Act 45 of 2009, S. 6 (w.e.f. 18-1-2010).")\[(_dd_) employee means a person, who is

(_i_) a railway servant as defined in clause (34) of Section 2 of the Railways Act, 1989 (24 of 1989), not permanently employed in any administrative district or sub-divisional office of a railway and not employed in any such capacity as is specified in Schedule II; or

(_ii_) (_a_) a master, seaman or other member of the crew of a ship,

(_b_) a captain or other member of the crew of an aircraft,

(_c_) a person recruited as driver, helper, mechanic, cleaner or in any other capacity in connection with a motor vehicle,

(_d_) a person recruited for work abroad by a company,

and who is employed outside India in any such capacity as is specified in Schedule II and the ship, aircraft or motor vehicle, or company, as the case may be, is registered in India; or

(_iii_) employed in any such capacity as is specified in Schedule II, whether the contract of employment was made before or after the passing of this Act and whether such contract is expressed or implied, oral or in writing; but does not include any person working in the capacity of a member of the Armed Forces of the Union; and any reference to any employee who has been injured shall, where the employee is dead, include a reference to his dependants or any of them;\]

(_e_) employer includes any body of persons whether incorporated or not and any managing agent of an employer and the legal representative of a deceased employer, and, when the services of a [21](#note_21 "21. Subs. for  workman  by Act 45 of 2009, S. 5 (w.e.f. 18-1-2010).")\[employee\] are temporarily lent or let on hire to another person by the person with whom the [22](#note_22 "22. Subs. for  workman  by Act 45 of 2009, S. 5 (w.e.f. 18-1-2010).")\[employee\] has entered into a contract of service or apprenticeship, means such other person while the [23](#note_23 "23. Subs. for  workman  by Act 45 of 2009, S. 5 (w.e.f. 18-1-2010).")\[employee\] is working for him;

(_f_) managing agent means any person appointed or acting as the representative of another person for the purpose of carrying on such other person's trade or business, but does not include an individual manager subordinate to an employer;

[24](#note_24 "24. Ins. by Act 8 of 1959, S. 2 (w.e.f. 1-6-1959).")\[(_ff_) minor means a person who has not attained the age of 18 years;\]

(_g_) partial disablement means, where the disablement is of a temporary nature, such disablement as reduces the earning capacity of a [25](#note_25 "25. Subs. for  workman  by Act 45 of 2009, S. 5 (w.e.f. 18-1-2010).")\[employee\] in any employment in which he was engaged at the time of the accident resulting in the disablement, and, where the disablement is of a permanent nature, such disablement as reduces his earning capacity in every employment which he was capable of undertaking at that time: provided that every injury specified [26](#note_26 "26. Subs. by Act 64 of 1962, S. 2 (w.e.f. 1-2-1963).")\[in Part II of Schedule I\] shall be deemed to result in permanent partial disablement;

(_h_) prescribed means prescribed by rules made under this Act;

(_i_) qualified medical practitioner means any person registered [27](#note_27 "27. Certain words omitted by Act 8 of 1959, S. 2 (w.e.f. 1-6-1959).")\[\* \* \*\] under any [28](#note_28 "28. Subs. by the A.O. 1950, for the words  Act of the Central Legislature or of any Legislature in a Province .")\[Central Act, Provincial Act or an Act of the Legislature of a [29](#note_29 "29. Subs. by the A.O. (No. 3) 1956, for  Part A State or Part B State .")\[State\]\] providing for the maintenance of a register of medical practitioners, or, in any area where no such last-mentioned Act is in force, any person declared by the State Government, by notification in the Official Gazette, to be a qualified medical practitioner for the purposes of this Act;

(_j_) [30](#note_30 "30. Cl. (j) omitted by Act 15 of 1933, S. 2.")\[\* \* \*\]

(_k_) seaman means any person forming part of the crew of any [31](#note_31 "31. The word  registered  omitted by Act 15 of 1933, S. 2.")\[\* \* \*\] ship, but does not include the master of [32](#note_32 "32. Subs. by Act 15 of 1933, S. 2, for  any such .")\[the\] ship;

(_l_) total disablement means such disablement, whether of a temporary or permanent nature, as incapacitates a [33](#note_33 "33. Subs. for  workman  by Act 45 of 2009, S. 5 (w.e.f. 18-1-2010).")\[employee\] for all work which he was capable of performing at the time of the accident resulting in such disablement:

[34](#note_34 "34. Subs. by Act 64 of 1962, S. 2 (w.e.f. 1-2-1963).")\[Provided that permanent total disablement shall be deemed to result from every injury specified in Part I of Schedule I or from any combination of injuries specified in Part II thereof where the aggregate percentage of the loss of earning capacity, as specified in the said Part II against those injuries, amounts to one hundred per cent or more\];

(_m_) wages includes any privilege or benefit which is capable of being estimated in money, other than a travelling allowance or the value of any travelling concession or a contribution paid by the employer of a [35](#note_35 "35. Subs. for  workman  by Act 45 of 2009, S. 5 (w.e.f. 18-1-2010).")\[employee\] towards any pension or provident fund or a sum paid to a [36](#note_36 "36. Subs. for  workman  by Act 45 of 2009, S. 5 (w.e.f. 18-1-2010).")\[employee\] to cover any special expenses entailed on him by the nature of his employment;

(_n_) [37](#note_37 "37. Clause (n) omitted by Act 45 of 2009, S. 6 (w.e.f. 18-1-2010). Prior to omission it read as: (n)  workman  means any person who is (i) a railway servant as defined in clause (34) of Section 2 of the Railways Act, 1989 (24 of 1989), not permanently employed in any administrative, district or sub-divisional office of a railway and not employed in any such capacity as is specified in Schedule II, or(i-a)(a) a master, seaman or other member of the crew of a ship,(b) a captain or other member of the crew of an aircraft,(c) a person recruited as driver, helper, mechanic, cleaner or in any other capacity in connection with a motor vehicle,(d) a person recruited for work abroad by a company,and who is employed outside India in any such capacity as is specified in Schedule II and the ship, aircraft or motor vehicle, or company, as the case may be, is registered in India, or(ii) employed in any such capacity as is specified in Schedule II,whether the contract of employment was made before or after the passing of this Act and whether such contract is expressed or implied, oral or in writing; but does not include any person working in the capacity of a member of the Armed Forces of the Union and any reference to a workman who has been injured shall, where the workman is dead, include a reference to his dependants or any of them. .")\[\* \* \*\]

(2) The exercise and performance of the powers and duties of a local authority or of any department [38](#note_38 "38. Subs. by the A.O. 1937, for  of the Government .")\[acting on behalf of the Government\] shall, for the purposes of this Act, unless a contrary intention appears, be deemed to be the trade or business of such authority or department.

[39](#note_39 "39. Subs. by Act 30 of 1995 (w.e.f. 15-9-1995).")\[(3) The Central Government or the State Government, by notification in the Official Gazette, after giving not less than three months' notice of its intention so to do, may, by a like notification, add to Schedule II any class of persons employed in any occupation which it is satisfied is a hazardous occupation, and the provisions of this Act shall thereupon apply, in case of a notification by the Central Government, within the territories to which the Act extends, or, in the case of a notification by the State Government, within the State, to such classes of persons:

Provided that in making addition, the Central Government or the State Government, as the case may be, may direct that the provisions of this Act shall apply to such classes of persons in respect of specified injuries only.\]

STATE AMENDMENTS

**West Bengal**. In its application to the State of West Bengal, after clause (_f_) of sub-section (1) of Section 2 of the said Act the following clause shall be _inserted_, namely:

(_ff_) medical referee means a qualified medical practitioner appointed under Section 24-A as a medical referee for the purposes of this Act; _Vide_ Bengal Act 6 of 1942, S. 3.

Chapter II

[40](#note_40 "40. Subs. for  workmen's  by Act 45 of 2009, S. 5 (w.e.f. 18-1-2010).")\[EMPLOYEES'\] COMPENSATION

Section 3. Employer's liability for compensation
------------------------------------------------

(1) If personal injury is caused to a [41](#note_41 "41. Subs. for  workman  by Act 45 of 2009, S. 5 (w.e.f. 18-1-2010).")\[employee\] by accident arising out of and in the course of his employment, his employer shall be liable to pay compensation in accordance with the provisions of this Chapter:

Provided that the employer shall not be so liable

(_a_) in respect of any injury which does not result in the total or partial disablement of the [42](#note_42 "42. Subs. for  workman  by Act 45 of 2009, S. 5 (w.e.f. 18-1-2010).")\[employee\] for a period exceeding [43](#note_43 "43. Subs. by Act 8 of 1959, S. 3 for  seven  (w.e.f. 1-6-1959).")\[three\] days;

(_b_) in respect of any [44](#note_44 "44. Subs. by Act 15 of 1933, S. 3 for  injury to a workman resulting from .")\[injury, not resulting in death [45](#note_45 "45. Ins. by Act 30 of 1995, S. 3 (w.e.f. 15-9-1995).")\[or permanent total disablement\], caused by\] an accident which is directly attributed to

(_i_) the [46](#note_46 "46. Subs. for  workman  by Act 45 of 2009, S. 5 (w.e.f. 18-1-2010).")\[employee\] having been at the time therefore under the influence of drink of drugs, or

(_ii_) the wilful disobedience of the [47](#note_47 "47. Subs. for  workman  by Act 45 of 2009, S. 5 (w.e.f. 18-1-2010).")\[employee\] to an order expressly given, or to a rule expressly framed, for the purpose of securing the safety of [48](#note_48 "48. Subs. for  workman  by Act 45 of 2009, S. 5 (w.e.f. 18-1-2010).")\[employee\], or

(_iii_) the wilful removal or disregard by the [49](#note_49 "49. Subs. for  workman  by Act 45 of 2009, S. 5 (w.e.f. 18-1-2010).")\[employee\] of any safety guard or other device which he knew to have been provided for the purpose of securing the safety of [50](#note_50 "50. Subs. for  workman  by Act 45 of 2009, S. 5 (w.e.f. 18-1-2010).")\[employees\] [51](#note_51 "51. The word  or  omitted by Act 5 of 1929, S. 2.")\[\* \* \*\];

[52](#note_52 "52. Cl. (c) omitted by Act 5 of 1929, S. 2.")\[\* \* \*\]

[53](#note_53 "53. Sub-ss. (2) and (3) subs. by Act 8 of 1959, S. 3 (w.e.f. 1-6-1959).")\[(2) If a [54](#note_54 "54. Subs. for  workman  by Act 45 of 2009, S. 5 (w.e.f. 18-1-2010).")\[employee\] employed in any employment specified in Part A of Schedule III contracts any disease specified therein as an occupational disease peculiar to that employment, or if a [55](#note_55 "55. Subs. for  workman  by Act 45 of 2009, S. 5 (w.e.f. 18-1-2010).")\[employee\], whilst in the service of an employer in whose service he has been employed for a continuous period of not less than six months (which period shall not include a period of service under any other employer in the same kind of employment) in any employment specified in Part B of Schedule III, contracts any disease specified therein as an occupational disease peculiar to that employment, or if a [56](#note_56 "56. Subs. for  workman  by Act 45 of 2009, S. 5 (w.e.f. 18-1-2010).")\[employee\] whilst in the service of one of more employers in any employment specified in Part C of Schedule III for such continuous period as the Central Government may specify in respect of each such employment, contracts any disease specified therein as an occupational disease peculiar to that employment, the contracting of the disease shall be deemed to be an injury by accident within the meaning of this section and, unless the contrary is proved, the accident shall be deemed to have arisen out of, and in the course of, the employment:

[57](#note_57 "57. Added by Act 64 of 1962, S. 3 (w.e.f 1-2-1963).")\[Provided that if it is proved

(_a_) that an [58](#note_58 "58. Subs. for  workman  by Act 45 of 2009, S. 5 (w.e.f. 18-1-2010).")\[employee\] whilst in the service of one or more employers in any employment specified in Part C of Schedule III has contracted a disease specified therein as an occupational disease peculiar to that employment during a continuous period which is less than the period specified under this sub-section for that employment, and

(_b_) that the disease has arisen out of and in the course of the employment;

the contracting of such disease shall be deemed to be an injury by accident within the meaning of this section:

Provided further that if it is proved that a [59](#note_59 "59. Subs. for  workman  by Act 45 of 2009, S. 5 (w.e.f. 18-1-2010).")\[employee\] who having served under any employer in any employment specified in Part B of Schedule III or who having served under one or more employers in any employment specified in Part C of that Schedule, for a continuous period specified under this sub-section for that employment and he has after the cessation of such service contracted any disease specified in the said Part B or the said Part C, as the case may be, as an occupational disease peculiar to the employment and that such disease arose out of the employment, the contracting of the disease shall be deemed to be an injury by accident within the meaning of this section\].

[60](#note_60 "60. Subs. by Act 64 of 1962, S. 3 (w.e.f. 1-2-1963).")\[(2-A) If a [61](#note_61 "61. Subs. for  workman  by Act 45 of 2009, S. 5 (w.e.f. 18-1-2010).")\[employee\] employed in any employment specified in Part C of Schedule III contracts any occupational disease peculiar to that employment, the contracting whereof is deemed to be an injury by accident within the meaning of this section, and such employment was under more than one employer, all such employers shall be liable for the payment of the compensation in such proportion as the Commissioner may, in the circumstances, deem just.\]

(3) [62](#note_62 "62. Subs. by Act 30 of 1995, S. 3 (w.e.f. 15-9-1995).")\[The Central Government or the State Government\], after giving, by notification in the Official Gazette, not less than three months' notice of its intention so to do, may, by a like notification, add any description of employment to the employments specified in Schedule III, and shall specify in the case of employments so added the diseases which shall be deemed for the purposes of this section to be occupational diseases peculiar to those employments respectively and thereupon the provisions of sub-section (2) shall apply [63](#note_63 "63. Omitted by Act 51 of 1970, S. 2.")\[\* \* \*\] [64](#note_64 "64. Ins. by Act 30 of 1995, S. 3 (w.e.f. 15-9-1995).")\[, in the case of a notification by the Central Government, within the territories to which this Act extends or, in case of a notification by the State Government, within the State\] as if such diseases had been declared by this Act to be occupational diseases peculiar to those employments.\]

(4) Save as provided by [65](#note_65 "65. Subs. by Act 8 of 1959, S. 3 for  sub-section (2)  (w.e.f. 1-6-1959).")\[sub-sections (2), (2-A)\] and (3), no compensation shall be payable to a [66](#note_66 "66. Subs. for  workman  by Act 45 of 2009, S. 5 (w.e.f. 18-1-2010).")\[employee\] in respect of any disease unless the disease is [67](#note_67 "67. The words  solely and  omitted by Act 15 of 1933, S. 3.")\[\* \* \*\] directly attributable to a specific injury by accident arising out of and in the course of his employment.

(5) Nothing herein contained shall be deemed to confer any right to compensation on a [68](#note_68 "68. Subs. for  workman  by Act 45 of 2009, S. 5 (w.e.f. 18-1-2010).")\[employee\] in respect of any injury if he has instituted in a Civil Court a suit for damages in respect of the injury against the employer or any other person; and no suit for damages shall be maintainable by a [69](#note_69 "69. Subs. for  workman  by Act 45 of 2009, S. 5 (w.e.f. 18-1-2010).")\[employee\] in any court of law in respect of any injury

(_a_) if he has instituted a claim to compensation in respect of the injury before a Commissioner; or

(_b_) if an agreement has been come to between the [70](#note_70 "70. Subs. for  workman  by Act 45 of 2009, S. 5 (w.e.f. 18-1-2010).")\[employee\] and his employer providing for the payment of compensation in respect of the injury in accordance with the provisions of this Act.

Section 4. Amount of compensation
---------------------------------

[71](#note_71 "71. Subs. by Act 22 of 1984, S. 3 (w.e.f. 1-7-1984).")\[(1) Subject to the provisions of this Act, the amount of compensation shall be as follows, namely:

  

(_a_)

where death results from the injury

an amount equal to [72](#note_72 "72. Subs. by Act 30 of 1995, S. 4 (w.e.f. 15-9-1995).")\[fifty per cent\] of the monthly wages of the deceased [73](#note_73 "73. Subs. for  workman  by Act 45 of 2009, S. 5 (w.e.f. 18-1-2010).")\[employee\] multiplied by the relevant factor;

or

an amount of [74](#note_74 "74. Subs. for  eighty thousand rupees  by Act 45 of 2009, S. 7 (w.e.f. 18-1-2010).")\[one lakh and twenty thousand rupees\], whichever is more;

(_b_)

Where permanent total disablement results from the injury

An amount equal to [75](#note_75 "75. Subs. by Act 30 of 1995, S. 4 (w.e.f. 15-9-1995).")\[sixty per cent\] of the monthly wages of the injured [76](#note_76 "76. Subs. for  workman  by Act 45 of 2009, S. 5 (w.e.f. 18-1-2010).")\[employee\] multiplied by the relevant factor;

or

an amount of [77](#note_77 "77. Subs. for  ninety thousand rupees  by Act 45 of 2009, S. 7 (w.e.f. 18-1-2010).")\[one lakh and forty thousand rupees\], whichever is more:

[78](#note_78 "78. Ins. by Act 45 of 2009, S. 7 (w.e.f. 18-1-2010).")\[Provided that the Central Government may, by notification in the Official Gazette, from time to time, enhance the amount of compensation mentioned in clauses (_a_) and (_b_).\]

_Explanation I_. For the purposes of clause (_a_) and clause (_b_), relevant factor , in relation to a [79](#note_79 "79. Subs. for  workman  by Act 45 of 2009, S. 5 (w.e.f. 18-1-2010).")\[employee\] means the factor specified in the second column of Schedule IV against the entry in the first column of that Schedule specifying the number of years which are the same as the completed years of the age of the [80](#note_80 "80. Subs. for  workman  by Act 45 of 2009, S. 5 (w.e.f. 18-1-2010).")\[employee\] on his last birthday immediately preceding the date on which the compensation fell due;

_Explanation II_. [81](#note_81 "81. Omitted by Act 45 of 2009, S. 7 (w.e.f. 18-1-2010). Prior to omission it read as: Explanation II. Where the monthly wages of a workman exceed four thousand rupees, his monthly wages for the purposes of clause (a) and clause (b) shall be deemed to be four thousand rupees only; .")\[\* \* \*\]

  

(_c_)

Where permanent partial disablement results from the injury

(_i_) in the case of an injury specified in Part II of Schedule I, such percentage of the compensation which would have been payable in the case of permanent total disablement as is specified therein as being the percentage of the loss of earning capacity caused by that injury, and

(_ii_) in the case of an injury not specified in Schedule I, such percentage of the compensation payable in the case of permanent total disablement as is proportionate to the loss of earning capacity (as assessed by the qualified medical practitioner) permanently caused by the injury;

_Explanation I_. Where more injuries than one are caused by the same accident, the amount of compensation payable under this head shall be aggregated but not so in any case as to exceed the amount which would have been payable if permanent total disablement had resulted from the injuries;

_Explanation II_. In assessing the loss of earning capacity for the purposes of sub-clause (_ii_), the qualified medical practitioner shall have due regard to the percentages of loss of earning capacity in relation to different injuries specified in Schedule I;

  

(_d_)

Where temporary disablement, whether total or partial results from the injury

a half-monthly payment of the sum equivalent to twenty-five per cent of monthly wages of the [82](#note_82 "82. Subs. for  workman  by Act 45 of 2009, S. 5 (w.e.f. 18-1-2010).")\[employee\], to be paid in accordance with the provisions of sub-section (2).

[83](#note_83 "83. Ins. by Act 30 of 1995, S. 4 (w.e.f. 15-9-1995).")\[(1-A) Notwithstanding anything contained in sub-section (1), while fixing the amount of compensation payable to a [84](#note_84 "84. Subs. for  workman  by Act 45 of 2009, S. 5 (w.e.f. 18-1-2010).")\[employee\] in respect of an accident occurred outside India, the Commissioner shall take into account the amount of compensation, if any, awarded to such [85](#note_85 "85. Subs. for  workman  by Act 45 of 2009, S. 5 (w.e.f. 18-1-2010).")\[employee\] in accordance with the law of the country in which the accident occurred and shall reduce the amount fixed by him by the amount of compensation awarded to the [86](#note_86 "86. Subs. for  workman  by Act 45 of 2009, S. 5 (w.e.f. 18-1-2010).")\[employee\] in accordance with the law of that country.\];

[87](#note_87 "87. Ins. by Act 45 of 2009, S. 7 (w.e.f. 18-1-2010).")\[(1-B) The Central Government may, by notification[88](#note_88 "88. In exercise of the powers conferred by sub-section (1-B) of Section 4 of the Employee's Compensation Act, 1923 (8 of 1923), the Central Government hereby specifies, for the purposes of sub-section (1) of the said section, the following amount as monthly wages, with effect from the date of publication of this notification in the Official Gazette, namely  Eight thousand rupees  [Vide S.O. 1258(E), dated 31-5-2010 (w.e.f. 31-5-2010)].") in the Official Gazette, specify, for the purposes of sub-section (1), such monthly wages in relation to an employee as it may consider necessary.\]

(2) The half-monthly payment referred to in clause (_d_) of sub-section (1) shall be payable on the sixteenth day

(_i_) from the date of disablement where such disablement lasts for a period of twenty-eight days or more, or

(_ii_) after the expiry of a waiting period of three days from the date of disablement where such disablement lasts for a period of less than twenty-eight days; and thereafter half-monthly during the disablement or during a period of five years, whichever period is shorter:

Provided that

(_a_) there shall be deducted from any lump sum or half-monthly payments to which the [89](#note_89 "89. Subs. for  workman  by Act 45 of 2009, S. 5 (w.e.f. 18-1-2010).")\[employee\] is entitled the amount of any payment or allowance which the [90](#note_90 "90. Subs. for  workman  by Act 45 of 2009, S. 5 (w.e.f. 18-1-2010).")\[employee\] has received from the employer by way of compensation during the period of disablement prior to the receipt of such lump sum or of the first half-monthly payment, as the case may be; and

(_b_) no half-monthly payment shall in any case exceed the amount, if any, by which half the amount of the monthly wages of the [91](#note_91 "91. Subs. for  workman  by Act 45 of 2009, S. 5 (w.e.f. 18-1-2010).")\[employee\] before the accident exceeds half the amount of such wages which he is earning after the accident.

_Explanation_. Any payment or allowance which the [92](#note_92 "92. Subs. for  workman  by Act 45 of 2009, S. 5 (w.e.f. 18-1-2010).")\[employee\] has received from the employer towards his medical treatment shall not be deemed to be a payment or allowance received by him by way of compensation within the meaning of clause (_a_) of the proviso.

[93](#note_93 "93. Ins. by Act 45 of 2009, S. 7 (w.e.f. 18-1-2010).")\[(2-A) The employee shall be reimbursed the actual medical expenditure incurred by him for treatment of injuries caused during the course of employment.\]

(3) On the ceasing of the disablement before the date on which any half-monthly payment falls due there shall be payable in respect of that half-month a sum proportionate to the duration of the disablement in that half-month.\]

[94](#note_94 "94. Ins. by Act 30 of 1995, S. 4 (w.e.f. 15-9-1995).")\[(4) If the injury of the [95](#note_95 "95. Subs. for  workman  by Act 45 of 2009, S. 5 (w.e.f. 18-1-2010).")\[employee\] results in his death, the employer shall, in addition to the compensation under sub-section (1), deposit with the Commissioner a sum of [96](#note_96 "96. Subs. for  two thousand and five hundred rupees  by Act 45 of 2009, S. 7 (w.e.f. 18-1-2010).")\[not less than five thousand rupees\] for payment of the same to the eldest surviving dependant of the [97](#note_97 "97. Subs. for  workman  by Act 45 of 2009, S. 5 (w.e.f. 18-1-2010).")\[employee\] towards the expenditure of the funeral of such [98](#note_98 "98. Subs. for  workman  by Act 45 of 2009, S. 5 (w.e.f. 18-1-2010).")\[employee\] or where the [99](#note_99 "99. Subs. for  workman  by Act 45 of 2009, S. 5 (w.e.f. 18-1-2010).")\[employee\] did not have a dependant or was not living with his dependant at the time of his death to the person who actually incurred such expenditure.\]

[100](#note_100 "100. Ins. by Act 45 of 2009, S. 7 (w.e.f. 18-1-2010).")\[Provided that the Central Government may, by notification in the Official Gazette, from time to time, enhance the amount specified in this sub-section.\]

[101](#note_101 "101. Ins. by Act 8 of 1959, S. 5 (w.e.f. 1-6-1959).")\[**4-A. Compensation to be paid when due and penalty for default**. (1) Compensation under Section 4 shall be paid as soon as it falls due.

(2) In cases where the employer does not accept the liability for compensation to the extent claimed, he shall be bound to make provisional payment based on the extent of liability which he accepts, and, such payment shall be deposited with the Commissioner or made to the [102](#note_102 "102. Subs. for  workman  by Act 45 of 2009, S. 5 (w.e.f. 18-1-2010).")\[employee\], as the case may be, without prejudice to the right of the [103](#note_103 "103. Subs. for  workman  by Act 45 of 2009, S. 5 (w.e.f. 18-1-2010).")\[employee\] to make any further claim.

[104](#note_104 "104. Sub-ss. (3) and (3-A) subs. for sub-s. (3) by Act 30 of 1995, S. 5 (w.e.f. 15-9-1995).")\[(3) Where any employer is in default in paying the compensation due under this Act within one month from the date it fell due, the Commissioner shall

(_a_) direct that the employer shall, in addition to the amount of the arrears, pay simple interest thereon at the rate of twelve per cent per annum or at such higher rate not exceeding the maximum of the lending rates of any scheduled bank as may be specified by the Central Government, by notification in the Official Gazette, on the amount due; and

(_b_) if, in his opinion, there is no justification for the delay, direct that the employer shall, in addition to the amount of the arrears and interest thereon, pay a further sum not exceeding fifty per cent of such amount by way of penalty:

Provided that an order for the payment of penalty shall not be passed under clause (_b_) without giving a reasonable opportunity to the employer to show cause why it should not be passed.

_Explanation_. For the purposes of this sub-section, scheduled bank means a bank for the time being included in the Second Schedule to the Reserve Bank of India Act, 1934 (2 of 1934).

[105](#note_105 "105. Subs. for  The interest payable under sub-section (3) shall be paid to the workman or his dependant, as the case may be, and the penalty shall be credited to the State Government  by Act 46 of 2000, S. 4.")\[(3-A) The interest and the penalty payable under sub-section (3) shall be paid to the [106](#note_106 "106. Subs. for  workman  by Act 45 of 2009, S. 5 (w.e.f. 18-1-2010).")\[employee\] or his dependant, as the case may be.\]\]

Section 5. Method of calculating wages
--------------------------------------

[107](#note_107 "107. The bracket and figure  (1)  omitted by Act 9 of 1938, S. 4.")\[\* \* \*\] [108](#note_108 "108. Subs. by Act 13 of 1939, S. 2 for  For the purposes of this Act the monthly wages of a workman shall be calculated  (w.e.f. 30-6-1934).")\[In this Act and for the purposes thereof the expression monthly wages means the amount of wages deemed to be payable for a month's service (whether the wages are payable by the month or by whatever other period or at piece rates), and calculated\] as follows, namely

(_a_) where the [109](#note_109 "109. Subs. for  workman  by Act 45 of 2009, S. 5 (w.e.f. 18-1-2010).")\[employee\] has, during a continuous period of not less than twelve months immediately preceding the accident, been in the service of the employer who is liable to pay compensation, the monthly wages of the [110](#note_110 "110. Subs. for  workman  by Act 45 of 2009, S. 5 (w.e.f. 18-1-2010).")\[employee\] shall be one-twelfth of the total wages which have fallen due for payment to him by the employer in the last twelve months of that period;

[111](#note_111 "111. Ins. by Act 15 of 1933, S. 5.")\[(_b_) where the whole of the continuous period of service immediately preceding the accident during which the [112](#note_112 "112. Subs. for  workman  by Act 45 of 2009, S. 5 (w.e.f. 18-1-2010).")\[employee\] was in the service of the employer who is liable to pay the compensation was less than one month, the monthly wages of the [113](#note_113 "113. Subs. for  workman  by Act 45 of 2009, S. 5 (w.e.f. 18-1-2010).")\[employee\] shall be [114](#note_114 "114. The words  deemed to be  omitted by Act 13 of 1939, S. 2 (w.e.f. 30-6-1934).")\[\* \* \*\] the average monthly amount which, during the twelve months immediately preceding the accident, was being earned by a [115](#note_115 "115. Subs. for  workman  by Act 45 of 2009, S. 5 (w.e.f. 18-1-2010).")\[employee\] employed on the same work by the same employer, or, if there was no [116](#note_116 "116. Subs. for  workman  by Act 45 of 2009, S. 5 (w.e.f. 18-1-2010).")\[employee\] employed, by a [117](#note_117 "117. Subs. for  workman  by Act 45 of 2009, S. 5 (w.e.f. 18-1-2010).")\[employee\] employed on similar work in the same locality;\]

[118](#note_118 "118. Original cl. (b) re-lettered as (c) by Act 15 of 1933, S. 5.")\[(_c_)\] [119](#note_119 "119. Subs. by Act 8 of 1959, S. 6, for  in other cases  (w.e.f. 1-6-1959).")\[in other cases \[including cases in which it is not possible for want of necessary information to calculate the monthly wages under clause (_b_)\], the monthly wages shall be thirty times the total wages earned in respect of the last continuous period of service immediately preceding the accident from the employer who is liable to pay compensation, divided by the number of days comprising such period.\]

[120](#note_120 "120. The proviso omitted by Act 15 of 1933, S. 5.")\[\* \* \*\]

_Explanation_. A period of service shall, for the purposes of [121](#note_121 "121. Subs. by Act 5 of 1929, S. 3, for  this section .")\[this [122](#note_122 "122. Subs. by Act 9 of 1938, S. 4, for  sub-section .")\[section\]\], be deemed to be continuous which has not been interrupted by a period of absence from work exceeding fourteen days.

[123](#note_123 "123. Sub-section (2) added by Act 5 of 1929, S. 3, omitted by Act 15 of 1933, S. 5.")\[\* \* \*\]

Section 6. Review
-----------------

(1) Any half-monthly payment payable under this Act, either under an agreement between the parties or under the order of a Commissioner, may be reviewed by the Commissioner, on the application either of the employer or of the [124](#note_124 "124. Subs. for  workman  by Act 45 of 2009, S. 5 (w.e.f. 18-1-2010).")\[employee\] accompanied by the certificate of a qualified medical practitioner that there has been a change in the condition of the [125](#note_125 "125. Subs. for  workman  by Act 45 of 2009, S. 5 (w.e.f. 18-1-2010).")\[employee\] or, subject to rules made under this Act, an application made without such certificate.

(2) Any half-monthly payment may, on review under this section, subject to the provisions of this Act, be continued, increased, decreased or ended, or if the accident is found to have resulted in permanent disablement, be converted to the lump sum to which the [126](#note_126 "126. Subs. for  workman  by Act 45 of 2009, S. 5 (w.e.f. 18-1-2010).")\[employee\] is entitled less any amount which he has already received by way of half-monthly payments.

Section 7. Commutation of half-monthly payments
-----------------------------------------------

Any right to receive half-monthly payments may, by agreement between the parties or, if the parties cannot agree and the payments have been continued for not less than six months, on the application of either party to the Commissioner be redeemed by the payment of a lump sum of such amount as may be agreed to by the parties or determined by the Commissioner, as the case may be.

Section 8. Distribution of compensation
---------------------------------------

[127](#note_127 "127. Subs. by Act 5 of 1929, S. 4, for the original sub-sections (1) to (3).")\[(1) No payment of compensation in respect of a [128](#note_128 "128. Subs. for  workman  by Act 45 of 2009, S. 5 (w.e.f. 18-1-2010).")\[employee\] whose injury has resulted in death, and no payment of a lump sum as compensation to a woman or a person under a legal disability, shall be made otherwise than by deposit with the Commissioner, and no such payment made directly by an employer shall be deemed to be a payment of compensation:

[129](#note_129 "129. Subs. by Act 15 of 1933, S. 6, for the former proviso.")\[Provided that, in the case of a deceased [130](#note_130 "130. Subs. for  workman  by Act 45 of 2009, S. 5 (w.e.f. 18-1-2010).")\[employee\], an employer may make to any dependant advances on account of compensation [131](#note_131 "131. Subs. by Act 30 of 1995, S. 6 (w.e.f. 15-9-1995).")\[of an amount equal to three months' wages of such [132](#note_132 "132. Subs. for  workman  by Act 45 of 2009, S. 5 (w.e.f. 18-1-2010).")\[employee\] and so much of such amount\] as does not exceed the compensation payable to that dependant shall be deducted by the Commissioner from such compensation and repaid to the employer\].

(2) Any other sum amounting to not less than ten rupees which is payable as compensation may be deposited with the Commissioner on behalf of the person entitled thereto.

(3) The receipt of the Commissioner shall be a sufficient discharge in respect of any compensation deposited with him.\]

(4) On the deposit of any money under sub-section (1), [133](#note_133 "133. Ins. by Act 5 of 1929, S. 4.")\[as compensation in respect of a deceased [134](#note_134 "134. Subs. for  workman  by Act 45 of 2009, S. 5 (w.e.f. 18-1-2010).")\[employee\]\] the Commissioner [135](#note_135 "135. Certain words omitted by Act 30 of 1995, S. 6 (w.e.f. 15-9-1995).")\[\* \* \*\] shall, if he thinks necessary, cause notice to be published or to be served on each dependant in such manner as he thinks fit, calling upon the dependants to appear before him on such date as he may fix for determining the distribution of the compensation. If the Commissioner is satisfied after any inquiry which he may deem necessary, that no dependant exists, he shall repay the balance of the money to the employer by whom it was paid. The Commissioner shall, on application by the employer, furnish a statement showing in detail all disbursements made.

[136](#note_136 "136. Sub-sections (5), (6) and (7) subs. by Act 5 of 1929, S. 4, for the original sub-section (5).")\[(5) Compensation deposited in respect of a deceased [137](#note_137 "137. Subs. for  workman  by Act 45 of 2009, S. 5 (w.e.f. 18-1-2010).")\[employee\] shall, subject to any deduction made under sub-section (4), be apportioned among the dependants of the deceased [138](#note_138 "138. Subs. for  workman  by Act 45 of 2009, S. 5 (w.e.f. 18-1-2010).")\[employee\] or any of them in such proportion as the Commissioner thinks fit, or may, in the discretion of the Commissioner, be allotted to any one dependant.

(6) Where any compensation deposited with the Commissioner is payable to any person, the Commissioner shall, if the person to whom the compensation is payable is not a woman or a person under a legal disability, and may, in other cases, pay the money to the person entitled thereto.

(7) Where any lump sum deposited with the Commissioner is payable to a woman or a person under a legal disability, such sum may be invested, applied or otherwise dealt with for the benefit of the woman, or of such person during his disability, in such manner as the Commissioner may direct; and where a half-monthly payment is payable to any person under a legal disability, the Commissioner may, of his own motion or on an application made to him in this behalf, order that the payment be made during the disability to any dependant of the [139](#note_139 "139. Subs. for  workman  by Act 45 of 2009, S. 5 (w.e.f. 18-1-2010).")\[employee\] or to any other person, whom the Commissioner thinks best fitted to provide for the welfare of the [140](#note_140 "140. Subs. for  workman  by Act 45 of 2009, S. 5 (w.e.f. 18-1-2010).")\[employee\].\]

[141](#note_141 "141. The original sub-section (6) renumbered as sub-section (8) by Act 5 of 1929, S. 4.")\[(8)\] Where, an application made to him in this behalf or otherwise, the Commissioner is satisfied that, on account of neglect of children on the part of a parent or on account of the variation of the circumstances of any dependant or for any other sufficient cause, an order of the Commissioner as to the distribution of any sum paid as compensation or as to the manner in which any sum payable to any such dependant is to be invested, applied or otherwise dealt with, ought to be varied, the Commissioner may make such orders for the variation of the former order as he thinks just in the circumstances of case:

Provided that no such order prejudicial to any person shall be made unless such person has been given an opportunity of showing cause why the order should not be made, or shall be made in any case in which it would involve the repayment by a dependant of any sum already paid to him.

[142](#note_142 "142. Added by Act 5 of 1929, S. 4.")\[(9) Where the Commissioner varies any order under sub-section (8) by reason of the fact that payment of compensation to any person has been obtained by fraud, impersonation or other improper means, any amount so paid to or on behalf of such person may be recovered in the manner hereinafter provided in Section 31.\]

STATE AMENDMENTS

**Andhra Pradesh**. In its application to the State of Andhra Pradesh, to sub-section (4) of Section 8, the following proviso shall be _added_, namely:

Provided that in respect of a workman belonging to an establishment to which the Andhra Pradesh Labour Welfare Fund Act, 1987 applies, the Commissioner shall pay the said balance of the money into the Fund constituted under that Act in lieu of repaying it to the employer _Vide_ A.P. Act 34 of 1987, S. 40 (w.e.f. the date to be notified)

**Goa**. In its application to the State of Goa, in S. 8(4), _add_ as under

Provided that in respect of a workman belonging to an establishment to which the Andhra Pradesh Labour Welfare Fund Act, 1987 applies, the Commissioner shall pay the said balance of the money into the fund constituted under that Act in lieu of repaying to the employer . _Vide_ Goa Act 4 of 1987, S. 40.

Section 9. Compensation not to be assigned, attached or charged
---------------------------------------------------------------

Save as provided by this Act, no lump sum or half-monthly payment payable under this Act shall in any way be capable of being assigned or charged or be liable to attachment or pass to any person other than the [143](#note_143 "143. Subs. for  workman  by Act 45 of 2009, S. 5 (w.e.f. 18-1-2010).")\[employee\] by operation of law, nor shall any claim be set-off against the same.

Section 10. Notice and claim
----------------------------

(1) [144](#note_144 "144. Subs. by Act 9 of 1939, S. 5, for certain original words.")\[No claim for compensation shall be entertained by a Commissioner unless notice of the accident has been given in the manner hereinafter provided as soon as practicable after the happening thereof and unless the claim is preferred before him within [145](#note_145 "145. Subs. by Act 8 of 1959, S. 8, for  one year  (w.e.f. 1-6-1959).")\[two years\] of the occurrence of the accident or, in case of death, within [146](#note_146 "146. Subs. by Act 8 of 1959, S. 8, for  one year  (w.e.f. 1-6-1959).")\[two years\] from the date of death:\]

Provided that, where the accident is the contracting of a disease in respect of which the provisions of sub-section (2) of Section 3 are applicable, the accident shall be deemed to have occurred on the first of the days during which the [147](#note_147 "147. Subs. for  workman  by Act 45 of 2009, S. 5 (w.e.f. 18-1-2010).")\[employee\] was continuously absent from work in consequence of the disablement caused by the disease:

[148](#note_148 "148. Ins. by Act 64 of 1962.")\[Provided further that in case of partial disablement due to the contracting of any such disease and which does not force the [149](#note_149 "149. Subs. for  workman  by Act 45 of 2009, S. 5 (w.e.f. 18-1-2010).")\[employee\] to absent himself from work, the period of two years shall be counted from the day the [150](#note_150 "150. Subs. for  workman  by Act 45 of 2009, S. 5 (w.e.f. 18-1-2010).")\[employee\] gives notice of the disablement to his employer:

Provided further that if a [151](#note_151 "151. Subs. for  workman  by Act 45 of 2009, S. 5 (w.e.f. 18-1-2010).")\[employee\] who, having been employed in an employment for a continuous period, specified under sub-section (2) of Section 3 in respect of that employment, ceases to be so employed and develops symptoms of an occupational disease peculiar to that employment within two years of the cessation of employment, the accident shall be deemed to have occurred on the day on which the symptoms were first detected:\]

[152](#note_152 "152. Ins. by Act 15 of 1933, S. 7.")\[Provided further that the want of or any defect or irregularity in a notice shall not be a bar to the [153](#note_153 "153. Subs. by Act 9 of 1938, S. 5, for  maintenance of proceedings .")\[entertainment of a claim\]

(_a_) if the claim is [154](#note_154 "154. Subs. by Act 9 of 1938, S. 5, for  made .")\[preferred\] in respect of the death of a [155](#note_155 "155. Subs. for  workman  by Act 45 of 2009, S. 5 (w.e.f. 18-1-2010).")\[employee\] resulting from an accident which occurred on the premises of the employer, or at any place where the [156](#note_156 "156. Subs. for  workman  by Act 45 of 2009, S. 5 (w.e.f. 18-1-2010).")\[employee\] at the time of the accident was working under the control of the employer or of any person employed by him, and the [157](#note_157 "157. Subs. for  workman  by Act 45 of 2009, S. 5 (w.e.f. 18-1-2010).")\[employee\] died on such premises or at such place, or on any premises belonging to the employer, or died without having left the vicinity of the premises or place where the accident occurred, or

(_b_) if the employer [158](#note_158 "158. Ins. by Act 9 of 1938, S. 5.")\[or any one of several employers or any person responsible to the employer for the management of any branch of the trade or business in which the injured [159](#note_159 "159. Subs. for  workman  by Act 45 of 2009, S. 5 (w.e.f. 18-1-2010).")\[employee\] was employed\] had knowledge of the accident from any other source at or about the time when it occurred:

Provided further that the Commissioner may [160](#note_160 "160. Subs. by Act 9 of 1938, S. 5, for  admit .")\[entertain and decide\] any claim to compensation in any case notwithstanding that the notice has not been given, or the claim has not been [161](#note_161 "161. Subs. by Act 9 of 1938, S. 5, for  instituted .")\[preferred\], in due time as provided in this sub-section, if he is satisfied that the failure so to give the notice or [162](#note_162 "162. Subs. by Act 9 of 1938, S. 5, for  institute .")\[prefer\] the claim, as the case may be, was due to sufficient cause.\]

(2) Every such notice shall give the name and address of the person injured and shall state in ordinary language the cause of the injury and the date on which the accident happened, and shall be served on the employer or upon [163](#note_163 "163. Subs. by Act 7 of 1924, S. 2 and Sch. I, for  any one or .")\[any one of\] several employers, or upon any person [164](#note_164 "164. The word  directly  omitted by Act 9 of 1938, S. 5.")\[\* \* \*\] responsible to the employer for the management of any branch of the trade or business in which the injured [165](#note_165 "165. Subs. for  workman  by Act 45 of 2009, S. 5 (w.e.f. 18-1-2010).")\[employee\] was employed.

[166](#note_166 "166. Subs. by Act 15 of 1933, S. 7, for the original sub-section (3).")\[(3) The State Government may require that any prescribed class of employers shall maintain at their premises at which [167](#note_167 "167. Subs. for  workmen  by Act 45 of 2009, S. 5 (w.e.f. 18-1-2010).")\[employees\] are employed a notice-book, in the prescribed form, which shall be readily accessible at all reasonable times to any injured [168](#note_168 "168. Subs. for  workman  by Act 45 of 2009, S. 5 (w.e.f. 18-1-2010).")\[employee\] employed on the premises and to any person acting bona fide on his behalf.

(4) A notice under this section may be served by delivering it at, or sending it by registered post addressed to, the residence or any office or place of business of the person on whom it is to be served, or, where a notice-book is maintained, by entry in the notice-book.\]

[169](#note_169 "169. Ins. by Act 15 of 1933, S. 8.")\[**10-A. Power to require from employers statements regarding fatal accidents**. (1) Where a Commissioner receives information from any source that a [170](#note_170 "170. Subs. for  workman  by Act 45 of 2009, S. 5 (w.e.f. 18-1-2010).")\[employee\] has died as a result of an accident arising out of and in the course of his employment, he may send by registered post a notice to the [171](#note_171 "171. Subs. for  workman's  by Act 45 of 2009, S. 5 (w.e.f. 18-1-2010).")\[employees'\] employer requiring him to submit, within thirty days of the service of the notice, a statement, in the prescribed form, giving the circumstances attending the death of the [172](#note_172 "172. Subs. for  workman  by Act 45 of 2009, S. 5 (w.e.f. 18-1-2010).")\[employee\], and indicating whether, in the opinion of the employer, he is or is not liable to deposit compensation on account of the death.

(2) If the employer is of opinion that he is liable to deposit compensation, he shall make the deposit within thirty days of the service of the notice.

(3) If the employer is of opinion that he is not liable to deposit compensation, he shall in his statement indicate the grounds on which he disclaims liability.

(4) Where the employer has so disclaimed liability, the Commissioner, after such enquiry as he may think fit, may inform any of the dependants of the deceased [173](#note_173 "173. Subs. for  workman  by Act 45 of 2009, S. 5 (w.e.f. 18-1-2010).")\[employee\] that it is open to the dependants to prefer a claim for compensation, and may give them such other further information as he may think fit.

**10-B. Reports of fatal accidents and serious bodily injuries**. (1) Where, by any law for the time being in force, notice is required to be given to any authority, by or on behalf of an employer, of any accident occurring on his premises which results in death [174](#note_174 "174. Ins. by Act 8 of 1959, S. 9 (w.e.f. 1-6-1959).")\[or serious bodily injury\], the person required to give the notice shall, within seven days of the death [175](#note_175 "175. Ins. by Act 8 of 1959, S. 9 (w.e.f. 1-6-1959).")\[or serious bodily injury\], send a report to the Commissioner giving the circumstances attending the death [176](#note_176 "176. Ins. by Act 8 of 1959, S. 9 (w.e.f. 1-6-1959).")\[or serious bodily injury\]:

Provide that where the State Government has so prescribed the person required to give the notice may instead of sending such report to the Commissioner send it to the authority to whom he is required to give the notice.

[177](#note_177 "177. Ins. by Act 8 of 1959, S. 9 (w.e.f. 1-6-1959).")\[_Explanation_. Serious bodily injury means an injury which involves, or in all probability will involve, the permanent loss of the use of, or permanent injury to, any limb, or the permanent loss of or injury to the sight or hearing, or the fracture of any limb, or the enforced absence of the injured person from work for a period exceeding twenty days.\]

(2) The State Government may, by notification in the Official Gazette, extend the provisions of sub-section (1) to any class of premises other than those coming within the scope of that sub-section, and may, by such notification, specify the persons who shall send the report to the Commissioner.

[178](#note_178 "178. Ins. by Act 8 of 1959, S. 9 (w.e.f. 1-6-1959).")\[(3) Nothing in this section shall apply to the factories to which the Employees' State Insurance Act, 1948 (34 of 1948), applies.\]\]

Section 11. Medical examination
-------------------------------

(1) Where a [179](#note_179 "179. Subs. for  workman  by Act 45 of 2009, S. 5 (w.e.f. 18-1-2010).")\[employee\] has given notice of an accident, he shall, if the employer, before the expiry of three days from the time at which service of the notice has been effected, offers to have him examined free of charge by a qualified medical practitioner, submit himself for such examination, and any [180](#note_180 "180. Subs. for  workman  by Act 45 of 2009, S. 5 (w.e.f. 18-1-2010).")\[employee\] who is in receipt of a half-monthly payment under this Act shall, if so required, submit himself for such examination from time to time:

Provided that a [181](#note_181 "181. Subs. for  workman  by Act 45 of 2009, S. 5 (w.e.f. 18-1-2010).")\[employee\] shall not be required to submit himself for examination by a medical practitioner otherwise than in accordance with rules made under this Act, or at more frequent intervals than may be prescribed.

(2) If a [182](#note_182 "182. Subs. for  workman  by Act 45 of 2009, S. 5 (w.e.f. 18-1-2010).")\[employee\], on being required to do so by the employer under sub-section (1) or by the Commissioner at any time, refuses to submit himself for examination by a qualified medical practitioner or in any way obstructs the same, his right to compensation shall be suspended during the continuance of such refusal or obstruction unless, in the case of refusal, he was prevented by any sufficient cause from so submitting himself.

(3) If a [183](#note_183 "183. Subs. for  workman  by Act 45 of 2009, S. 5 (w.e.f. 18-1-2010).")\[employee\], before the expiry of the period within which he is liable under sub-section (1) to be required to submit himself for medical examination, voluntarily leaves without having been so examined the vicinity of the place in which he was employed, his right to compensation shall be suspended until he returns and offers himself for such examination.

(4) Where a [184](#note_184 "184. Subs. for  workman  by Act 45 of 2009, S. 5 (w.e.f. 18-1-2010).")\[employee\], whose right to compensation has been suspended under sub-section (2) or sub-section (3), dies without having submitted himself for medical examination as required by either of those sub-sections, the Commissioner may, if he thinks fit, direct the payment of compensation to the dependants of the deceased [185](#note_185 "185. Subs. for  workman  by Act 45 of 2009, S. 5 (w.e.f. 18-1-2010).")\[employee\].

(5) Where under sub-section (2) or sub-section (3) a right to compensation is suspended, no compensation shall be payable in respect of the period of suspension, and, if the period of suspension commences before the expiry of the waiting period referred to in clause (_d_) of sub-section (1) of Section 4, the waiting period shall be increased by the period during which the suspension continues.

(6) Where an injured [186](#note_186 "186. Subs. for  workman  by Act 45 of 2009, S. 5 (w.e.f. 18-1-2010).")\[employee\] has refused to be attended by a qualified medical practitioner whose services have been offered to him by the employer free of charge or having accepted such offer has deliberately disregarded the instructions of such medical practitioner, then, [187](#note_187 "187. Subs. by Act 9 of 1938, S. 6, for certain words.")\[if it is proved that the [188](#note_188 "188. Subs. for  workman  by Act 45 of 2009, S. 5 (w.e.f. 18-1-2010).")\[employee\] has not thereafter been regularly attended by a qualified medical practitioner or having been so attended has deliberately failed to follow his instructions and that such refusal, disregard or failure was unreasonable\] in the circumstances of the case and that the injury has been aggravated thereby, the injury and resulting disablement shall be deemed to be of the same nature and duration as they might reasonably have been expected to be if the [189](#note_189 "189. Subs. for  workman  by Act 45 of 2009, S. 5 (w.e.f. 18-1-2010).")\[employee\] had been regularly attended by a qualified medical practitioner, [190](#note_190 "190. Ins. by Act 9 of 1938, S. 6.")\[whose instructions he had followed\], and compensation, if any, shall be payable accordingly.

Section 12. Contracting
-----------------------

(1) Where any person (hereinafter in this section referred to as the principal) in the course of or for the purposes of his trade or business contracts with any other person (hereinafter in this section referred to as the contractor) for the execution by or under the contractor of the whole or any part of any work which is ordinarily part of the trade or business of the principal, the principal shall be liable to pay to any [191](#note_191 "191. Subs. for  workman  by Act 45 of 2009, S. 5 (w.e.f. 18-1-2010).")\[employee\] employed in the execution of the work any compensation which he would have been liable to pay if that [192](#note_192 "192. Subs. for  workman  by Act 45 of 2009, S. 5 (w.e.f. 18-1-2010).")\[employee\] had been immediately employed by him; and where compensation is claimed from the principal, this Act shall apply as if references to the principal were substituted for references to the employer except that the amount of compensation shall be calculated with reference to the wages of the [193](#note_193 "193. Subs. for  workman  by Act 45 of 2009, S. 5 (w.e.f. 18-1-2010).")\[employee\] under the employer by whom he is immediately employed.

(2) Where the principal is liable to pay compensation under this section, he shall be entitled to be indemnified by the contractor, [194](#note_194 "194. Ins. by Act 15 of 1933, S. 9.")\[or any other person from whom the [195](#note_195 "195. Subs. for  workman  by Act 45 of 2009, S. 5 (w.e.f. 18-1-2010).")\[employee\] could have recovered compensation and where a contractor who is himself a principal is liable to pay compensation or to indemnify a principal under this section he shall be entitled to be indemnified by any person standing to him in the relation of a contractor from whom the [196](#note_196 "196. Subs. for  workman  by Act 45 of 2009, S. 5 (w.e.f. 18-1-2010).")\[employee\] could have recovered compensation,\] and all questions as to the right to and the amount of any such indemnity shall, in default of agreement, be settled by the Commissioner.

(3) Nothing in this section shall be construed as preventing a [197](#note_197 "197. Subs. for  workman  by Act 45 of 2009, S. 5 (w.e.f. 18-1-2010).")\[employee\] from recovering compensation from the contractor instead of the principal.

(4) This section shall not apply in any case where the accident occurred elsewhere than on, in or about the premises on which the principal has undertaken or usually undertakes, as the case may be, to execute the work or which are otherwise under his control or management.

Section 13. Remedies of employer against stranger
-------------------------------------------------

Where a [198](#note_198 "198. Subs. for  workman  by Act 45 of 2009, S. 5 (w.e.f. 18-1-2010).")\[employee\] has recovered compensation in respect of any injury caused under circumstances creating a legal liability of some person other than the person by whom the compensation was paid to pay damages in respect thereof, the person by whom the compensation was paid and any person who has been called on to pay an indemnity under Section 12 shall be entitled to be indemnified by the person so liable to pay damages as aforesaid.

Section 14. Insolvency of employer
----------------------------------

(1) Where any employer has entered into a contract with any insurers in respect of any liability under this Act to any [199](#note_199 "199. Subs. for  workman  by Act 45 of 2009, S. 5 (w.e.f. 18-1-2010).")\[employee\], then in the event of the employer becoming insolvent or making a composition or scheme of arrangement with his creditors or, if the employer is a company, in the event of the company having commenced to be wound up, the rights of the employer against the insurers as respects that liability shall, notwithstanding anything in any law for the time being in force relating to insolvency or the winding up of companies, be transferred to and vest in the [200](#note_200 "200. Subs. for  workman  by Act 45 of 2009, S. 5 (w.e.f. 18-1-2010).")\[employee\], and upon any such transfer the insurers shall have the same rights and remedies and be subject to the same liabilities as if they were the employer, so, however, that the insurers shall not be under any greater liability to the [201](#note_201 "201. Subs. for  workman  by Act 45 of 2009, S. 5 (w.e.f. 18-1-2010).")\[employee\] than they would have been under the employer.

(2) If the liability of the insurers to the [202](#note_202 "202. Subs. for  workman  by Act 45 of 2009, S. 5 (w.e.f. 18-1-2010).")\[employee\] is less than the liability of the employer to the [203](#note_203 "203. Subs. for  workman  by Act 45 of 2009, S. 5 (w.e.f. 18-1-2010).")\[employee\], the [204](#note_204 "204. Subs. for  workman  by Act 45 of 2009, S. 5 (w.e.f. 18-1-2010).")\[employee\] may prove for the balance in the insolvency proceedings or liquidation.

(3) Where in any case such as is referred to in sub-section (1) the contract of the employer with the insurers is void or voidable by reason of non-compliance on the part of the employer with any terms or conditions of the contract (other than a stipulation for the payment of premia), the provisions of that sub-section shall apply as if the contract were not void or voidable, and the insurers shall be entitled to prove in the insolvency proceedings or liquidation for the amount paid to the [205](#note_205 "205. Subs. for  workman  by Act 45 of 2009, S. 5 (w.e.f. 18-1-2010).")\[employee\]:

Provided that the provisions of this sub-section shall not apply in any case in which the [206](#note_206 "206. Subs. for  workman  by Act 45 of 2009, S. 5 (w.e.f. 18-1-2010).")\[employee\] fails to give notice to the insurers of the happening of the accident and of any resulting disablement as soon as practicable after he becomes aware of the institution of the insolvency or liquidation proceedings.

(4) There shall be deemed to be included among the debts which under Section 49 of the Presidency Towns Insolvency Act, 1909 (3 of 1909), or under Section 61 of the Provincial Insolvency Act, 1920 (5 of 1920), or under [207](#note_207 "207. Subs. by Act 30 of 1995, S. 7 (w.e.f. 15-9-1995).")\[Section 530 of the Companies Act, 1956 (1 of 1956)\], are in the distribution of the property of an insolvent or in the distribution of the assets of a company being wound upto be paid in priority to all other debts, the amount due in respect of any compensation the liability wherefor accrued before the date of the order of adjudication of the insolvent or the date of the commencement of the winding up, as the case may be, and those Acts shall have effect accordingly.

(5) Where the compensation is a half-monthly payment, the amount due in respect thereof shall, for the purposes of this section, be taken to be the amount of the lump sum for which the half-monthly payment could, if redeemable, be redeemed if application were made for that purpose under Section 7, and a certificate of the Commissioner as to the amount of such sum shall be conclusive proof thereof.

(6) The provisions of sub-section (4) shall apply in the case of any amount for which an insurer is entitled to prove under sub-section (3), but otherwise those provisions shall not apply where the insolvent or the company being wound up has entered into such a contract with insurers as is referred to in sub-section (1).

(7) This section shall not apply where a company is wound up voluntarily merely for purposes of reconstruction or of amalgamation with another company.

[208](#note_208 "208. Ins. by Act 8 of 1959, S. 10 (w.e.f. 1-6-1959).")\[**14-A. Compensation to be first charge on assets transferred by employer**. Where an employer transfers his assets before any amount due in respect of any compensation, the liability wherefor accrued before the date of the transfer, has been paid, such amount shall, notwithstanding anything contained in any other law for the time being in force, be a first charge on that part of the assets so transferred as consists of immovable property.\]

Section 15. Special provisions relating to masters and seamen
-------------------------------------------------------------

This Act shall apply in the case of [209](#note_209 "209. Subs. for  workmen  by Act 45 of 2009, S. 5 (w.e.f. 18-1-2010).")\[employees\] who are masters of [210](#note_210 "210. The word  registered  omitted by Act 15 of 1933, S. 10.")\[\* \* \*\] ships or seamen subject to the following modifications, namely:

(1) The notice of the accident and the claim for compensation may, except where the person injured is the master of the ship, be served on the master of the ship as if he were the employer, but where the accident happened and the disablement commenced on board the ship, it shall not be necessary for any seaman to give any notice of the accident.

(2) In the case of the death of a master or seaman, the claim for compensation shall be made within [211](#note_211 "211. Subs. by Act 8 of 1959, S. 11, for  six months  (w.e.f. 1-6-1959).")\[one year\] after the news of the death has been received by the claimant or, where the ship has been or is deemed to have been lost with all hands, within eighteen months of the date on which the ship was, or is deemed to have been, so lost:

[212](#note_212 "212. Added by Act 8 of 1959, S. 11 (w.e.f. 1-6-1959).")\[Provided that the Commissioner may entertain any claim to compensation in any case notwithstanding that the claim has not been preferred in due time as provided in this sub-section, if he is satisfied that the failure so to prefer the claim was due to sufficient cause.\]

(3) Where an injured master or seaman is discharged or left behind in any part of [213](#note_213 "213. Ins. by the A.O. 1950.")\[India or\] [214](#note_214 "214. Subs. by Act 22 of 1984, S. 4, w.e.f. 1-7-1984.")\[in any foreign country\] any depositions taken by any Judge or Magistrate in that part or by any Consular Officer in the foreign country and transmitted by the person by whom they are taken to the Central Government or any State Government shall, in any proceedings for enforcing the claim, be admissible in evidence

(_a_) if the deposition is authenticated by the signature of the Judge, Magistrate or Consular Officer before whom it is made;

(_b_) if the defendant or the person accused, as the case may be, had an opportunity by himself or his agent to cross-examine the witness; and

(_c_) if the deposition was made in the course of a criminal proceeding, on proof that the deposition was made in the presence of the person accused;

and it shall not be necessary in any case to prove the signature or official character of the person appearing to have signed any such deposition and a certificate by such person that the defendant or the person accused had an opportunity of cross-examining the witness and that the deposition if made in a criminal proceeding was made in the presence of the person accused shall, unless the contrary is proved, be sufficient evidence that he had that opportunity and that it was so made.

[215](#note_215 "215. Original cl. (4) omitted by Act 9 of 1938, S. 7.")\[\* \* \*\]

[216](#note_216 "216. Original cl. (5) renumbered as cl. (4) by Act 9 of 1938, S. 7.")\[(4)\] No [217](#note_217 "217. Subs. by Act 7 of 1924, S. 2 and Sch. I, for  monthly payment .")\[half-monthly payment\] shall be payable in respect of the period during which the owner of the ship is, under any law in force for the time being [218](#note_218 "218. The words and letters  in Part A States and Part C States  omitted by Act 3 of 1951, S. 3 and Sch.")\[\* \* \*\] relating to merchant shipping liable to defray the expenses of maintenance of the injured master or seaman.

[219](#note_219 "219. Subs. by Act 1 of 1942, S. 2, for cl. (5)(w.e.f. 3-9-1939). Cl. (5) was ins. by Act 42 of 1939, S. 2.")\[(5) No compensation shall be payable under this Act in respect of any injury in respect of which provision is made for payment of a gratuity, allowance or pension under the War Pensions and Detention Allowances (Mercantile Marine, _etc._) Scheme, 1939, or the War Pensions and Detention Allowances (Indian Seamen, etc.) Scheme, 1941, made under the Pensions (Navy, Army, Air Force and Mercantile Marine) Act, 1939, or under the War Pensions and Detention Allowances (Indian Seaman) Scheme, 1942, made by the Central Government.

(6) Failure to give a notice or make a claim or commence proceedings within the time required by this Act shall not be a bar to the maintenance of proceedings under this Act in respect of any personal injury, if

(_a_) an application has been made for payment in respect of that injury under any of the schemes referred to in the preceding clause, and

(_b_) the State Government certifies that the said application was made in the reasonable belief that the injury was one in respect of which the scheme under which the application was made makes provisions for payments, and that the application was rejected or that payments made in pursuance of the application were discontinued on the ground that the injury was not such an injury, and

(_c_) the proceedings under this Act are commenced within one month from the date on which the said certificate of the State Government was furnished to the person commencing the proceedings.\]

[220](#note_220 "220. Ss. 15-A and 15-B ins. by Act 30 of 1995, S. 8 (w.e.f. 15-9-1995).")\[**15-A. Special provisions relating to captains and other members of crew of aircrafts**. This Act shall apply in the case of [221](#note_221 "221. Subs. for  workmen  by Act 45 of 2009, S. 5 (w.e.f. 18-1-2010).")\[employees\] who are captains or other members of the crew of aircrafts subject to the following modifications, namely:

(1) The notice of the accident and the claim for compensation may, except where the person injured is the captain of the aircraft, be served on the captain of the aircraft as if he were the employer, but where the accident happened and the disablement commenced on board the aircraft it shall not be necessary for any member of the crew to give notice of the accident.

(2) In the case of the death of the captain or other member of the crew, the claim for compensation shall be made within one year after the news of the death has been received by the claimant or, where the aircraft has been or is deemed to have been lost with all hands, within eighteen months of the date on which the aircraft was, or is deemed to have been, so lost:

Provided that the Commissioner may entertain any claim for compensation in any case notwithstanding that the claim has not been preferred in due time as provided in this sub-section, if he is satisfied that the failure so to prefer the claim was due to sufficient cause.

(3) Where an injured captain or other member of the crew of the aircraft is discharged or left behind in any part of India or in any other country, any depositions taken by any Judge or Magistrate in that part or by any Consular Officer in the foreign country and transmitted by the person by whom they are taken to the Central Government or any State Government shall, in any proceedings for enforcing the claims, be admissible in evidence

(_a_) if the deposition is authenticated by the signature of the Judge, Magistrate or Consular Officer before whom it is made;

(_b_) if the defendant or the person accused, as the case may be, had an opportunity by himself or his agent to cross-examine the witness;

(_c_) if the deposition was made in the course of a criminal proceeding, on proof that the deposition was made in the presence of the person accused,

and it shall not be necessary in any case to prove the signature or official character of the person appearing to have signed any such deposition and a certificate by such person that the defendant or the person accused had an opportunity of cross-examining the witness and that the deposition if made in a criminal proceeding was made in the presence of the person accused shall, unless the contrary is proved, be sufficient evidence that he had that opportunity and that it was so made.

**15-B. Special provisions relating to** [222](#note_222 "222. Subs. for  workmen  by Act 45 of 2009, S. 5 (w.e.f. 18-1-2010).")\[**employees**\] **abroad of companies and motor vehicles**. This Act shall apply

(_i_) in the case of [223](#note_223 "223. Subs. for  workman  by Act 45 of 2009, S. 5 (w.e.f. 18-1-2010).")\[employees\] who are persons recruited by companies registered in India and working as such abroad, and

(_ii_) persons sent for work abroad along with motor vehicles registered under the Motor Vehicles Act, 1988 as drivers, helpers, mechanics, cleaners or other [224](#note_224 "224. Subs. for  workman  by Act 45 of 2009, S. 5 (w.e.f. 18-1-2010).")\[employees\], subject to the following modifications, namely:

(1) The notice of the accident and the claim for compensation may be served on the local agent of the company, or the local agent of the owner of the motor vehicle, in the country of accident, as the case may be.

(2) In the case of death of the [225](#note_225 "225. Subs. for  workman  by Act 45 of 2009, S. 5 (w.e.f. 18-1-2010).")\[employee\] in respect of the whom the provisions of this section shall apply, the claim for compensation shall be made within one year after the news of the death has been received by the claimant:

Provided that the Commissioner may entertain any claim for compensation in any case notwithstanding that the claim has not been preferred in due time as provided in this sub-section, if he is satisfied that the failure so to prefer the claim was due to sufficient cause.

(3) Where an injured [226](#note_226 "226. Subs. for  workman  by Act 45 of 2009, S. 5 (w.e.f. 18-1-2010).")\[employee\] is discharged or left behind in any part of India or in any other country any depositions taken by any Judge or Magistrate in that part or by any Consular Officer in the foreign country and transmitted by the person by whom they are taken to the Central Government or any State Government shall, in any proceedings for enforcing the claims, be admissible in evidence

(_a_) if the deposition is authenticated by the signature of the Judge, Magistrate or Consular Officer before whom it is made;

(_b_) if the defendant or the person accused, as the case may be, had an opportunity by himself or his agent to cross-examine the witness;

(_c_) if the deposition was made in the course of a criminal proceeding, on proof that the deposition was made in the presence of the person accused,

and it shall not be necessary in any case to prove the signature or official character of the person appearing to have signed any such deposition and a certificate by such person that the defendant or the person accused had an opportunity of cross-examining the witness and that the deposition if made in a criminal proceeding was made in the presence of the person accused shall, unless the contrary is proved, be sufficient evidence that he had that opportunity and that it was so made.\]

Section 16. Returns as to compensation
--------------------------------------

The [227](#note_227 "227. The words  G.G. in C  have been substituted by the A.O. 1937 and the A.O. 1950, to read as above.")\[State Government\] may, by notification in the Official Gazette, direct that every person employing [228](#note_228 "228. Subs. for  workmen  by Act 45 of 2009, S. 5 (w.e.f. 18-1-2010).")\[employees\], or that any specified class of such persons, shall send at such time and in such form and to such authority, as may be specified in the notification, a correct return specifying the number of injuries in respect of which compensation has been paid by the employer during the previous year and the amount of such compensation, together with such other particulars as to the compensation as the [229](#note_229 "229. The words  G.G. in C  have been substituted by the A.O. 1937 and the A.O. 1950, to read as above.")\[State Government\] may direct.

Section 17. Contracting out
---------------------------

Any contract or agreement whether made before or after the commencement of this Act, whereby a [230](#note_230 "230. Subs. for  workman  by Act 45 of 2009, S. 5 (w.e.f. 18-1-2010).")\[employee\] relinquishes any right of compensation from the employer for personal injury arising out of or in the course of the employment, shall be null and void in so far as it purports to remove or reduce the liability of any person to pay compensation under this Act.

[231](#note_231 "231. Ins. by Act 11 of 2017, S. 2 (w.e.f. 15-5-2017).")\[**17-A. Duty of employer to inform employee of his rights**. Every employer shall immediately at the time of employment of an employee, inform the employee of his rights to compensation under this Act, in writing as well as through electronic means, in english or hindi or in the official language of the area of employment, as may be understood by the employee.\]

Section 18. Proof of age
------------------------

\[_Repealed by the Workmen's Compensation (Amendment) Act, 1959_ (_8 of 1959_), _Section 12_ (_w.e.f. 1-6-1959_).\]

[232](#note_232 "232. Ins. by Act 15 of 1933, S. 11.")\[**18-A. Penalties**. (1) Whoever

(_a_) fails to maintain a notice-book which he is required to maintain under sub-section (3) of Section 10, or

(_b_) fails to send to the Commissioner a statement which he is required to send under sub-section (1) of Section 10-A, or

(_c_) fails to send a report which he is required to send under Section 10-B, or

(_d_) fails to make a return which he is required to make under [233](#note_233 "233. Subs. for  Section 16,  by Act 11 of 2017, S. 3(i)(w.e.f. 15-5-2017).")\[Section 16, or\]

[234](#note_234 "234. Ins. by Act 11 of 2017, S. 3(ii)(w.e.f. 15-5-2017).")\[(_e_) fails to inform the employee of his rights to compensation as required under Section 17-A,\]

shall be punishable with fine [235](#note_235 "235. Subs. for  which may extend to five thousand rupees  by Act 11 of 2017, S. 3(iii)(w.e.f. 15-5-2017).")\[which shall not be less than fifty thousand rupees but which may extend to one lakh rupees\].

(2) No prosecution under this section shall be instituted except by or with the previous sanction of a Commissioner, and no Court shall take cognisance of any offence under this section, unless complaint thereof is made [236](#note_236 "236. Subs. by Act 64 of 1962.")\[within six months of the date on which the alleged commission of the offence came to the knowledge of the Commissioner\].

Chapter III

COMMISSIONERS

Section 19. Reference to Commissioners
--------------------------------------

(1) If any question arises in any proceedings under this Act as to the liability of any person to pay compensation (including any question as to whether a person injured is or is not a [237](#note_237 "237. Subs. for  workman  by Act 45 of 2009, S. 5 (w.e.f. 18-1-2010).")\[employee\]) or as to the amount or duration of compensation (including any question as to the nature or extent of disablement), the question shall, in default of agreement, be settled by [238](#note_238 "238. Subs. by Act 15 of 1933, S. 12, for  the Commissioner .")\[a Commissioner\].

(2) No Civil Court shall have jurisdiction to settle, decide or deal with any question which is by or under this Act required to be settled, decided or dealt with by a Commissioner or to enforce any liability incurred under this Act.

Section 20. Appointment of Commissioners
----------------------------------------

(1) The State Government may, by notification in the Official Gazette, appoint any person [239](#note_239 "239. Ins. by Act 45 of 2009, S. 8 (w.e.f. 18-1-2010).")\[who is or has been a member of a State Judicial Service for a period of not less than five years or is or has been for not less than five years an advocate or a pleader or is or has been a Gazetted Officer for not less than five years having educational qualifications and experience in personnel management, human resource development and industrial relations\] to be a Commissioner for [240](#note_240 "240. Subs. for  workmen's  by Act 45 of 2009, S. 5 (w.e.f. 18-1-2010).")\[Employees'\] Compensation for such [241](#note_241 "241. The word  Local  omitted by Act 64 of 1962.")\[\* \* \*\] area as may be specified in the notification.

[242](#note_242 "242. Ins. by Act 15 of 1933, S. 13.")\[(2) Where more than one Commissioner has been appointed for any [243](#note_243 "243. The word  Local  omitted by Act 64 of 1962.")\[\* \* \*\] area, the State Government may, by general or special order, regulate the distribution of business between them.\]

[244](#note_244 "244. Original sub-sections (2) and (3) renumbered as sub-sections (3) and (4) by Act 15 of 1933, S. 13.")\[(3)\] Any Commissioner may, for the purpose of deciding any matter referred to him for decision under this Act, choose one or more persons possessing special knowledge of any matter relevant to the matter under inquiry to assist him in holding the inquiry.

[245](#note_245 "245. Renumbered by Act 15 of 1933, S. 13.")\[(4)\] Every Commissioner shall be deemed to be a public servant within the meaning of the Indian Penal Code (45 of 1860).

Section 21. Venue of proceedings and transfer
---------------------------------------------

[246](#note_246 "246. Subs. by Act 30 of 1995, S. 10 (w.e.f. 1-10-1996).")\[(1) Where any matter under this Act is to be done by or before a Commissioner, the same shall, subject to the provisions of this Act and to any rules made hereunder, be done by or before the Commissioner for the area in which

(_a_) the accident took place which resulted in the injury; or

(_b_) the [247](#note_247 "247. Subs. for  workman  by Act 45 of 2009, S. 5 (w.e.f. 18-1-2010).")\[employee\] or in case of his death, the dependant claiming the compensation ordinarily resides; or

(_c_) the employer has his registered office:

Provided that no matter shall be processed before or by a Commissioner, other than the Commissioner having jurisdiction over the area in which the accident took place, without his giving notice in the manner prescribed by the Central Government to the Commissioner having jurisdiction over the area and the State Government concerned:

Provided further that, where the [248](#note_248 "248. Subs. for  workman  by Act 45 of 2009, S. 5 (w.e.f. 18-1-2010).")\[employee\], being the master of a ship or a seaman or the captain or a member of the crew of an aircraft or a [249](#note_249 "249. Subs. for  workman  by Act 45 of 2009, S. 5 (w.e.f. 18-1-2010).")\[employee\] in a motor vehicle or a company, meets with the accident outside India any such matter may be done by or before a Commissioner for the area in which the owner or agent of the ship, aircraft or motor vehicle resides or carries on business or the registered office of the company is situate, as the case may be.

(1-A) If a Commissioner, other than the Commissioner with whom any money has been deposited under Section 8, proceeds with a matter under this Act, the former may for the proper disposal of the matter call for transfer of any records or money remaining with the latter and on receipt of such a request, he shall comply with the same.\]

(2) If a Commissioner is satisfied [250](#note_250 "250. Subs. by Act 9 of 1938, S. 9, for certain words.")\[that any matter arising out of any proceedings pending before him\] can be more conveniently dealt with by any other Commissioner, whether in the same State or not, he may, subject to rules made under this Act, order such matter to be transferred to such other Commissioner either for report or for disposal, and, if he does so, shall forthwith transmit to such other Commissioner all documents relevant for the decision of such matter and, where the matter is transferred for disposal, shall also transmit in the prescribed manner any money remaining in his hands or invested by him for the benefit of any party to the proceedings:

[251](#note_251 "251. Ins. by Act 9 of 1938, S. 9.")\[Provided that the Commissioner shall not, where any party to the proceedings has appeared before him, make any order of transfer relating to the distribution among dependants of a lump sum without giving such party an opportunity of being heard:\]

[252](#note_252 "252. Second Proviso omitted by Act 30 of 1995, S. 10 (w.e.f. 1-10-1996).")\[\* \* \*\].

(3) The Commissioner to whom any matter is so transferred shall, subject to rules made under this Act, inquire there into and, if the matter was transferred for report, return his report thereon or, if the matter as transferred for disposal, continue the proceedings as if they had originally commenced before him.

(4) On receipt of a report from a Commissioner to whom any matter has been transferred for report under sub-section (2), the Commissioner by whom it was referred shall decide the matter referred in conformity with such report.

[253](#note_253 "253. Ins. by Act 15 of 1933, S. 14.")\[(5) The State Government may transfer any matter from any Commissioner appointed by it to any other Commissioner appointed by it.\]

Section 22. Form of application
-------------------------------

[254](#note_254 "254. Subs. by Act 30 of 1995, S. 11 (w.e.f. 15-9-1995).")\[(1) Where an accident occurs in respect of which liability to pay compensation under this Act arises, a claim for such compensation may, subject to the provisions of this Act, be made before the Commissioner.

(1-A) Subject to the provisions of sub-section (1), no application for the settlement\] of any matter by a Commissioner, [255](#note_255 "255. Ins. by Act 15 of 1933, S. 15.")\[other than an application by a dependant or dependants for compensation\] shall be made unless and until some question has arisen between the parties in connection therewith which they have been unable to settle by agreement.

(2) [256](#note_256 "256. Subs. by Act 15 of 1933, S. 15, for  Where any such question has arisen, the application .")\[An application to a Commissioner\] may be made in such form and shall be accompanied by such fee, if any, as may be prescribed and shall contain, in addition to any particulars which may be prescribed, the following particulars, namely

(_a_) a concise statement of the circumstances in which the application is made and the relief or order which the applicant claims;

(_b_) in the case of a claim for compensation against an employer, the date of service of notice of the accident on the employer and, if such notice has not been served or has not been served in due time, the reason for such omission;

(_c_) the names and addresses of the parts; and

(_d_) [257](#note_257 "257. Ins. by Act 15 of 1933, S. 15.")\[except in the case of an application by dependants for compensation\] a concise statement of the matters on which agreement has and [258](#note_258 "258. Subs. by Act 37 of 1925, S. 2 and Sch. I, for  on .")\[of\] those on which agreement has not been come to.

(3) If the applicant is illiterate or for any other reason is unable to furnish the required information in writing, the application shall, if the applicant so desires, be prepared under the direction of the Commissioner.

STATE AMENDMENT

**Gujarat**. In its application to the State of Gujarat, in the Employee's Compensation Act, 1923 (8 of 1923), in Section 22, to sub-section (1-A), the following proviso shall be _added_, namely:

Provided that if an application by an employee or by dependent or dependents for compensation is not made before the Commissioner within a period of ninety days from the date of the occurrence of the accident, then such application may be filed by an officer authorised by the State Government in this behalf for the purpose of compensation to be paid to such employee or dependent or dependents. . \[_Vide_ Gujarat Act 29 of 2015, S. 2 (w.e.f. 1-1-2016)\]

**Uttar Pradesh**. In its application to the State of Uttar Pradesh, in Section 22, in sub-section (1-A), the following proviso shall be _inserted_ at the end, namely

Provided that if an application is not made before the Commissioner by an employee or by dependant or dependants thereof within a period of ninety days from the date of the occurrence of the accident, then without prejudice to the right conferred to an employee or dependant or dependants thereof under this Act or the rules made thereunder, such application may be filed by an officer authorised by the State Government in this behalf for the purpose of compensation to be paid to such employee or dependant or dependants thereof:

Provided further that where it comes to the notice of the Commissioner that application for compensation arising out of same accident has been filed by both the employee or dependant or dependants thereof and by the officer referred to in the first proviso, the Commissioner shall club both the applications and decide the same by single order without prejudice to the right of such employee or dependant or dependants thereof. \[_Vide_ U.P. Act 27 of 2018, S. 2, dt. 9-5-2018\]

[259](#note_259 "259. Ins. by Act 15 of 1933, S. 16.")\[**22-A. Power of Commissioner to require further deposit in cases of fatal accident**. (1) Where any sum has been deposited by an employer as compensation payable in respect of a [260](#note_260 "260. Subs. for  workman  by Act 45 of 2009, S. 5 (w.e.f. 18-1-2010).")\[employee\] whose injury has resulted in death, and in the opinion of the Commissioner such sum is insufficient, the Commissioner may, by notice in writing stating his reasons, call upon the employer to show cause why he should not make a further deposit within such time as may be stated in the notice.

(2) If the employer fails to show cause to the satisfaction of the Commissioner, the Commissioner may make an award determining the total amount payable, and requiring the employer to deposit the deficiency.\]

Section 23. Powers and procedure of Commissioners
-------------------------------------------------

The Commissioner shall have all the powers of a Civil Court under the Code of Civil Procedure, 1908 (5 of 1908), for the purpose of taking evidence on oath (which such Commissioner is hereby empowered to impose) and of enforcing the attendance of witnesses and compelling the production of documents and material objects [261](#note_261 "261. Added by Act 5 of 1929, S. 5.")\[and the Commissioner shall be deemed to be Civil Court for all the purposes of [262](#note_262 "262. Subs. by Act 30 of 1995 (w.e.f. 15-9-1995).")\[Section 195 and of Chapter XXVI of the Code of Criminal Procedure, 1973 (2 of 1974)\]\].

Section 24. Appearance of parties
---------------------------------

[263](#note_263 "263. Subs. by Act 8 of 1959, S. 14, for the former section (w.e.f. 1-6-1959).")\[Any appearance, application or act required to be made or done by any person before or to a Commissioner (other than an appearance of a party which is required for the purpose of his examination as a witness) may be made or done on behalf of such person by a legal practitioner or by an official of an Insurance Company or a registered Trade Union or by an Inspector appointed under sub-section (1) of Section 8 of the Factories Act, 1948 (63 of 1948), or under sub-section (1) of Section 5 of the Mines Act, 1952 (35 of 1952), or by any other officer specified by the State Government in this behalf, authorised in writing by such person, or, with the permission of the Commissioner, by any other person so authorised.\]

STATE AMENDMENTS

SECTIONS 24-A AND 24-B

**West Bengal**. In its application to the State of West Bengal, after Section 24 of the said Act the following sections shall be _inserted_, namely:

24-A. _Reference of disputed medical questions to a medical referee_. (1) If any question arises in any proceedings under this Act pending before the Commissioner as to

(_a_) the nature and extent of the permanent disablement of a workman, or

(_b_) the duration of his temporary disablement, or

(_c_) whether the incapacity of a workman is due to personal injury by accident, or

(_d_) whether a workman has contracted any occupational disease specified in Schedule III,

the question shall, in default of agreement, on the joint application of both parties or on the application of either party in the prescribed manner, and on payment in the prescribed manner by the parties, or the party making the application, as the case may be, of the prescribed fees and expenses, be referred by the Commissioner to a medical referee appointed by him in his discretion from amongst the medical practitioners included in the list prepared under Section 24-B:

Provided that where an application is made by only one of the parties, if the Commissioner is of the opinion that the question is one which ought not on account of the exceptional difficulty of the case or for any other sufficient reason be referred to a medical referee, he may after recording his reasons in writing reject the application:

Provided further that if the parties themselves jointly select any medical referee, the Commissioner shall, on payment of the prescribed fees and expenses in the prescribed manner, appoint that medical practitioner:

Provided further that a medical practitioner whose services have been used for the medical treatment of an injury by accident to a workman, or of an occupational disease specified in Schedule III contracted by such workman, by or on behalf of such workman or his employer or by or on behalf of any insurers interested in any proceeding under this Act arising out of such injury or disease, shall not act as a medical referee in any proceedings under this Act in respect of such injury or disease.

(2) The medical referee to whom such a reference is made under sub-section (1) shall, in accordance with the prescribed rules, require the workman to submit to a medical examination by him or under his personal direction and shall personally or with such medical assistance as he may deem necessary examine the workman medically and send to the Commissioner who has made the reference a report in respect of the question specifically mentioned in the order of reference.

(3) If a workman refuses to submit himself for medical examination by or under the personal direction of the medical referee to whom a reference has been made under this section, or if a workman in any way obstructs the medical examination by or under the personal direction of the medical referee the workman's right to compensation under this Act and his right to continue any proceedings under this Act shall be suspended until such examination has taken place.

(4) In any proceedings under this Act in which evidence is recorded, the report of the medical referee shall as between the parties to the proceedings be conclusive proof of the facts related therein within the meaning of Section 4 of the Indian Evidence Act, 1872:

Provided that such report shall not so be regarded as conclusive proof of the facts related therein if in the particular proceedings the Commissioner, either of his own motion or on application being made to him by either party, for reasons to be recorded by him in writing, deems it expedient in the interests of justice to allow the parties to adduce further evidence on such facts.

24-B. _List of qualified medical practitioners for appointment as medical referees_. The State Government shall prepare a list of qualified medical practitioners who may be appointed as medical referees under Section 24-A and shall publish the said list in the _Official Gazette_. _Vide_ Bengal Act 6 of 1942, S. 4.

Section 25. Method of recording evidence
----------------------------------------

The Commissioner shall make a brief memorandum of the substance of the evidence of every witness as the examination of the witness proceeds, and such memorandum shall be written and signed by the Commissioner with his own hand and shall form a part of the record:

Provided that, if the Commissioner is prevented from making such memorandum, he shall record the reason of his inability to do so and shall cause such memorandum to be made in writing from his dictation and shall sign the same, and such memorandum shall form a part of the record:

Provided further that the evidence of any medical witness shall be taken down as nearly as may be word for word.

[264](#note_264 "264. Ins. by Act 45 of 2009, S. 9 (w.e.f. 18-1-2010).")\[**25-A. Time limit for disposal of cases relating to compensation**. The Commissioner shall dispose of the matter relating to compensation under this Act within a period of three months from the date of reference and intimate the decision in respect thereof within the said period to the employee.\]

Section 26. Costs
-----------------

All costs, incidental to any proceedings before a Commissioner, shall, subject to rules made under this Act, be in the discretion of the Commissioner.

Section 27. Power to submit cases
---------------------------------

A Commissioner may, if he thinks fit, submit any question of law for the decision of the High Court and, if he does so, shall decide the question in conformity with such decision.

Section 28. Registration of agreements
--------------------------------------

(1) Where the amount of any lump sum payable as compensation has been settled by agreement, whether by way of redemption of a half-monthly payment or otherwise, or where any compensation has been so settled as being payable [265](#note_265 "265. Subs. by Act 5 of 1929, S. 6, for  to a person under a legal disability .")\[to a woman or a person under a legal disability\] [266](#note_266 "266. The words  or to a dependant , repealed by Act 7 of 1924, S. 3 and Sch. II.")\[\* \* \*\] a memorandum thereof shall be sent by the employer to the Commissioner, who shall, on being satisfied as to its genuineness, record the memorandum in a register in the prescribed manner:

Provided that

(_a_) no such memorandum shall be recorded before seven days after communication by the Commissioner of notice to the parties concerned;

(_b_) [267](#note_267 "267. Cl. (b) omitted by Act 5 of 1929, S. 6.")\[\* \* \*\];

(_c_) the Commissioner may at any time rectify the register;

(_d_) where it appears to the Commissioner that an agreement as to the payment of a lump sum whether by way of redemption of a half-monthly payment or otherwise, or an agreement as to the amount of compensation payable [268](#note_268 "268. Subs. by Act 5 of 1929, S. 6 for  to a person under any legal disability .")\[to a woman or a person under a legal disability\] [269](#note_269 "269. The words  or to any dependant  repealed by Act 7 of 1924, S. 3 and Sch. II.")\[\* \* \*\] ought not to be registered by reason of the inadequacy of the sum or amount, or by reason of the agreement having been obtained by fraud or undue influence or other improper means, he may refuse to record the memorandum of the agreement [270](#note_270 "270. Subs. by Act 7 of 1924, S. 2 and Sch. I, for  or may make such order .")\[and may make such order\] including an order as to any sum already paid under the agreement, as he thinks just in the circumstances.

(2) An agreement for the payment of compensation which has been registered under sub-section (1) shall be enforceable under this Act notwithstanding anything contained in the Indian Contract Act, 1872 (9 of 1872), or in any other law for the time being in force.

Section 29. Effect of failure to register agreement
---------------------------------------------------

Where a memorandum of any agreement, the registration of which is required by Section 28, is not sent to the Commissioner as required by that section, the employer shall be liable to pay the full amount of compensation which he is liable to pay under the provisions of this Act, and notwithstanding anything contained in the proviso to sub-section (1) of Section 4, shall not, unless the Commissioner otherwise directs, be entitled to deduct more than half of any amount paid to the [271](#note_271 "271. Subs. for  workmen  by Act 45 of 2009, S. 5 (w.e.f. 18-1-2010).")\[employees\] by way of compensation whether under the agreement or otherwise.

Section 30. Appeals
-------------------

(1) An appeal shall lie to the High Court from the following orders of a Commissioner, namely

(_a_) an order awarding as compensation a lump sum whether by way of redemption of a half-monthly payment or otherwise or disallowing a claim in full or in part for a lump sum;

[272](#note_272 "272. Ins. by Act 8 of 1959, S. 15 (w.e.f. 1-6-1959).")\[(_aa_) an order awarding interest or penalty under Section 4-A;\]

(_b_) an order refusing to allow redemption of a half-monthly payment;

(_c_) an order providing for the distribution of compensation among the dependants of a deceased [273](#note_273 "273. Subs. for  workman  by Act 45 of 2009, S. 5 (w.e.f. 18-1-2010).")\[employee\], or disallowing any claim of a person alleging himself to be such dependant;

(_d_) an order allowing or disallowing any claim for the amount of an indemnity under the provisions of sub-section (2) of Section 12; or

(_e_) an order refusing to register a memorandum of agreement or registering the same or providing for the registration of the same subject to conditions:

Provided that no appeal shall lie against any order unless a substantial question of law is involved in the appeal and, in the case of an order other than an order such as is referred to in clause (_b_), unless the amount in dispute in the appeal is not less than [274](#note_274 "274. Subs. for  three hundred rupees  by Act 11 of 2017, S. 4 (w.e.f. 15-5-2017).")\[ten thousand rupees or such higher amount as the Central Government may, by notification in the Official Gazette, specify\]:

Provided further that no appeal shall lie in any case in which the parties have agreed to abide by the decision of the Commissioner, or in which the order of the Commissioner gives effect to an agreement come to by the parties:

[275](#note_275 "275. Added by Act 15 of 1933, S. 17.")\[Provided further that no appeal by an employer under clause (_a_) shall lie unless the memorandum of appeal is accompanied by a certificate by the Commissioner to the effect that the appellant has deposited with him the amount payable under the order appealed against.\]

(2) The period of limitation for an appeal under this section shall be sixty days.

(3) The provisions of Section 5 of [276](#note_276 "276. Subs. by Act 30 of 1995 (w.e.f. 15-9-1995).")\[the Limitation Act, 1963 (36 of 1963)\], shall be applicable to appeals under this section.

**30-A. Withholding of certain payments pending decision of appeal**. [277](#note_277 "277. Omitted by Act 11 of 2017, S. 5 (w.e.f. 15-5-2017). Prior to omission it read as: 30-A. Withholding of certain payments pending decision of appeal. Where an employer makes an appeal under clause (a) of sub-section (1) of Section 30, the Commissioner may, and if so directed by the High Court shall, pending the decision of the appeal, withhold payment of any sum in deposit with him. ")\[\* \* \*\]

Section 31. Recovery
--------------------

The Commissioner may recover as an arrear of land-revenue any amount payable by any person under this Act, whether under an agreement for the payment of compensation or otherwise, and the Commissioner shall be deemed to be a public officer within the meaning of Section 5 of the Revenue Recovery Act, 1890 (1 of 1890).

STATE AMENDMENTS

SECTION 31-A

**West Bengal**. In its application to the State of West Bengal, after Section 31 the following section shall be _inserted_, namely:

31-A. _Application of Section 36 of Act XVIII of 1879 to touts in office of the Commissioner_. (1) The provisions of Section 36 of the Legal Practitioners Act, 1879, shall, subject to the provisions of this section, be applicable, so far as may be, to the framing and publication of a list of touts, to the exclusion of touts included in the list from the precincts of the court of the Commissioner and to the arrest, detention, trial and punishment of such touts.

(2) A Commissioner shall, for the purposes of the said Section 36, be deemed to be an authority referred to in sub-section (1) of that section.

(3) For the purposes of this Section tout means

(_a_) a tout as defined in clause (_a_) of Section 3 of the Legal Practitioners Act, 1879; or

(_b_) a person who habitually frequents the precincts of the court of a Commissioner

(_i_) for the purpose of procuring work as an agent under Section 24, or

(_ii_) otherwise than as a party to or a witness in any proceedings before the Commissioner or as a _bona fide_ agent appointed under Section 24. _Vide_ Bengal Act 5 of 1942, S. 12 (w.e.f. 1-11-1943).

Chapter IV

RULES

Section 32. Power of the State Government to make rules
-------------------------------------------------------

(1) The [278](#note_278 "278. The words  G.-G. in C.  successively subs. by the A. O. 1937 and the A. O. 1950, to read as above.")\[State Government\] may make rules to carry out the purposes of this Act.

(2) In particular and without prejudice to the generality of the foregoing power, such rules may provide for all or any of the following matters, namely

(_a_) for prescribing the intervals at which and the conditions subject to which an application for review may be made under Section 6 when not accompanied by a medical certificate;

(_b_) for prescribing the intervals at which and the conditions subject to which a [279](#note_279 "279. Subs. for  workman  by Act 45 of 2009, S. 5 (w.e.f. 18-1-2010).")\[employee\] may be required to submit himself for medical examination under sub-section (1) of Section 11;

(_c_) for prescribing the procedure to be followed by Commissioners in the disposal of cases under this Act and by the parties in such cases;

(_d_) for regulating the transfer of matters and cases from one Commissioner to another and the transfer of money in such cases;

(_e_) for prescribing the manner in which money in the hands of a Commissioner may be invested for the benefit of dependants of a deceased [280](#note_280 "280. Subs. for  workman  by Act 45 of 2009, S. 5 (w.e.f. 18-1-2010).")\[employee\] and for the transfer of money so invested from one Commissioner to another;

(_f_) for the representation in proceedings before Commissioners of parties who are minor or are unable to make an appearance;

(_g_) for prescribing the form and manner in which memoranda of agreements shall be presented and registered;

(_h_) for the withholding by Commissioners, whether in whole or in part of half-monthly payments pending decision on applications for review of the same; [281](#note_281 "281. The words  and  at the end of cl. (h) and the original cl. (i) omitted by the A. O. 1937.")\[\* \* \*\]

[282](#note_282 "282. The original cl. (i) omitted by the A. O. 1937.")\[\* \* \*\]

[283](#note_283 "283. Cls. (a) to (f) of S. 33 after being relettered as cls. (i) to (n) respectively were added to S. 32 and the rest of S. 33 was omitted by the A. O. 1937.")\[(_i_) for regulating the scales of costs which may be allowed in proceedings under this Act;

(_j_) for prescribing and determining the amount of the fees payable in respect of any proceedings before a Commissioner under this Act;

(_k_) for the maintenance by Commissioners of registers and records of proceedings before them;

(_l_) for prescribing the classes of employers who shall maintain notice-books under sub-section (3) of Section 10, and the form of such notice-books;

(_m_) for prescribing the form of statement to be submitted by employers under Section 10-A; [284](#note_284 "284. The word  and  ins. by the A. O. 1937, omitted by Act 58 of 1960, S. 3 and Sch. II.")\[\* \* \*\]

(_n_) for prescribing the cases in which the report referred to in Section 10-B may be sent to an authority other than the Commissioner;\]

[285](#note_285 "285. Ins. by Act 8 of 1959, S. 16 (w.e.f. 1-6-1959).")\[(_o_) for prescribing abstracts of this Act and requiring the employers to display notices containing such abstracts;

(_p_) for prescribing the manner in which diseases specified as occupational diseases may be diagnosed;

(_q_) for prescribing the manner in which diseases may be certified for any of the purposes of this Act;

(_r_) for prescribing the manner in which, and the standards by which, incapacity may be assessed.\]

[286](#note_286 "286. Ins. by Act 4 of 1986 (w.e.f. 15-5-1986).")\[(3) Every rule made under this section shall be laid, as soon as may be after it is made, before the State Legislature.\]

List of Rules under Workmen's Compensation Act, 1923 made by State Government and Union Territories

1\. Andaman and Nicobar Islands Workmen's Compensation Rules, 1966

2\. Andhra Pradesh Workmen's Compensation Rules, 1953

3\. Andhra Pradesh Workmen's Compensation (Occupational Diseases Pneumoconiosis) Rules, 1965

4\. Bihar Workmen's Compensation Rules, 1924

5\. Bihar Workmen's Compensation (Occupational Diseases) Rules, 1962

6\. Bombay Workmen's Compensation Rules, 1934

7\. Bombay Workmen's Compensation (Unclaimed Deposits) Rules, 1941

8\. Maharashtra Workmen's Compensation (Occupational Diseases) Rules, 1963

9\. Goa, Daman and Diu Workmen's Compensation Rules, 1965

10\. Gujarat Workmen's Rules, 1967

11\. Gujarat Workmen's Compensation (Unclaimed Deposits) Rules, 1967

12\. Himachal Pradesh Workmen's Compensation Rules, 1951

13\. Himachal Pradesh Workmen's Compensation (Occupational Diseases) Rules, 1979

14\. Jammu and Kashmir Workmen's Compensation Rules, 1972

15\. Karnataka Workmen's Compensation Rules, 1966

16\. Karnataka Workmen's Compensation (Occupational Diseases) Rules, 1968

17\. Kerala Workmen's Compensation Rules, 1958

18\. Kerala Workmen's Compensation (Occupational Diseases) Rules, 1962

19\. Workmen's Compensation (Madhya Pradesh) Rules, 1962

20\. Madhya Pradesh Workmen's Compensation (Occupational Diseases) Rules, 1963

21\. Mysore Workmen's Compensation Rules, 1966

22\. Mysore Workmen's Compensation (Occupational Diseases) Rules, 1968

23\. Orissa Workmen's Compensation (Occupational Diseases) Rules, 1964

24\. Pondicherry Workmen's Compensation Rules, 1954

25\. Pondicherry Workmen's Compensation (Occupational Diseases) Rules, 1965

26\. Rajasthan Silicosis Rules, 1955

27\. Rajasthan Workmen's Compensation (Unclaimed Deposits) Rules, 1959

28\. Rajasthan Workmen's Compensation (Cost and Fees) Rules, 1959

29\. Rajasthan Workmen's Compensation Rules, 1960

30\. Rajasthan Workmen's Compensation (Occupational Diseases) Rules, 1965

31\. Tamil Nadu Workmen's Compensation Rules, 1924

32\. Tamil Nadu Workmen's Compensation (Occupational Diseases) Rules, 1964

33\. Uttar Pradesh Workmen's Compensation Rules, 1975

34\. Uttar Pradesh Workmen's Compensation (Occupational Diseases) Rules, 1964

35\. West Bengal Workmen's Compensation Rules, 1924

36\. West Bengal Workmen's Compensation (Occupational Diseases) Rules, 1962

STATE AMENDMENTS

**West Bengal**. In its application to the State of West Bengal, after clause (_f_) of sub-section (2) of Section 32 of the said Act the following clauses shall be _inserted_, namely:

(_ff_) for prescribing the procedure relating to the reference of medical questions to medical referees under sub-section (1) of Section 24-A.

(_ff1_) for regulating the procedure relating to the medical examination of a workman by or under the personal direction of a medical referee and the submission of the report of such medical referee, under sub-section (2) of Section 24-A;

(_ff2_) for prescribing and determining the fees and expenses payable in connection with references of medical questions to medical referees under sub-section (1) of Section 24-A. _Vide_ Bengal Act 6 of 1942, S. 5.

Section 33. Power of Local Government to make rules
---------------------------------------------------

\[_Rep. by the A. O. 1937_\].

Section 34. Publication of rules
--------------------------------

(1) The power to make rules conferred by [287](#note_287 "287. Subs. by the A. O. 1937 for  Ss. 32 and 33 .")\[Section 32\] shall be subject to the condition of the rules being made after previous publication.

(2) The date to be specified in accordance with clause (3) of Section 23 of the General Clauses Act, 1897 (10 of 1897), as that after which a draft of rules proposed to be made under Section 32 [288](#note_288 "288. The words and figures  or S. 33  omitted by the A. O. 1937.")\[\* \* \*\] will be taken into consideration, shall not be less than three months from the date on which the draft of the proposed rules was published for general information.

(3) Rules so made shall be published in [289](#note_289 "289. The words  the Gazette of India or  omitted by the A. O. 1937.")\[\* \* \*\] the Official Gazette [290](#note_290 "290. The words  as the case may be  omitted by the A. O. 1937.")\[\* \* \*\] and, on such publication, shall have effect as if enacted in this Act.

Section 35. Rules to give effect to arrangements with other countries for the transfer of money paid as compensation
--------------------------------------------------------------------------------------------------------------------

[291](#note_291 "291. Ins. by Act 15 of 1933, S. 20.")\[[292](#note_292 "292. S. 35 renumbered as sub-section (1) of that section by Act 7 of 1937, S. 2.")\[(1)\] The Central Government may, by notification in the Official Gazette, make rules for the transfer [293](#note_293 "293. The words and letter  to any Part B State or  omitted by Act 3 of 1951, S. 3 and Sch.")\[\* \* \*\] [294](#note_294 "294. Subs. by Act 22 of 1984, S. 5 (w.e.f. 1-7-1984).")\[to any foreign country\] of money [295](#note_295 "295. Subs. by Act 7 of 1937, S. 2, for  paid to .")\[deposited with\] a Commissioner under this Act [296](#note_296 "296. Subs. by Act 7 of 1937, S. 2, for  for the benefit of .")\[which has been awarded to or may be due to\], any person residing or about to reside in [297](#note_297 "297. Subs. by Act 22 of 1984, S. 5 (w.e.f. 1-7-1984).")\[such foreign country\] and for the receipt [298](#note_298 "298. Ins. by Act 7 of 1937, S. 2.")\[, distribution\] and administration in [299](#note_299 "299. Subs. by Act 3 of 1951, S. 3 and Sch. for  a Part A State or Part C State .")\[any State\] of any money [300](#note_300 "300. Subs. by Act 7 of 1937, S. 2, for  awarded .")\[deposited\] under the law relating to [301](#note_301 "301. Subs. for  workmen's  by Act 45 of 2009, S. 5 (w.e.f. 18-1-2010).")\[employees'\] compensation [302](#note_302 "302. The words and letter  in any Part B State  omitted by Act 3 of 1951, S. 3 and Sch.")\[\* \* \*\] [303](#note_303 "303. The word  or  omitted by Act 36 of 1957, S. 3 and Sch. II.")\[\* \* \*\] [304](#note_304 "304. Subs. by Act 22 of 1984, S. 5 (w.e.f. 1-7-1984).")\[in any foreign country\], [305](#note_305 "305. Subs. by Act 7 of 1937, S. 2, for  and applicable for the benefit of .")\[which has been awarded to, or may be due to\] any person residing or about to reside in [306](#note_306 "306. Subs. by Act 3 of 1951, S. 3 and Sch. for  a Part A State or Part C State .")\[any State\]:

[307](#note_307 "307. Added by Act 7 of 1937, S. 2.")\[Provided that no sum deposited under this Act in respect of fatal accidents shall be so transferred without the consent of the employer concerned until the Commissioner receiving the sum has passed orders determining its distribution and apportionment under the provisions of sub-sections (4) and (5) of Section 8.

(2) Where money deposited with a Commissioner has been so transferred in accordance with the rules made under this section, the provisions elsewhere contained in this Act regarding distribution by the Commissioner of compensation deposited with him shall cease to apply in respect of any such money.\]

Section 36. Rules made by Central Government to be laid before Parliament
-------------------------------------------------------------------------

[308](#note_308 "308. Added by Act 64 of 1962.")\[Every rule made under this Act by Central Government shall be laid as soon as may be after it is made before each House of Parliament while it is in session for a total period of thirty days which may be comprised in one session or in [309](#note_309 "309. Subs. by Act 65 of 1976 (w.e.f. 21-5-1976).")\[two or more successive sessions, and if, before the expiry of the session immediately following the session or the successive sessions aforesaid\], both Houses agree in making any modification in the rule or both Houses agree that the rule should not be made, the rule shall thereafter have effect only in such modified form or be of no effect, as the case may be; so however that any such modification or annulment shall be without prejudice to the validity of anything previously done under this rule.\]

[310](#note_310 "310. Subs. by Act 8 of 1959, S. 17, for the original Sch. (w.e.f. 1-6-1959).")\[**SCHEDULE I**

\[_See_ Sections 2(1) and 4\]

[311](#note_311 "311. Subs. by Act 64 of 1962.")\[**Part I**

_**List of Injuries Deemed to Result in Permanent Total Disablement**_\]

  

Serial No.

Description of injury

Percentage of loss of earning capacity

1.

Loss of both hands or amputation at higher sites

100

2.

Loss of hand and a foot

100

3.

Double amputation through leg or thigh, or amputation through leg or thigh on one side and loss of other foot

100

4.

Loss of sight to such an extent as to render the claimant unable to perform any work for which eyesight is essential

100

5.

Very severe facial disfigurement

100

6.

Absolute deafness

100

[312](#note_312 "312. A new heading inserted and entries at Serial Nos. 7 to 54 renumbered as Serial Nos. 1 to 48 by Act 64 of 1962.")\[**Part II**

_**List of Injuries Deemed to Result in Permanent Partial Disablement**_\]

_Amputation cases upper limbs_ (_either arm_)

  

Serial No.

Description of injury

Percentage of loss of earning capacity

1.

Amputation through shoulder joint

90

2.

Amputation below shoulder with stump less than \[20.32 cms\][313](#note_313 "313. Subs. by Act 30 of 1995, S. 14 (w.e.f. 15-9-1995).") from tip of acromion

80

3.

Amputation from \[20.32 cms\][314](#note_314 "314. Subs. by Act 30 of 1995, S. 14 (w.e.f. 15-9-1995).") from tip of acromion to less than \[11.43 cms\][315](#note_315 "315. Subs. by Act 30 of 1995, S. 14 (w.e.f. 15-9-1995).") below tip of olecranon

70

4.

Loss of hand or of the thumb and four fingers of one hand or amputation from \[11.43 cms\][316](#note_316 "316. Subs. by Act 30 of 1995, S. 14 (w.e.f. 15-9-1995).") below tip of olecranon

60

5.

Loss of thumb

30

6.

Loss of thumb and its metacarpal bone

40

7.

Loss of four fingers of one hand

50

8.

Loss of three fingers of one hand

30

9.

Loss of two fingers of one hand

20

10.

Loss of terminal phalanx of thumb

20

[317](#note_317 "317. Ins. by Act 30 of 1995, S. 14 (w.e.f. 15-9-1995).")\[10-A.

Guillotine amputation of tip of thumb without loss of bone

10\]

_Amputation cases_ _lower limbs_

11.

Amputation of both feet resulting in end-bearing stumps

90

12.

Amputation through both feet proximal to the metatarso-phalangeal joint

80

13.

Loss of all toes of both feet through the metatarso-phalangeal joint

40

14.

Loss of all toes of both feet proximal to the proximal inter-phalangeal joint

30

15.

Loss of all toes of both feet distal to the proximal inter-phalangeal joint

20

16.

Amputation at hip

90

17.

Amputation below hip with stump not exceeding [318](#note_318 "318. Subs. by Act 30 of 1995, S. 14 (w.e.f. 15-9-1995).")\[12.70 cms\] in length measured from tip of great trenchanter

80

18.

Amputation below hip with stump exceeding [319](#note_319 "319. Subs. by Act 30 of 1995, S. 14 (w.e.f. 15-9-1995).")\[12.70 cms\] in length measured from tip of great trenchanter but not beyond middle thigh

70

19.

Amputation below middle thigh to [320](#note_320 "320. Subs. by Act 30 of 1995, S. 14 (w.e.f. 15-9-1995).")\[8.89 cms\] below knee

60

20.

Amputation below knee with stump exceeding [321](#note_321 "321. Subs. by Act 30 of 1995, S. 14 (w.e.f. 15-9-1995).")\[8.89 cms\] but not exceeding [322](#note_322 "322. Subs. by Act 30 of 1995, S. 14 (w.e.f. 15-9-1995).")\[12.70 cms\]

50

21.

Amputation below knee with stump exceeding [323](#note_323 "323. Subs. by Act 30 of 1995, S. 14 (w.e.f. 15-9-1995).")\[12.70 cms\]

\[50\][324](#note_324 "324. Subs. by Act 30 of 1995, S. 14 (w.e.f. 15-9-1995).")

22.

Amputation of one foot resulting in end-bearing

\[50\][325](#note_325 "325. Subs. by Act 30 of 1995, S. 14 (w.e.f. 15-9-1995).")

23.

Amputation through one foot proximal to the metatarso-phalangeal joint

\[50\][326](#note_326 "326. Subs. by Act 30 of 1995, S. 14 (w.e.f. 15-9-1995).")

24.

Loss of all toes of one foot through the metatarso-phalangeal joint

20

_Other injuries_

25.

Loss of one eye, without complications, the other being normal

40

26.

Loss of vision of one eye, without complications or disfigurement of eye-ball, the other being normal

30

Loss of

[327](#note_327 "327. Ins. by Act 30 of 1995, S. 14 (w.e.f. 15-9-1995).")\[26-A.

Loss of partial vision of one eye

10\]

A. _Fingers of right or left hand_

Index finger

27.

Whole

14

28.

Two phalanges

11

29.

One phalanx

9

30.

Guillotine amputation of tip without loss of bone

5

_Middle finger_

31.

Whole

12

32.

Two phalanges

9

33.

One phalanx

7

34.

Guillotine amputation of tip without loss of bone

4

_Ring or little finger_

35.

Whole

7

36.

Two phalanges

6

37.

One phalanx

5

38.

Guillotine amputation of tip without loss of bone

2

B. _Toes of right or left foot Great toe_

39.

Through metatarso-phalangeal joint

14

40.

Part, with some loss of bone

3

_Any other toe_

41.

Through metatarso-phalangeal joint

3

42.

Part, with some loss of bone

1

_Two toes of one foot, excluding great toe_

43.

Through metatarso-phalangeal joint

5

44.

Part, with some loss of bone

2

_Three toes of one foot, excluding great toe_

45.

Through metatarso-phalangeal joint

6

46.

Part, with some loss of bone

3

_Four toes of one foot, excluding great toe_

47.

Through metatarso-phalangeal joint

9

48.

Part, with some loss of bone

3

[328](#note_328 "328. Added by Act 58 of 1960, S. 3 and Sch. II.")\[**Note**. Complete and permanent loss of the use of any limb or member referred to in this Schedule shall be deemed to be the equivalent of the loss of that limb or member.\]

SCHEDULE II

\[_See_ [329](#note_329 "329. Subs. for  Section 2(1)(n)  by Act 45 of 2009, S. 10 (w.e.f. 18-1-2010).")\[Section 2(1)(_dd_)\])

List of Persons who, subject to the provisions of 330\[Section 2(1)(dd)\], are included in the definition of 331\[employees\]

The following persons are [332](#note_332 "332. Subs. for  workmen  by Act 45 of 2009, S. 5 (w.e.f. 18-1-2010).")\[employees\] within the meaning of [333](#note_333 "333. Subs. for  Section 2(1)(n)  by Act 45 of 2009, S. 10 (w.e.f. 18-1-2010).")\[Section 2(1)(_dd_)\] and subject to the provisions of that section, that is to say, any person who is

[334](#note_334 "334. Subs. by Act 15 of 1933, S. 21, for the former cls. (i) to (xiii).")\[[335](#note_335 "335. Subs. by Act 8 of 1959, S. 18, for cls. (i) to (ix)(w.e.f. 1-6-1959).")\[(_i_) [336](#note_336 "336. Subs. for  employed, otherwise than in a clerical capacity or on a railway  by Act 45 of 2009, S. 10 (w.e.f. 18-1-2010).")\[employed in railways\] in connection with the operation, [337](#note_337 "337. Ins. by Act 30 of 1995, S. 15 (w.e.f. 15-9-1995).")\[, repair\] or maintenance of a lift or a vehicle propelled by steam or other mechanical power or by electricity or in connection with the loading or unloading of any such vehicle; or

(_ii_) employed, [338](#note_338 "338. The words  otherwise than in a clerical capacity  omitted by Act 45 of 2009, S. 10 (w.e.f. 18-1-2010).")\[\* \* \*\] in any premises wherein or within the precincts whereof a manufacturing process as defined in clause (_k_) of Section 2 of the Factories Act, 1948 (63 of 1948), is being carried on, or in any kind of work whatsoever incidental to or connected with any such manufacturing process or with the article made, [339](#note_339 "339. Added by Act 64 of 1962.")\[whether or not employment in any such work is within such premises or precincts\] and steam, water or other mechanical power or electrical power is used; or

(_iii_) employed for the purpose of making, altering, repairing, ornamenting, finishing or otherwise adapting for use, transport or sale of any article or part of an article in any premises [340](#note_340 "340. The words  wherein or within the precincts whereof twenty or more persons are so employed  omitted by Act 45 of 2009, S. 10 (w.e.f. 18-1-2010).")\[\* \* \*\]; \[\* \* \*\][341](#note_341 "341. The word  or  omitted by Act 64 of 1962.")

[342](#note_342 "342. Added by Act 64 of 1962.")\[_Explanation_. For the purposes of this clause, persons employed outside such premises or precincts but in any work incidental to, or connected with, the work relating to making, altering, repairing, ornamenting, finishing or otherwise adapting for use, transport or sale any article or part of an article shall be deemed to be employed within such premises or precincts; or\]

(_iv_) employed in the manufacture or handling of explosives in connection with the employer's trade or business; or

(_v_) employed in any mine as defined in clause (_j_) of Section 2 of the Mines Act, 1952 (35 of 1952), in any mining operating or in any kind of work, [343](#note_343 "343. The words  other than clerical work  omitted by Act 45 of 2009, S. 10 (w.e.f. 18-1-2010).")\[\* \* \*\] incidental to or connected with any mining operation or with mineral obtained, or in any kind of work whatsoever below ground; or

(_vi_) employed as the master or as a seaman of

(_a_) any ship which is propelled wholly or in part by steam or other mechanical power or by electricity or which is towed or intended to be towed by a ship so propelled; or

(_b_) [344](#note_344 "344. Clause (b) omitted by Act 45 of 2009, S. 10 (w.e.f. 18-1-2010). Prior to omission it read as: (b) any ship not included in sub-clause (a) of twenty-five tons net tonnage or over; or ")\[\* \* \*\]

(_c_) any sea-going ship not included in sub-clause (_a_) [345](#note_345 "345. The words  or sub-clause (b)  omitted by Act 45 of 2009, S. 10 (w.e.f. 18-1-2010).")\[\* \* \*\] provided with sufficient area for navigation under sails alone; or

(_vii_) employed for the purpose of

(_a_) loading, unloading, fuelling, constructing, repairing, demolishing, cleaning or painting any ship of which he is not the master or a member of the crew, or handling or transport within the limits of any port subject to [346](#note_346 "346. Subs. by Act 30 of 1995, S. 15 (w.e.f. 15-9-1995).")\[the Ports Act, 1908 (15 of 1908) or the Major Port Trusts Act, 1963 (38 of 1963)\], of goods which have been discharged from or are to be loaded into any vessel; or

(_b_) warping a ship through the lock; or

(_c_) mooring and unmooring ships at harbour wall berths or in pier; or

(_d_) removing or replacing dry dock caisoons when vessels are entering or leaving dry docks; or

(_e_) the docking or undocking of any vessel during an emergency; or

(_f_) preparing splicing coir springs and check wires, painting depth marks on lock-sides, removing or replacing fenders whenever necessary, landing of gangways, maintaining life-buoys upto standard or any other maintenance work of a like nature; or

(_g_) any work on jolly-boats for bringing a ship's line to the wharf; or

(_viii_) employed in the construction, maintenance, repair or demolition of

(_a_) any building which is designed to be or is or has been more than one storey in height above the ground or twelve feet or more from the ground level to the apex of the roof; or

(_b_) any dam or embankment which is twelve feet or more in height from its lowest to its highest point; or

(_c_) any road, bridge, tunnel or canal; or

(_d_) any wharf, quay, sea-wall or other marine work including any moorings of ships; or

(_ix_) employed in setting up, maintaining, repairing or taking down any telegraph or telephone line or post or any overhead electric line or cable or post or standard or fittings and fixtures for the same; or\]

(_x_) employed, [347](#note_347 "347. The words  otherwise than in a clerical capacity  omitted by Act 45 of 2009, S. 10 (w.e.f. 18-1-2010).")\[\* \* \*\] in the construction, working, repair or demolition of any aerial ropeway, canal, pipe-line, or sewer; or

(_xi_) employed in the service of any fire brigade; or

(_xii_) employed upon a railway as defined in [348](#note_348 "348. Subs. by Act 30 of 1995, S. 15 (w.e.f. 15-9-1995).")\[clause (31) of Section 2 and sub-section (1) of Section 197 of the Railways Act, 1989 (24 of 1989)\] either directly or through a sub-contractor, by a person fulfilling a contract with the Railway administration; or

(_xiii_) employed as an inspector, mail guard, sorter or van peon in the Railway Mail Service, [349](#note_349 "349. Ins. by Act 8 of 1959, S. 18 (w.e.f. 1-6-1959).")\[or as a telegraphist or as a postal or railway signaller\], or employed in any occupation ordinarily involving outdoor work in the Indian Posts and Telegraphs Department; or

(_xiv_) employed [350](#note_350 "350. The words  otherwise than in a clerical capacity  omitted by Act 45 of 2009, S. 10 (w.e.f. 18-1-2010).")\[\* \* \*\] in connection with operation for winning natural petroleum or natural gas; or

(_xv_) employed in any occupation involving blasting operations; or

(_xvi_) employed in the making of any excavation [351](#note_351 "351. The words  in which on any one day of the preceding twelve months more than twenty-five persons have been employed  omitted by Act 45 of 2009, S. 10 (w.e.f. 18-1-2010).")\[\* \* \*\] or explosives have been used, or whose depth from its highest to its lowest point exceeds [352](#note_352 "352. Subs. by Act 8 of 1959, S. 18, for  twenty  (w.e.f. 1-6-1959).")\[twelve\] feet; or

(_xvii_) employed in the operation of any ferry boat capable of carrying more than ten persons; or

[353](#note_353 "353. Subs. by Act 45 of 2009, S. 10 (w.e.f. 18-1-2010).")\[(_xviii_) employed on any estate which is maintained for the purpose of growing cardamom cinchona, coffee, rubber or tea; or\]

[354](#note_354 "354. Subs. by Act 30 of 1995, S. 15 (w.e.f. 15-9-1995).")\[(_xix_) employed, [355](#note_355 "355. The words  otherwise than in a clerical capacity  omitted by Act 45 of 2009, S. 10 (w.e.f. 18-1-2010).")\[\* \* \*\] in the generating, transforming, transmitting or distribution of electrical energy or in generation or supply of gas; or\];

(_xx_) employed in a lighthouse as defined in clause (_d_) of Section 2 of the Indian Lighthouse Act, 1927 (17 of 1927); or

(_xxi_) employed in producing cinematograph pictures intended for public exhibition or in exhibiting such pictures; or

(_xxii_) employed in the training, keeping or working of elephants or wild animals; or

[356](#note_356 "356. Ins. by Act 9 of 1938, S. 11.")\[(_xxiii_) employed in the tapping of palm-trees or the felling or logging of trees, or the transport of timber by inland waters, or the control or extinguishing of forest fires; or

(_xxiv_) employed in operations for the catching or hunting of elephants or other wild animals; or\]

[357](#note_357 "357. Original clause (xxiii) re-numbered (xxv) by Act 9 of 1938, S. 11.")\[(_xxv_)\] employed as a driver; [358](#note_358 "358. Ins. by Act 9 of 1938, S. 11.")\[or

(_xxvi_) employed in the handling or transport of goods in, or within the precincts of,

(_a_) any warehouse or other place in which goods are stored [359](#note_359 "359. The words  and in which on any one day of the preceding twelve months ten or more persons have been so employed  omitted by Act 45 of 2009, S. 10 (w.e.f. 18-1-2010).")\[\* \* \*\]; or

(_b_) any market [360](#note_360 "360. The words  in which on any one day of the preceding twelve months fifty or more persons have been so employed  omitted by Act 45 of 2009, S. 10 (w.e.f. 18-1-2010).")\[\* \* \*\]; or

(_xxvii_) employed in any occupation involving the handling and manipulation of radium or X-rays apparatus, or contact with radioactive substances\]; [361](#note_361 "361. Ins. by Act 8 of 1959, S. 18 (w.e.f. 1-6-1959).")\[or

(_xxviii_) employed in or in connection with the construction, erection, dismantling, operation or maintenance of an aircraft as defined in Section 2 of the Indian Aircraft Act, 1934 (22 of 1934); or

(_xxix_) [362](#note_362 "362. Subs. by Act 30 of 1995, S. 15 (w.e.f. 15-9-1995).")\[employed in horticultural operations, forestry, bee-keeping or farming\] by tractors or other contrivances driven by steam or other mechanical power or by electricity; or

(_xxx_) employed, [363](#note_363 "363. The words  otherwise than in a clerical capacity  omitted by Act 45 of 2009, S. 10 (w.e.f. 18-1-2010).")\[\* \* \*\] in the construction, working, repair or maintenance of a tube-well; or

(_xxxi_) employed in the maintenance, repair or renewal of electric fittings in any building; or

(_xxxii_) employed in a circus.\]

[364](#note_364 "364. Ins. by Act 30 of 1995, S. 15 (w.e.f. 15-9-1995).")\[(_xxxiii_) employed as watchman in any factory or establishment; or

(_xxxiv_) employed in any operation in the sea for catching fish; or

(_xxxv_) employed in any employment which requires handling of snakes for the purpose of extraction of venom or for the purpose of looking after snakes or handling any other poisonous animal or insect; or

(_xxxvi_) employed in handling animals like horses, mules and bulls; or

(_xxxvii_) employed for the purpose of loading or unloading of any mechanically propelled vehicle or in the handling or transport of goods which have been loaded in such vehicles; or

(_xxxviii_) employed in cleaning of sewer lines or septic tanks within the limits of a local authority; or

(_xxxix_) employed on surveys and investigation, exploration or gauge or discharge observation of rivers including drilling operations, hydrological observations and flood forecasting activities, ground water surveys and exploration; or

(_xl_) employed in cleaning of jungles or reclaiming land or ponds [365](#note_365 "365. The words  in which on any one day of the preceding twelve months more than twenty-five persons have been employed  omitted by Act 45 of 2009, S. 10 (w.e.f. 18-1-2010).")\[\* \* \*\]; or

(_xli_) employed in cultivation of land or rearing and maintenance of live-stock or forest operations or fishing [366](#note_366 "366. The words  in which on any one day of the preceding twelve months more than twenty-five persons have been employed  omitted by Act 45 of 2009, S. 10 (w.e.f. 18-1-2010).")\[\* \* \*\]; or

(_xlii_) employed in installation, maintenance or repair of pumping equipment used for lifting of water from wells, tubewells, ponds, lakes, streams and the like; or

(_xliii_) employed in the construction boring or deepening of an open well or dug well, bore well, bore-cum-dug well, filter-point and the like; or

(_xliv_) employed in spraying and dusting of insecticides or pesticides in agricultural operations or plantations; or

(_xlv_) employed in mechanised harvesting and threshing operations; or

(_xlvi_) employed in working or repair or maintenance of bulldozers, tractors, power tillers and the like; or

(_xlvii_) employed as artist for drawing pictures on advertisement boards at a height of 3.66 metres or more from the ground level; or

(_xlviii_) employed in any newspaper establishment as defined in the Working Journalists and Other Newspaper Employees (Conditions of Service) and Miscellaneous Provisions Act, 1955 and engaged in outdoor work.\]

[367](#note_367 "367. Ins. by G.S.R. 381, dt. 3-11-1987.")\[(_xlix_) employed as divers for work under water.\]

_Explanation_. [368](#note_368 "368. Omitted by Act 45 of 2009, S. 10 (w.e.f. 18-1-2010). Prior to omission it read as: Explanation. In this Schedule,  the preceding twelve months  relates in any particular case to the twelve months ending with the day on which the accident in such case occurred. ")\[\* \* \*\]

STATE AMENDMENTS

**Andhra Pradesh**. In its application to the State of Andhra Pradesh following clauses are _added_ after Schedule II as under:

**1.** _xl._ Employed in cook houses, messes, bakeries or catering establishments in which food for over 30 persons is prepared or more than 200 loaves of bread per day are prepared or training to at least 10 cooks or caterers is imparted at any time.

_xli._ Employed in any employment which requires handling of snakes for the purpose of extraction of venom or for the purpose of looking after snakes or handling any other poisonous insects or animal.

_xlii._ Employed in training, keeping or working with animals like horses, mules, bulls, etc.

_xliii._ Employed in cleaning of sewers or septic tanks within the limits of a local authority.

_xliv._ Employed in any operation in the sea for catching of fish. _Vide_ A.P. Gaz., 23-2-1989, Pt. I, p. 167

**2.** _xlvii._ Employed in any newspaper establishment as defined in Working Journalists and Other Newspaper Employees (Conditions of Service) and Miscellaneous Provisions Act, 1955 and engaged in outdoor duties . _Vide_ A.P. Gaz., 16-11-1995, Pt. I, p. 995.

**A.P.; Assam; Bihar; Delhi; Goa; Haryana; H.P.; J.&K.; Karnataka; Kerala; M.P.; Maharashtra; Orissa; Pondicherry; Punjab; Rajasthan and U.P.**. In application to the above mentioned States, after clause (_xxxii_) the following clause shall be _added_, namely:

(_xxxiii_) Employed otherwise than in a clerical capacity in the conduct of surveys in river valleys including collection of data relating to the river. _See_ A.P. Gaz., 10-10-1968, Pt. I, P. 1850; Assam Gaz., 24-5-1972, Pt. II-A, P. 999; Bihar Gaz., 1-1-1969, Pt. II, P. 5; Delhi Gaz., 8-7-1976, Pt. IV, P. 415 (No. 28); Goa Govt. Gaz., 2-1-1975, S. 1, No. 40, P. 693; Haryana Gaz., 6-8-1968, Pt. III (L.S.), Ext., P. 359; Ker. Gaz., 30-12-1969, Pt. I, P. 616, M.P. Gaz., 20-2-1970; Pt. I, P. 270; Maharashtra Gaz., 12-6-1969, Pt. I-L, P. 2908; Mys. Gaz., 15-5-1969, Pt. IV, S. 2-C(_ii_), P. 2249; Orissa Gaz., 7-11-1969, Pt. III, P. 1024, Punj. Gaz., 5-9-1969, Pt. III (L.S.), Raj. Gaz., 27-4-1970, Pt. IV (Ga), Ext., P. 13; U.P. Gaz., 20-6-1970, Pt. I, P. 2676.

**Assam**. In its application to the State of Assam,

**1.** (1) Sweepers, (2) Scavengers are _added_ to the list of persons included in this Schedule, w.e.f. 12-5-1970 _Vide_ Assam Gaz., 27-5-1970, Pt. II-A, p. 1851.

**2.** After Sch. II, the employments _added_ are as under:

The following persons are workmen within the meaning of Section 2(1)(_n_) of the Act and subject to the provisions of that Section, that is to say any person who is

(_i_) employed in clearing of jungles or reclaiming land or ponds in which on any one day of the preceding twelve months more than twenty-five persons have been employed;

(_ii_) employed in cultivation of land or rearing and maintenance of livestock or forest operation or fishing in which on any one day of the preceding twelve months more than twenty-five persons have been employed;

(_iii_) employed, other than in clerical capacity, in installation, maintenance, repair of pumping equipment used for lifting of water from wells, tube-wells, ponds, lakes, streams, etc.;

(_iv_) employed, otherwise than in clerical capacity in the construction, boring or deepening of an open well or dug-well through mechanical contrivances;

(_v_) employed otherwise than in clerical capacity in the construction, working, repair or maintenance of a bore well, bore-cum-dug-well, filter point, etc.;

(_vi_) employed in spraying and dusting of insecticides or pesticides in agricultural operation;

(_vii_) employed in working or repair or maintenance of bulldozers, tractors, power-tillers, power threshers, etc. _Vide_ Assam Gaz., 13-3-1977, Pt. II-A, P. 1284.

**Bihar**. In its application to the State of Bihar,

**1.** The following clauses shall be _inserted_ after clause (_xxxii_), namely:

(_xxxiii_) employed in cooking houses, messes, bakeries or catering establishments in which food for over 50 persons is prepared or more than 200 loaves of bread are prepared every day or training to at least 10 cooks/caterers is imparted at any time;

(_xxxiv_) employed in any employment in which workmen are exposed to low temperature conditions while working in high altitude;

(_xxxv_) employed in any employment which requires handling of snakes for the purpose of extraction of venom or for the purpose of looking after snakes or handling any other poisonous insect or animal;

(_xxxvi_) employed in training; keeping or working with animals like horses, mules, bulls, etc. _Vide_ Bihar Gaz., II, Pt. I, p. 369.

(_xxxvii_) employed in clearing of sewers or septic tanks within the limits of a local authority;

(_xxxviii_) employed in horticultural operation;

(_xxxix_) persons employed on surveys and investigation including drilling operations, Hydrological observations and flood forecasting activities, Ground matter surveys and exploration. _Vide_ Bihar Gaz., II, Pt. I, p. 369.

**2.** The following classes of persons are _added_ after Schedule II, namely:

(_a_) Employed in clearing of jungles or reclaiming land or ponds in which during the preceding twelve months more than twenty-five persons have generally been employed;

(_b_) Employed in cultivation of land or rearing and maintenance of livestock or forest operations or fishing in which during the preceding twelve months more than twenty-five persons have generally been employed;

(_c_) Employed, otherwise than in clerical capacity, in installation, maintenance or repair of pumping equipments used for lifting of water from wells, tube-wells, ponds, lakes, streams, etc. in which during the preceding twelve months more than twenty-five persons have generally been employed;

(_d_) Employed otherwise than in clerical capacity, in the construction, boring or deepening of an open well/dug-well through mechanical contrivances in which during the preceding twelve months mote than twenty-five persons have generally been employed;

(_e_) Employed otherwise than in clerical capacity in the construction, working, repair or maintenance of a bore-well, bore-cum-dug-well, filter point, etc. in which during the preceding twelve months more than twenty-five persons have generally been employed;

(_f_) Employed in spraying and dusting of insecticides or pesticides in agricultural operation or plantations in which during the preceding twelve months more than twenty-five persons have generally been employed;

(_g_) Employed in working or repair or maintenance of bulldozers, tractors, power-tillers, etc. in which during the preceding twelve months more than twenty-five persons have generally been employed. _Vide_ Bihar Gaz., 31-10-1977, Ext., p. 2.

**Delhi**. In its application to Delhi, the following clauses shall be _inserted_, namely:

**1.** (_xxxiii_) employed in cook houses, messes, bakeries or catering establishments in which food for over fifty persons is prepared or more than two hundred loaves of bread per day are prepared or training to at least ten cooks or caterers is imparted at any time;

(_xxxiv_) employed in the training, keeping or working with animals, like horses, mules, bulls etc.;

(_xxxv_) employed in cleaning of sewers or septic tanks within the limits of a local authority;

(_xxxvi_) employed in horticultural operations;

(_xxxvii_) employed in any employment which requires handling of snakes for the purpose of extraction of venom or for the purpose of looking after Snakes or handling any other poisonous insects or animals . _Vide_ Del. Gaz., 5-3-1990, Pt. IV, P. 1 (No. 34).

**2.** (_xxxviii_) Persons employed on surveys and investigation, including drilling operations, hydrological observation and flood forecasting activities, ground water surveys and exploration. _Vide_ Del. Gaz., 24-1-1991, Ext., P. 5 (No. 13).

**Goa**. In its application to the State of Goa, the following clauses shall be _inserted_, namely:

**1.** (_xxxiii_) employed in clearing of jungles or reclaiming land or ponds in which, on any one day of the preceding twelve months, more than twenty-five persons have been employed;

(_xxxiv_) employed in cultivation of land or rearing and maintenance of livestock or forest operations or fishing in which, on any one day of the preceding twelve months more than twenty-five persons have been employed;

(_xxxv_) employed, otherwise than in clerical capacity, in the installation, maintenance, repair of pumping equipment used for lifting of water from wells, tube-wells, ponds, lakes, streams, etc.;

(_xxxvi_) employed, otherwise than in clerical capacity, in the construction, boring or deepening of an open well/dug well through mechanical contrivances . _Vide_ J&K Gaz., 18-11-1991, Pt. I-B, Extra., p. 1(33-G).

**2.** (_xxxvii_) employed, otherwise than in clerical capacity, in the construction, working, repairs, or maintenance of a bore-well, bore-cum-dug-well, filter point, etc.;

(_xxxviii_) employed in spraying and dusting of insecticides or pesticides in agricultural operation or plantation;

(_xxxix_) employed in working or repairs or maintenance of bulldozers, tractors, power-tillers, etc . _Vide_ Gaz., of Goa, Daman and Diu, 11-2-1982, Series 1, No. 46, p. 387.

**Himachal Pradesh**. In its application to the State of Himachal Pradesh, items (_i_) to (_vi_) are the same as items (_xxix-a_) to (_xxix-f_) for Punjab.

For the words twenty-five in items (_xxix-a_) and (_xxix-b_) in Punjab, the words fifteen have been used in items (_i_) and (_ii_) in H. P. _Vide_ H.P. Gaz., 16-11-1983, Ext., p. 1203.

**Jammu and Kashmir**. In its application to the State of Jammu and Kashmir, the following clause shall be _inserted_, namely:

(_xxxvi_) Persons employed in surveys and investigation including drilling operations, hydrological observations and flood forecasting activities and ground water surveys and exploration . _Vide_ J&K Gaz., 18-11-1991, Pt. I-B, Ext., p. 1 (33-G).

**Karnataka**. In its application to the State of Karnataka,

**1.** After clause (_xxxii_) the following clauses shall be _inserted_, namely:

(_xxxiii_) employed in any operation in the sea for catching fish . _Vide_ Karn. Gaz., 28-11-1981, Pt. IV; 2C(_ii_), Extra., p. 1 (No. 874)

**2.** After clause (_xxxiii_), clause (_xxxiv_) shall be _inserted_:

\[Sub-clauses (_i_) to (_vii_) of clause (_xxxiv_) are same as clauses (_xxxiii_) to (_xxxix_) for Goa.\] Karn. Gaz., 3-12-1981, Pt. IV, S. 2C(_ii_), Ext., Pt. 1 (No. 876).

**3.** After clause (_xxxix_) the following clauses shall be _inserted_, namely:

(_xl_) persons employed on surveys and investigations including drilling operations, hydrological observations and flood forecasting activities, ground water surveys and exploration. _Vide_ Karn. Gaz., 7-2-1992, Pt. IV, 2-C(_ii_), Ext., p. 2.

(_xli_) persons employed in any Newspaper establishment as defined in Working Journalists and Other Newspaper Employees (Conditions of Service) and Miscellaneous Provisions Act, 1955 (Central Act 45 of 1955) and engaged in outdoor duties. _Vide_ Karn. Gaz., 26-11-1992, Pt. IV, S. 2-C(_ii_), p. 3607.

**Kerala**. In its application to the State of Kerala.

**1.** In clause (_xviii_),

(_i_) between the words coffee and rubber , word cardamom will be _inserted_. _Vide_ Ker. Gaz., 14-3-1961, Pt. I, G. 874;

(_ii_) after the words cardamom, cinchona, coffee, rubber or tea words eucalyptus, orange or teak and also any plantations maintained by any Deptt. of the State Government will be _inserted_ and the word or between rubber and tea , will be _omitted_ _Vide_ Ker. Gaz., 27-8-1968, Pt. I, p. 490.

**2.** After clause (_xxxii_), _add_ the following clauses.

(_i_) After item (_xxxii-B_), new entry _added_ as follows:

(_xxxii-C_) Employed in water transport using valloms. _Vide_ Ker. Gaz., 19-5-1964, Pt. I, p. 298:

(_ii_) After item (_xxxii-C_), new entry _added_ as follows:

(_xxxii-D_) Employed in cattle breeding . _Vide_ Ker. Gaz., 2-2-1965, Pt. I, p. 62.

(_iii_) After item (_xxxii-D_), new item (_xxxii-E_) _added_ as follows:

(_xxxii-E_) Employed in handling animals in the Veterinary Institutions . _Vide_ Ker. Gaz., 19-10-1965, Pt. I, p. 561.

(_iv_) (_xxxii-G_)Employed in spraying insecticides in paddy fields, plantations _Vide_ Ker. Gaz.,

31-7-1973, Pt. I (No. 31), G. 1264.

(_xxxii-H_) Employed in clearing of jungles or reclaiming land or ponds;

(_xxxii-I_) Employed in cultivation of land, or rearing and maintenance of livestock, or forest operations, or fishing;

(_xxxii-J_) Employed, otherwise than in a clerical capacity, in installation, maintenance, repair of pumping equipment used for lifting of water from wells, tube-wells, ponds, lakes streams; .

(_xxxii-K_) Employed otherwise than in a clerical capacity in the construction, boring or deepening of an open well or dug well through mechanical contrivances;

(_xxxii-L_) Employed, otherwise than in a clerical capacity in the construction, working, repair or maintenance of bore well, bore-cum-dug-well, filter points;

(_xxxii-M_) Employed in operation or repair or maintenance of bulldozers, tractors, powers tillers. _Vide_ Ker. Gaz., 13-12-1977, Pt. I, S. (_iv_), P. 1, G. 2173 (No. 50).

(_xxxii-N_) Employed in any sewage farm in planting, irrigating or harvesting of fodder grass or in diverting of sewage water or in cleaning of main channel or sub-carriers. _Vide_ Ker. Gaz., 9-11-1982, Pt. I, S. (_iv_), G. 1457, p. 1.

(_xxxii-O_) Employed in cleaning of sewers or septic tanks by any Local Authority. _Vide_ Ker. Gaz., 31-5-1988, Pt. I, S. (_iv_), p. 1 (G253).

(_xxxii_) Employment as Farm Workers in Government Agricultural Farms. _Vide_ Ker. Gaz. 27-1-1990, Ext., P. 1 (No. 83).

**3.** In the Schedule II

(_i_) the existing item (_xxxii_\-_O_) Employed in cleaning of sewers or septic tanks by any local authority shall be _renumbered_ as item (_xxxii-P_).

(_ii_) after item (_xxxii-P_) as so _renumbered_, the following item shall be _added_, namely:

(_xxxii-Q_) Employed otherwise than in a clerical capacity in brick kiln industry

(_iii_) the existing item (_xxxii-S_) Employed as farm workers in Government agricultural farms shall be _renumbered_ as item (_xxxii_\-_R_).

(_iv_) the existing item (_xxxiii_) Parathozhilalikal employed in the occupation of breaking of rock and stones in quarries shall be _renumbered_ as (_xxxii-S_).

(_v_) after item (_xxxii-S_), as so _renumbered_, the following item shall be _added_, namely:

(_xxxii-T_) Employed in Kakkavaral and works connected or incidental thereto. _Vide_ Ker. Gaz. 5-11-1993, Ext., p. 1 (No. 1125).

**Madhya Pradesh**. In its application to the State of M.P., the following clauses shall be _inserted_, namely:

**1.** (_xxxiv_) Persons employed on survey and investigations including drilling operations, hydrological observations and flood forecasting activities, ground water surveys and exploration . _Vide_ M.P. Gaz., 26-6-1992, Pt. I, p. 1903.

**2.** (_xxxv_) Persons employed in the outdoor duties in a Newspaper Establishment as defined in the Working Journalists and Other Newspaper Employees (Conditions of Service) and Miscellaneous Provisions Act, 1955 . _Vide_ M.P. Gaz., 10-12-1993, Pt. I, p. 2794.

**Orissa**. In its application to the State of Orissa, in Schedule II after item (_xxxiii_), new items shall be _added_, namely:

**1.** (_xxxiv_) employed in clearing of jungles or reclaiming lands or ponds in which on any one day of the preceding twelve months more than twenty-five persons have been employed; or

(_xxxv_) employed in cultivation of land or in rearing and maintenance of livestock or forest operations or fishing in which on any one day of the preceding twelve months more than twenty-five persons have been employed; or

(_xxxvi_) employed, otherwise than in clerical capacity, in installation, maintenance, repair of pumping equipment used for lifting of water from wells, tubewells, ponds, lakes, streams etc.

(_xxxvii_) employed, otherwise than in clerical capacity, in the construction, boring or deepening of an open well/dug-well through mechanical contrivances; or

(_xxxviii_) employed, otherwise than in clerical capacity, in the construction, working, repair or maintenance of a bore well, bore-cum-dug-well, filter point, etc.; or

(_xxxix_) employed in spraying and dusting of insecticides or pesticides in agricultural operation or plantations; or

(_xl_) employed in working or repair or maintenance of bulldozers, tractors, power-tillers, etc.; or

(_xli_) employed in making, clearing compost; or

(_xlii_) employed in inter-cultural operation; or

(_xliii_) employed in harvesting operation; or

(_xliv_) employed in threshing operation . _Vide_ Orissa Gaz., 23-2-1979, Pt. III, p. 63.

After clause (_l_) the following clauses are _added_ as under:

**2.** (_li_) Employed in cook-houses, messes, bakeries or catering establishments in which food for over 50 persons is prepared or more than 200 loaves of bread per day are prepared or training to at least 10 cooks/caterers is imparted at any time.

(_lii_) Employed in any employment in which workmen are exposed to low temperature condition, while working in high altitude.

(_Iiii_) Employed in any employment which required handling of snakes for the purpose of extraction of venom or for the purpose of looking after snakes or handling any other poisonous insect or animal.

(_liv_) Employed in training, keeping or working with animals like horses, mules, bulls, etc.

(_lv_) Employed in clearing of sewers or septic tanks within the limits of a local authority.

(_lvi_) Employed in horticultural operations

(_lvii_) Employed in sea fishing.

(_lviii_) Employed in any newspaper establishment as defined in Working Journalists and Other Newspapers Employees (Conditions of Service) and Miscellaneous Provisions Act, 1955 and engaged in outdoor duties.

(_lix_) Employed in surveys and investigations, including drilling operations, hydrological observations and flood forecasting activities, ground-water surveys and exploration . _Vide_ Orissa Gaz., 24-4-1993, Ext., p. 1 (No. 543).

**Pondicherry**. In its application to the Union Territory of Pondicherry, after clause (_xxxii_), the following clauses shall be _added_, namely:

**1.** (_xxxiii_) Employed, otherwise than in clerical capacity in the construction, working, repair or maintenance of a bore-well, bore-cum-dug-well, filter point; or

(_xxxiv_) Employed, in spraying and dusting of insecticides or pesticides in agricultural operation or plantations . _Vide_ Pondicherry Gaz., 15-3-1977, Ord., p. 308;

**2.** (_xxxv_) Employed in fishing in which on any one day of the preceding twelve months more than twenty-five persons have been employed . _Vide_ Pondicherry Gaz., 13-10-1981, p. 820 (No. 41).

**Punjab**. In its application to the State of Punjab,

**1.** After clause (_xxix_), new clauses (_xxix-a_) to (_xxix-g_) shall be _added_ as under:

(_xxix-a_) employed in clearing of jungles or reclaiming land or ponds in which on any one day of the preceding twelve months more than twenty-five persons have been employed; or

(_xxix-b_) employed in the cultivation of land or rearing and maintenance of livestock or forest operations or fishing in which on any one day of the preceding twelve months more than twenty-five persons have been employed; or

(_xxix-c_) employed, otherwise than in clerical capacity in installation, maintenance, repair pumping equipment used for lifting of water from wells, tubewells, ponds, lakes, streams, etc. or

(_xxix-d_) employed, otherwise than in clerical capacity in the construction, boring or deepening of an open well or dugwell through mechanical contrivances; or

(_xxix-e_) employed, otherwise than in clerical capacity in the construction, working, repair or maintenance of a bore well, bore-cum-dugwell, filter point; or

(_xxix-f_) employed in spraying and dusting of insecticides or pesticides in agricultural operations or plantations; or

(_xxix-g_) employed in working or repair or maintenance of bulldozers, tractors, power-tillers, etc.; or . _Vide_ Punjab Govt. Gaz., 3-6-1977, Pt. III (LS), p. 385.

**2.** After clause (_xxxvi_), the following clauses shall be _inserted_, namely:

(_xxxvii_) employed on surveys and investigation, including drilling operations, hydrological observations, flood forecasting activities, ground water surveys and explorations. _Vide_ Punj. Gaz., 23-9-1994, Pt. III (LS) p. 487.

(_xxxviii_) employed in any newspaper establishments as defined in the Working Journalists and Other Newspaper Employees (Conditions of Service) and Miscellaneous Provisions Act, 1955, and engaged in outdoor duties. _Vide_ Punj. Gaz., 23-9-1994, Pt. III (LS), p. 489.\]

**Tamil Nadu**. In its application to the State of Tamil Nadu, for clause (_iii-a_) the following shall be _substituted_:

**1.** (_iii-a_) employed as Artist for drawing pictures on the advertisement boards at a height of 3.66 metres or more from the ground level;

(_iii-b_) employed as sculptors to work on the vimanas of temples; and

(_iii-c_) employed in surveys and investigation, including drilling operations, hydrological operations and flood and forecasting activities, ground water surveys and explorations. _Vide_ G.O. MS. No. 658, Labour and Employment, dt. 7-4-1989.

**2.** After clause (_xxxii_) the following clauses shall be _added_, namely:

(_xxxiii_) Employed in any occupation involving indoor or outdoor work in the service of the Highways Research Station and its Laboratories. _Vide_ Ft. St. Geo. Gaz., 3-8-1966, Pt. II, S. 1, p. 1360.

**3.** (_xxxiv_) Employed otherwise than in a clerical capacity in the conduct of surveys in river valleys including collection of data relating to the river. _Vide_ Ft. St. Geo. Gaz., 19-2-1969, Pt. II, S. 1, p. 104.

**4.** (_xxxv_) Employed as a sweeper or scavenger under a local authority . _Vide_ T.N. Gaz., 14-11-1973, Pt. II, S. 1, p. 584.

**5.** (_xxxvi_) Employed in any operation in the Sea for catching fish . _Vide_ T.N. Govt. Gaz., 10-3-1982, Pt. II, S. 2, p. 156.

**6.** (_xxxvii_) Employed in clearing of jungles or reclaiming land or ponds in which in any one day of the preceding twelve months more than twenty-five persons have been employed;

(_xxxviii_) Employed in cultivation of land or rearing and maintenance of livestock or forest operations or fishing in which on any one day of the preceding twelve months more than twenty-five persons have been employed;

(_xxxix_) Employed otherwise than in clerical capacity in installation, maintenance, repair of pumping equipment used for lifting of water from wells, tube-wells, ponds, lakes, streams etc.;

(_xl_) Employed, otherwise than in the clerical capacity, in the construction, boring or deepening of an open well/dug well through mechanical contrivances;

(_xli_) Employed, otherwise than in clerical capacity, in the construction, working, repair or maintenance of a bore well, bore-cum-dug well, filter point, etc.;

(_xlii_) Employed in spraying and dusting of insecticides or pesticides in agricultural operations or plantations;

(_xliii_) Employed in working or repair or maintenance of bulldozers, tractors, power-tillers, etc.;

(_xliv_) Employed in climbing tall trees;

(_xlv_) Employed in baling water from wells;

(_xlvi_) Employed in crushing sugarcane . _Vide_ T.N. Govt. Gaz., 23-9-1981, Pt. II, S. 2, pp. 619, 620.

**7.** (_xlvii_) Employed in cook houses, messes, bakeries or catering establishments in which food for over 50 persons is prepared or more than 200 loaves of bread per day are prepared or training to at least 10 cooks/caterers is imparted at any time;

(_xlviii_) employed in any employment in which workmen are exposed to low temperature condition, while working in high altitude;

(_xlix_) employed in any employment which requires handling of snakes for the purpose of extraction of venom or for the purpose of looking after snakes or handling any other poisonous insect or animal;

(_1_) employed in training, keeping or working with animals like horses, mules, bulls, etc.;

(_li_) employed in cleaning of sewers or septic tanks within the limits of a local authority;

(_lii_) employed in horticultural operations. _Vide_ T.N. Gaz., 5-8-1987, Pt. II, S. 2, p. 580.

**8.** (_liii_) employed in any newspaper establishment as defined in the Working Journalists and Other Newspaper Employees (Conditions of Service) and Miscellaneous Provisions Act, 1955 (Central Act 45 of 1955), and engaged in outdoor duties. _Vide_ T.N. Gaz., 9-3-1994, Pt. II, S. 2, p. 232.

**Tripura**. In its application to the State of Tripura, following employments are _added_ for payment of compensation for injuries in Parts I and II:

**1.** (_i_) employed in clearing of jungles or reclaiming land and ponds in which on any one day of the preceding twelve months more than twenty-five persons have been employed;

(_ii_) employed in cultivation of land or rearing and maintenance of livestock or forest operations or fishing in which on any one day of the preceding twelve months more than twenty-five persons have been employed;

(_iii_) employed, otherwise than in clerical capacity, in installation, maintenance, repair or pumping equipment used for lifting of water from wells, tube-wells, ponds, lakes, streams etc.;

(_iv_) employed, otherwise than in clerical capacity in the construction, boring or deepening of an open well/dug well through mechanical contrivances;

(_v_) employed, otherwise than in electrical capacity in the construction, working, repairing, maintenance of a borewell, bore-cum-dug-well, filter points etc.;

(_vi_) employed, in spraying and dusting of insecticides or pesticides in agricultural operation or plantations;

(_vii_) employed in working or repair or maintenance of bulldozers, tractors, power tillers etc . _Vide_ Tri. Gaz., 1-6-1981, Pt. I, Ext., p. 4.

**2.** (_viii_) employed in any newspaper establishment as defined in Working Journalists and Other Newspaper Employees (Conditions of Service) and Miscellaneous Provisions Act, 1955 and engaged in outdoor duties.

(_ix_) persons employed on surveys and investigation, including drilling operations, hydrological observations and flood forecasting activities, ground water surveys and exploration . _Vide_ Tri. Gaz., 10-5-1994, Ext., p. 13.

[369](#note_369 "369. Subs. by Act 22 of 1984, S. 6 (w.e.f. 1-7-1984).")\[**SCHEDULE III**

\[_See_ Section 3\]

List of Occupational Diseases

  

Sl. No.

Occupational disease

Employment

(1)

(2)

(3)

**Part A**

1.

Infectious and parasitic diseases contracted in an occupation where there is a particular risk of contamination.

(_a_) All work involving exposure to health or laboratory work;

(_b_) All work involving exposure to veterinary work;

(_c_) Work relating to handling animals, animal carcasses, part of such carcasses, or merchandise which may have been contaminated by animals or animal carcasses;

(_d_) Other work carrying a particular risk of contamination.

2.

Diseases caused by work in compressed air.

All work involving exposure to the risk concerned.

3.

Diseases caused by lead or its toxic compounds.

All work involving exposure to the risk concerned.

4.

Poisoning by nitrous fumes.

All work involving exposure to the risk concerned.

5.

Poisoning by organo phosphorus compounds.

All work involving exposure to the risk concerned.

**Part B**

1.

Diseases caused by phosphorus or its toxic compounds.

All work involving exposure to the risk concerned.

2.

Diseases caused by mercury or its toxic compounds.

All work involving exposure to the risk concerned.

3.

Diseases caused by benzene or its toxic homologues.

All work involving exposure to the risk concerned.

4.

Diseases caused by nitro and amido toxic derivatives of benzene or its homologues.

All work involving exposure to the risk concerned.

5.

Diseases caused by chromium or its toxic compounds.

All work involving exposure to the risk concerned.

6.

Diseases caused by arsenic or its toxic compounds.

All work involving exposure to the risk concerned.

7.

Diseases caused by radioactive substances and ionising radiations.

All work involving exposure to the action of radioactive substances or ionising radiations.

8.

Primary epitheliomatous cancer of the skin caused by tar, pitch, bitumen, mineral oil, anthracene, or the compounds, products or residues of these substances.

All work involving exposure to the risk concerned.

9.

Diseases caused by the toxic halogen derivatives of hydro-carbons (of the aliphatic and aromatic series).

All work involving exposure to the risk concerned.

10.

Diseases caused by carbon disulphide.

All work involving exposure to the risk concerned.

11.

Occupational cataract due to infra-red radiations.

All work involving exposure to the risk concerned.

12.

Diseases caused by manganese or its toxic compounds.

All work involving exposure to the risk concerned.

13.

Skin diseases caused by physical, chemical or biological agents not included in other items.

All work involving exposure to the risk concerned.

14.

Hearing impairment caused by noise.

All work involving exposure to the risk concerned.

15.

Poisoning by dinitrophenol or a homologue or by substituted dinitrophenol or by the salts of such substances.

All work involving exposure to the risk concerned.

16.

Disease caused by beryllium or its toxic compounds.

All work involving exposure to the risk concerned.

17.

Diseases caused by cadmium or its toxic compounds.

All work involving exposure to the risk concerned.

18.

Occupational asthma caused by recognised sensitising agents inherent to the work process.

All work involving exposure to the risk concerned.

19.

Diseases caused by fluorine or its toxic compounds.

All work involving exposure to the risk concerned.

20.

Diseases caused by nitroglycerine or other nitroacid esters.

All work involving exposure to the risk concerned.

21.

Diseases caused by alcohols and ketones.

All work involving exposure to the risk concerned.

22.

Diseases caused by asphyxiants: carbon monoxide, and its toxic derivatives, hydrogen sulfide.

All work involving exposure to the risk concerned.

23.

Lung cancer and mesotheliomas caused by asbestos.

All work involving exposure to the risk concerned.

24.

Primary neoplasm of the epithelial lining of the urinary bladder or the kidney or the ureter.

All work involving exposure to the risk concerned.

[370](#note_370 "370. Entries at Serial Nos. 25 to 27 ins. by Act 30 of 1995, S. 16 (w.e.f. 15-9-1995).")\[25.

Snow blindness in snow bound areas.

All work involving exposure to the risk concerned.

26.

Disease due to effect of heat in extreme hot climate.

All work involving exposure to the risk concerned.

27.

Disease due to effect of cold in extreme cold climate.

All work involving exposure to the risk concerned.\]

**Part C**

1.

Pneumoconioses caused by sclerogenic mineral dust (silicosis, anthraoosilicosis, asbestosis) and silico-turberculosis provided that silicosis is an essential factor in causing the resultant incapacity or death.

All work involving exposure to the risk concerned.

2.

Bagassosis.

All work involving exposure to the risk concerned.

3.

Bronchopulmonary diseases caused by cotton, flax hemp and sisal dust (Byssinosis).

All work involving exposure to the risk concerned.

4.

Extrinsic allergic alveelitis caused by the inhalation of organic dusts.

All work involving exposure to the risk concerned.

5.

Bronchopulmonary diseases caused by hard metals.

All work involving exposure to the risk concerned.\]

[371](#note_371 "371. Ins. by S.O. 2615, dt. 15-9-1987.")\[6.

Acute Pulmonary Oedema of high altitude.

All work involving exposure to the risk concerned.\]

STATE AMENDMENTS

**Note**. Prior to substitution of Sch. III by Act 22 of 1984, amendments to Sch. III, were as under:

IN PART A OF SCH. III

**Andhra Pradesh, Bihar, Chandigarh, Gujarat, Haryana, Orissa and Tamil Nadu**.

**1.** In application to the above mentioned states the amendments to Sch. III, Pt. A are as that for Punjab. _Vide_ A.P. Gaz., 17-9-1981, Pt. I, p. 820, Bihar Gaz., 19-5-1983, Ext., p. 3, Chand. Gaz., 20-12-1979, Ext., p. 535, Guj. Govt. Gaz., 20-10-1980, Pt. I-L, p. 5089, Haryana Gaz., 29-4-1980, Pt. I, p. 699, Orissa Gaz., 23-2-1979, Pt. III, p. 55, T.N. Govt. Gaz., 18-11-1981, Pt. II, p. 754.

**Kerala**. In its application to the State of Kerala, the following shall be _added_ to Pt. A of Sch. III, namely:

**1.** Occupational Dermatitis. Any process carried on in shelling of cashew nut in cashew industry . Ker. Gaz., 23-9-1975, Pt. I, S. IV, p. 1 (No. 37).

**Punjab**. In its application to the State of Punjab the following shall be _added_ to Pt. of A of Sch. III, namely:

  

Occupational diseases

Employment

**1.** (_i_)

Poisoning by tricresyl phosphate

Any employment involving the use or handling of, or exposure to the fumes of, or vapour containing tricresyl phosphate.

(_ii_)

Poisoning by diethylene dioxide (dioxan)

Any employment involving the use or handling of, or exposure to the fumes of, or vapours containing diethylene dioxide (dioxan).

(_iii_)

Poisoning by nickel carbonyl

Any employment involving exposure to nickel carbonyl gas.

_Vide_ Punj. Govt. Gaz., 5-9-1980, Pt. III (LS), p. 361.

IN PART B OF SCH. III

**Andhra Pradesh, Bihar, Chandigarh (U.T.), Gujarat, Haryana, Orissa and Tamil Nadu**. **1.** In application to the above mentioned states the amendments to Sch. III, Pt. B are as that for Punjab. _Vide_ A.P. Gaz., 17-9-1981, Pt. I, p. 820, Bihar Gaz., 19-5-1983, Ext., p. 3, Chand. Admn. Gaz., 20-12-1979, Ext., p. 535, Guj. Govt. Gaz., 2-10-1980, Pt. 1-L, p. 5089, Haryana Gaz., 29-4-1980, Pt. I, p. 699, Orissa Gaz., 23-2-1979, Pt. III, p. 51, T.N. Govt. Gaz., 18-11-1981, Pt. II, S. 2, p. 754.

**Kerala**. In its application to the State of Kerala, the following shall be _added_ to Pt. B of Sch. III, namely:

  

**1.**

Bysinosis

Any process involving carding and blowing in Cotton Mills.

Poisoning by halogens and their derivatives

Any employment in chemical industries involving the use of halogens or their derivatives.

_Vide_ Ker. Gaz., 23-9-1975, Pt. I, S. IV, p. 1(No. 37).

**Punjab**. In its application to the State of Punjab, the following shall be _added_ to Pt. B of Sch. III, namely:

  

**1.** (_i_)

Non-infective dermatitis of external origin (including chrome ulceration of the skin but excluding dermatitis due to ionising particles of electro-magnetic radiations other than radiant heat).

Any employment involving exposure to dust, liquid or vapour or any other external agent capable of irritating the skin (including friction or heat but excluding ionising particles or electro-magnetic radiations other than radiant heat).

(_ii_)

Noise induced hearing loss.

Any employment involving exposure to high noise level over a prolonged period.

(_iii_)

Poisoning by Dinitrophenol or a homologue or by substituted dinitrophenol or by the salts of such substances.

Any employment involving the use or handling of, or exposure to the fumes of, or vapour containing dinitrophenol or homologue or substituted dinitrophenols or the salts of such substances.

(_iv_)

Poisoning by beryllium or a compound of beryllium.

Any employment involving the use or handling of, or exposure to the fumes, dust or vapour of beryllium or a compound of beryllium or a substance containing beryllium.

(_v_)

Poisoning by cadmium.

Any employment involving exposure to cadmium fumes.

(_vi_)

Primary neoplasm, of the epithelial lining of the urinary bladder (Papilloma of the bladder) or of the renal pelvis or of the epithelial lining of the ureter.

(_a_) Any employment involving the use or handling of, or exposure to any of the following substances:

(_i_) Beta naphthylamine and its salts.

(_ii_) Benzidine and its salts.

(_iii_) 4 amino diphenyl and its salts.

(_iv_) 4 nitro diphenyl and its salts.

(_v_) Alpha naphthylamine and its salts.

(_vi_) Orthotolidine and its salts.

(_vii_) Dianisidine and its salts.

(_viii_) Dichlorobenzidine and its salts.

(_ix_) Auramine.

(_x_) Magneta.

(_b_) Maintenance or cleaning of any part of machinery used for the production, handling or processing of any substances mentioned in (_a_) above.

(_c_) Cleaning of any clothing used by working with any substances mentioned in (_a_) above in a laundry which forms a part of the place of employment.

_Vide_ Punj. Govt. Gaz., 5-9-1980, Pt. III (LS), p. 361.

**Tamil Nadu**. In its application to the State of Tamil Nadu, at the end of Sch. III the following shall be _added_:

  

**1.** (_a_)

Poisoning by

Manganese or a compound of Manganese; or its sequelae.

Any process involving the use of or handling of, or exposure to the fumes, dust or vapour of manganese or compound of manganese, or a substance containing manganese.

(_b_)

Poisoning by

Organic phosphorous insecticides hexaethyl tetraphosphate (HETP), Tetraethyl Pyrophosphate (TEPP), and oo-diethyl O.P. nitrophenyl thio phosphate (PARATHION)

Any process involving the use or handling or exposure to fumes, dust or vapour containing any of the organic phosphorus insecticides.

_Vide_ Ft. St. Geo. Gaz., 16-5-1962, Pt. II, S. 1, p. 849.

[372](#note_372 "372. Subs. by Act 22 of 1984, S. 7 (w.e.f. 1-7-1984).")\[**SCHEDULE IV**

\[_See_ Section 4\]

Factors for working out lump sum equivalent of compensation amount in case of permanent disablement and death

     

Completed years of age on the last birthday of the [373](#note_373 "373. Subs. for  workman  by Act 45 of 2009, S. 5 (w.e.f. 18-1-2010).")\[employee\] immediately preceding the date on which the compensationfell due

Factors

Completed years of age on the last birthday of the [374](#note_374 "374. Subs. for  workman  by Act 45 of 2009, S. 5 (w.e.f. 18-1-2010).")\[employee\] immediately preceding the date on which the compensation fell due

Factors

     

(1)

(2)

(1)

(2)

not more than 16

..

228.54

41

..

181.37

17

..

227.49

42

..

178.49

18

..

226.38

43

..

175.54

19

..

225.22

44

..

172.52

20

..

224.00

45

..

169.44

21

..

222.71

46

..

166.29

22

..

221.37

47

..

163.07

23

..

219.95

48

..

159.80

24

..

218.47

49

..

156.47

25

..

216.91

50

..

153.09

26

..

215.28

51

..

149.67

27

..

213.57

52

..

146.20

28

..

211.79

53

..

142.68

29

..

209.92

54

..

139.13

30

..

207.98

55

..

135.56

31

..

205.98

56

..

131.95

32

..

203.85

57

..

128.33

33

..

201.66

58

..

124.70

34

..

199.40

59

..

121.05

35

..

197.06

60

..

117.41

36

..

194.64

61

..

113.77

37

..

192.14

62

..

110.14

38

..

189.56

63

..

106.52

39

..

186.90

64

..

102.93

40

..

184.17

65 or more

99.37\]

\* Ed.: Formerly known as the Workmen's Compensation Act .

1\. Subs. for Workmen by Act 45 of 2009, S. 4 (w.e.f. 18-1-2010).

2\. For Statement of Objects and Reasons see Gaz. of India, 1922, Pt. V, p. 313, and for Report of Joint Committee, see ibid., 1923, Pt. V, p. 37. This Act has been extended to Berar by the Berar Laws Act, 1941 (4 of 1941), and also been declared in force in the district of Khondmals by S. 3 and Sch. of the Khondmals Laws Regulation, 1936 (4 of 1936), and in the district of Augul by S. 3 and Sch. of the Augul Laws Regulation, 1936 (5 of 1936). For modifications of this Act in its application to apprentices under the Apprentices Act, 1961, see p. 63, in Vol. I.

3\. Received the assent of the President on April 12, 2017.

4\. Subs. for workmen by Act 45 of 2009, S. 2 (w.e.f. 18-1-2010).

5\. Subs. for workmen by Act 45 of 2009, S. 3 (w.e.f. 18-1-2010).

6\. Subs. for workmen's by Act 45 of 2009, S. 4 (w.e.f. 18-1-2010).

7\. Subs. by the A.O. 1950, for the former sub-section.

8\. The words except the State of Jammu & Kashmir omitted by Act 51 of 1970.

9\. Cl. (a) omitted by Act 8 of 1959, S. 2 (w.e.f. 1-6-1959).

10\. Subs. for workmen's by Act 45 of 2009, S. 5 (w.e.f. 18-1-2010).

11\. Subs. by Act 8 of 1959, S. 2, for the former cl. (w.e.f. 1-6-1959).

12\. Subs. for workman by Act 45 of 2009, S. 5 (w.e.f. 18-1-2010).

13\. Subs. by Act 30 of 1995 (w.e.f. 15-9-1995).

14\. Subs. by Act 30 of 1995 (w.e.f. 15-9-1995).

15\. Subs. for workman by Act 45 of 2009, S. 5 (w.e.f. 18-1-2010).

16\. Subs. for workman by Act 45 of 2009, S. 5 (w.e.f. 18-1-2010).

17\. Subs. by Act 30 of 1995 (w.e.f. 15-9-1995).

18\. Subs. for workman by Act 45 of 2009, S. 5 (w.e.f. 18-1-2010).

19\. Ins. by Act 30 of 1995 (w.e.f. 15-9-1995).

20\. Ins. by Act 45 of 2009, S. 6 (w.e.f. 18-1-2010).

21\. Subs. for workman by Act 45 of 2009, S. 5 (w.e.f. 18-1-2010).

22\. Subs. for workman by Act 45 of 2009, S. 5 (w.e.f. 18-1-2010).

23\. Subs. for workman by Act 45 of 2009, S. 5 (w.e.f. 18-1-2010).

24\. Ins. by Act 8 of 1959, S. 2 (w.e.f. 1-6-1959).

25\. Subs. for workman by Act 45 of 2009, S. 5 (w.e.f. 18-1-2010).

26\. Subs. by Act 64 of 1962, S. 2 (w.e.f. 1-2-1963).

27\. Certain words omitted by Act 8 of 1959, S. 2 (w.e.f. 1-6-1959).

28\. Subs. by the A.O. 1950, for the words Act of the Central Legislature or of any Legislature in a Province .

29\. Subs. by the A.O. (No. 3) 1956, for Part A State or Part B State .

30\. Cl. (j) omitted by Act 15 of 1933, S. 2.

31\. The word registered omitted by Act 15 of 1933, S. 2.

32\. Subs. by Act 15 of 1933, S. 2, for any such .

33\. Subs. for workman by Act 45 of 2009, S. 5 (w.e.f. 18-1-2010).

34\. Subs. by Act 64 of 1962, S. 2 (w.e.f. 1-2-1963).

35\. Subs. for workman by Act 45 of 2009, S. 5 (w.e.f. 18-1-2010).

36\. Subs. for workman by Act 45 of 2009, S. 5 (w.e.f. 18-1-2010).

37\. Clause (n) omitted by Act 45 of 2009, S. 6 (w.e.f. 18-1-2010). Prior to omission it read as: (n) workman means any person who is (i) a railway servant as defined in clause (34) of Section 2 of the Railways Act, 1989 (24 of 1989), not permanently employed in any administrative, district or sub-divisional office of a railway and not employed in any such capacity as is specified in Schedule II, or (i-a)(a) a master, seaman or other member of the crew of a ship, (b) a captain or other member of the crew of an aircraft, (c) a person recruited as driver, helper, mechanic, cleaner or in any other capacity in connection with a motor vehicle, (d) a person recruited for work abroad by a company, and who is employed outside India in any such capacity as is specified in Schedule II and the ship, aircraft or motor vehicle, or company, as the case may be, is registered in India, or (ii) employed in any such capacity as is specified in Schedule II, whether the contract of employment was made before or after the passing of this Act and whether such contract is expressed or implied, oral or in writing; but does not include any person working in the capacity of a member of the Armed Forces of the Union and any reference to a workman who has been injured shall, where the workman is dead, include a reference to his dependants or any of them. .

38\. Subs. by the A.O. 1937, for of the Government .

39\. Subs. by Act 30 of 1995 (w.e.f. 15-9-1995).

40\. Subs. for workmen's by Act 45 of 2009, S. 5 (w.e.f. 18-1-2010).

41\. Subs. for workman by Act 45 of 2009, S. 5 (w.e.f. 18-1-2010).

42\. Subs. for workman by Act 45 of 2009, S. 5 (w.e.f. 18-1-2010).

43\. Subs. by Act 8 of 1959, S. 3 for seven (w.e.f. 1-6-1959).

44\. Subs. by Act 15 of 1933, S. 3 for injury to a workman resulting from .

45\. Ins. by Act 30 of 1995, S. 3 (w.e.f. 15-9-1995).

46\. Subs. for workman by Act 45 of 2009, S. 5 (w.e.f. 18-1-2010).

47\. Subs. for workman by Act 45 of 2009, S. 5 (w.e.f. 18-1-2010).

48\. Subs. for workman by Act 45 of 2009, S. 5 (w.e.f. 18-1-2010).

49\. Subs. for workman by Act 45 of 2009, S. 5 (w.e.f. 18-1-2010).

50\. Subs. for workman by Act 45 of 2009, S. 5 (w.e.f. 18-1-2010).

51\. The word or omitted by Act 5 of 1929, S. 2.

52\. Cl. (c) omitted by Act 5 of 1929, S. 2.

53\. Sub-ss. (2) and (3) subs. by Act 8 of 1959, S. 3 (w.e.f. 1-6-1959).

54\. Subs. for workman by Act 45 of 2009, S. 5 (w.e.f. 18-1-2010).

55\. Subs. for workman by Act 45 of 2009, S. 5 (w.e.f. 18-1-2010).

56\. Subs. for workman by Act 45 of 2009, S. 5 (w.e.f. 18-1-2010).

57\. Added by Act 64 of 1962, S. 3 (w.e.f 1-2-1963).

58\. Subs. for workman by Act 45 of 2009, S. 5 (w.e.f. 18-1-2010).

59\. Subs. for workman by Act 45 of 2009, S. 5 (w.e.f. 18-1-2010).

60\. Subs. by Act 64 of 1962, S. 3 (w.e.f. 1-2-1963).

61\. Subs. for workman by Act 45 of 2009, S. 5 (w.e.f. 18-1-2010).

62\. Subs. by Act 30 of 1995, S. 3 (w.e.f. 15-9-1995).

63\. Omitted by Act 51 of 1970, S. 2.

64\. Ins. by Act 30 of 1995, S. 3 (w.e.f. 15-9-1995).

65\. Subs. by Act 8 of 1959, S. 3 for sub-section (2) (w.e.f. 1-6-1959).

66\. Subs. for workman by Act 45 of 2009, S. 5 (w.e.f. 18-1-2010).

67\. The words solely and omitted by Act 15 of 1933, S. 3.

68\. Subs. for workman by Act 45 of 2009, S. 5 (w.e.f. 18-1-2010).

69\. Subs. for workman by Act 45 of 2009, S. 5 (w.e.f. 18-1-2010).

70\. Subs. for workman by Act 45 of 2009, S. 5 (w.e.f. 18-1-2010).

71\. Subs. by Act 22 of 1984, S. 3 (w.e.f. 1-7-1984).

72\. Subs. by Act 30 of 1995, S. 4 (w.e.f. 15-9-1995).

73\. Subs. for workman by Act 45 of 2009, S. 5 (w.e.f. 18-1-2010).

74\. Subs. for eighty thousand rupees by Act 45 of 2009, S. 7 (w.e.f. 18-1-2010).

75\. Subs. by Act 30 of 1995, S. 4 (w.e.f. 15-9-1995).

76\. Subs. for workman by Act 45 of 2009, S. 5 (w.e.f. 18-1-2010).

77\. Subs. for ninety thousand rupees by Act 45 of 2009, S. 7 (w.e.f. 18-1-2010).

78\. Ins. by Act 45 of 2009, S. 7 (w.e.f. 18-1-2010).

79\. Subs. for workman by Act 45 of 2009, S. 5 (w.e.f. 18-1-2010).

80\. Subs. for workman by Act 45 of 2009, S. 5 (w.e.f. 18-1-2010).

81\. Omitted by Act 45 of 2009, S. 7 (w.e.f. 18-1-2010). Prior to omission it read as: Explanation II. Where the monthly wages of a workman exceed four thousand rupees, his monthly wages for the purposes of clause (a) and clause (b) shall be deemed to be four thousand rupees only; .

82\. Subs. for workman by Act 45 of 2009, S. 5 (w.e.f. 18-1-2010).

83\. Ins. by Act 30 of 1995, S. 4 (w.e.f. 15-9-1995).

84\. Subs. for workman by Act 45 of 2009, S. 5 (w.e.f. 18-1-2010).

85\. Subs. for workman by Act 45 of 2009, S. 5 (w.e.f. 18-1-2010).

86\. Subs. for workman by Act 45 of 2009, S. 5 (w.e.f. 18-1-2010).

87\. Ins. by Act 45 of 2009, S. 7 (w.e.f. 18-1-2010).

88\. In exercise of the powers conferred by sub-section (1-B) of Section 4 of the Employee's Compensation Act, 1923 (8 of 1923), the Central Government hereby specifies, for the purposes of sub-section (1) of the said section, the following amount as monthly wages, with effect from the date of publication of this notification in the Official Gazette, namely Eight thousand rupees \[Vide S.O. 1258(E), dated 31-5-2010 (w.e.f. 31-5-2010)\].

89\. Subs. for workman by Act 45 of 2009, S. 5 (w.e.f. 18-1-2010).

90\. Subs. for workman by Act 45 of 2009, S. 5 (w.e.f. 18-1-2010).

91\. Subs. for workman by Act 45 of 2009, S. 5 (w.e.f. 18-1-2010).

92\. Subs. for workman by Act 45 of 2009, S. 5 (w.e.f. 18-1-2010).

93\. Ins. by Act 45 of 2009, S. 7 (w.e.f. 18-1-2010).

94\. Ins. by Act 30 of 1995, S. 4 (w.e.f. 15-9-1995).

95\. Subs. for workman by Act 45 of 2009, S. 5 (w.e.f. 18-1-2010).

96\. Subs. for two thousand and five hundred rupees by Act 45 of 2009, S. 7 (w.e.f. 18-1-2010).

97\. Subs. for workman by Act 45 of 2009, S. 5 (w.e.f. 18-1-2010).

98\. Subs. for workman by Act 45 of 2009, S. 5 (w.e.f. 18-1-2010).

99\. Subs. for workman by Act 45 of 2009, S. 5 (w.e.f. 18-1-2010).

100\. Ins. by Act 45 of 2009, S. 7 (w.e.f. 18-1-2010).

101\. Ins. by Act 8 of 1959, S. 5 (w.e.f. 1-6-1959).

102\. Subs. for workman by Act 45 of 2009, S. 5 (w.e.f. 18-1-2010).

103\. Subs. for workman by Act 45 of 2009, S. 5 (w.e.f. 18-1-2010).

104\. Sub-ss. (3) and (3-A) subs. for sub-s. (3) by Act 30 of 1995, S. 5 (w.e.f. 15-9-1995).

105\. Subs. for The interest payable under sub-section (3) shall be paid to the workman or his dependant, as the case may be, and the penalty shall be credited to the State Government by Act 46 of 2000, S. 4.

106\. Subs. for workman by Act 45 of 2009, S. 5 (w.e.f. 18-1-2010).

107\. The bracket and figure (1) omitted by Act 9 of 1938, S. 4.

108\. Subs. by Act 13 of 1939, S. 2 for For the purposes of this Act the monthly wages of a workman shall be calculated (w.e.f. 30-6-1934).

109\. Subs. for workman by Act 45 of 2009, S. 5 (w.e.f. 18-1-2010).

110\. Subs. for workman by Act 45 of 2009, S. 5 (w.e.f. 18-1-2010).

111\. Ins. by Act 15 of 1933, S. 5.

112\. Subs. for workman by Act 45 of 2009, S. 5 (w.e.f. 18-1-2010).

113\. Subs. for workman by Act 45 of 2009, S. 5 (w.e.f. 18-1-2010).

114\. The words deemed to be omitted by Act 13 of 1939, S. 2 (w.e.f. 30-6-1934).

115\. Subs. for workman by Act 45 of 2009, S. 5 (w.e.f. 18-1-2010).

116\. Subs. for workman by Act 45 of 2009, S. 5 (w.e.f. 18-1-2010).

117\. Subs. for workman by Act 45 of 2009, S. 5 (w.e.f. 18-1-2010).

118\. Original cl. (b) re-lettered as (c) by Act 15 of 1933, S. 5.

119\. Subs. by Act 8 of 1959, S. 6, for in other cases (w.e.f. 1-6-1959).

120\. The proviso omitted by Act 15 of 1933, S. 5.

121\. Subs. by Act 5 of 1929, S. 3, for this section .

122\. Subs. by Act 9 of 1938, S. 4, for sub-section .

123\. Sub-section (2) added by Act 5 of 1929, S. 3, omitted by Act 15 of 1933, S. 5.

124\. Subs. for workman by Act 45 of 2009, S. 5 (w.e.f. 18-1-2010).

125\. Subs. for workman by Act 45 of 2009, S. 5 (w.e.f. 18-1-2010).

126\. Subs. for workman by Act 45 of 2009, S. 5 (w.e.f. 18-1-2010).

127\. Subs. by Act 5 of 1929, S. 4, for the original sub-sections (1) to (3).

128\. Subs. for workman by Act 45 of 2009, S. 5 (w.e.f. 18-1-2010).

129\. Subs. by Act 15 of 1933, S. 6, for the former proviso.

130\. Subs. for workman by Act 45 of 2009, S. 5 (w.e.f. 18-1-2010).

131\. Subs. by Act 30 of 1995, S. 6 (w.e.f. 15-9-1995).

132\. Subs. for workman by Act 45 of 2009, S. 5 (w.e.f. 18-1-2010).

133\. Ins. by Act 5 of 1929, S. 4.

134\. Subs. for workman by Act 45 of 2009, S. 5 (w.e.f. 18-1-2010).

135\. Certain words omitted by Act 30 of 1995, S. 6 (w.e.f. 15-9-1995).

136\. Sub-sections (5), (6) and (7) subs. by Act 5 of 1929, S. 4, for the original sub-section (5).

137\. Subs. for workman by Act 45 of 2009, S. 5 (w.e.f. 18-1-2010).

138\. Subs. for workman by Act 45 of 2009, S. 5 (w.e.f. 18-1-2010).

139\. Subs. for workman by Act 45 of 2009, S. 5 (w.e.f. 18-1-2010).

140\. Subs. for workman by Act 45 of 2009, S. 5 (w.e.f. 18-1-2010).

141\. The original sub-section (6) renumbered as sub-section (8) by Act 5 of 1929, S. 4.

142\. Added by Act 5 of 1929, S. 4.

143\. Subs. for workman by Act 45 of 2009, S. 5 (w.e.f. 18-1-2010).

144\. Subs. by Act 9 of 1939, S. 5, for certain original words.

145\. Subs. by Act 8 of 1959, S. 8, for one year (w.e.f. 1-6-1959).

146\. Subs. by Act 8 of 1959, S. 8, for one year (w.e.f. 1-6-1959).

147\. Subs. for workman by Act 45 of 2009, S. 5 (w.e.f. 18-1-2010).

148\. Ins. by Act 64 of 1962.

149\. Subs. for workman by Act 45 of 2009, S. 5 (w.e.f. 18-1-2010).

150\. Subs. for workman by Act 45 of 2009, S. 5 (w.e.f. 18-1-2010).

151\. Subs. for workman by Act 45 of 2009, S. 5 (w.e.f. 18-1-2010).

152\. Ins. by Act 15 of 1933, S. 7.

153\. Subs. by Act 9 of 1938, S. 5, for maintenance of proceedings .

154\. Subs. by Act 9 of 1938, S. 5, for made .

155\. Subs. for workman by Act 45 of 2009, S. 5 (w.e.f. 18-1-2010).

156\. Subs. for workman by Act 45 of 2009, S. 5 (w.e.f. 18-1-2010).

157\. Subs. for workman by Act 45 of 2009, S. 5 (w.e.f. 18-1-2010).

158\. Ins. by Act 9 of 1938, S. 5.

159\. Subs. for workman by Act 45 of 2009, S. 5 (w.e.f. 18-1-2010).

160\. Subs. by Act 9 of 1938, S. 5, for admit .

161\. Subs. by Act 9 of 1938, S. 5, for instituted .

162\. Subs. by Act 9 of 1938, S. 5, for institute .

163\. Subs. by Act 7 of 1924, S. 2 and Sch. I, for any one or .

164\. The word directly omitted by Act 9 of 1938, S. 5.

165\. Subs. for workman by Act 45 of 2009, S. 5 (w.e.f. 18-1-2010).

166\. Subs. by Act 15 of 1933, S. 7, for the original sub-section (3).

167\. Subs. for workmen by Act 45 of 2009, S. 5 (w.e.f. 18-1-2010).

168\. Subs. for workman by Act 45 of 2009, S. 5 (w.e.f. 18-1-2010).

169\. Ins. by Act 15 of 1933, S. 8.

170\. Subs. for workman by Act 45 of 2009, S. 5 (w.e.f. 18-1-2010).

171\. Subs. for workman's by Act 45 of 2009, S. 5 (w.e.f. 18-1-2010).

172\. Subs. for workman by Act 45 of 2009, S. 5 (w.e.f. 18-1-2010).

173\. Subs. for workman by Act 45 of 2009, S. 5 (w.e.f. 18-1-2010).

174\. Ins. by Act 8 of 1959, S. 9 (w.e.f. 1-6-1959).

175\. Ins. by Act 8 of 1959, S. 9 (w.e.f. 1-6-1959).

176\. Ins. by Act 8 of 1959, S. 9 (w.e.f. 1-6-1959).

177\. Ins. by Act 8 of 1959, S. 9 (w.e.f. 1-6-1959).

178\. Ins. by Act 8 of 1959, S. 9 (w.e.f. 1-6-1959).

179\. Subs. for workman by Act 45 of 2009, S. 5 (w.e.f. 18-1-2010).

180\. Subs. for workman by Act 45 of 2009, S. 5 (w.e.f. 18-1-2010).

181\. Subs. for workman by Act 45 of 2009, S. 5 (w.e.f. 18-1-2010).

182\. Subs. for workman by Act 45 of 2009, S. 5 (w.e.f. 18-1-2010).

183\. Subs. for workman by Act 45 of 2009, S. 5 (w.e.f. 18-1-2010).

184\. Subs. for workman by Act 45 of 2009, S. 5 (w.e.f. 18-1-2010).

185\. Subs. for workman by Act 45 of 2009, S. 5 (w.e.f. 18-1-2010).

186\. Subs. for workman by Act 45 of 2009, S. 5 (w.e.f. 18-1-2010).

187\. Subs. by Act 9 of 1938, S. 6, for certain words.

188\. Subs. for workman by Act 45 of 2009, S. 5 (w.e.f. 18-1-2010).

189\. Subs. for workman by Act 45 of 2009, S. 5 (w.e.f. 18-1-2010).

190\. Ins. by Act 9 of 1938, S. 6.

191\. Subs. for workman by Act 45 of 2009, S. 5 (w.e.f. 18-1-2010).

192\. Subs. for workman by Act 45 of 2009, S. 5 (w.e.f. 18-1-2010).

193\. Subs. for workman by Act 45 of 2009, S. 5 (w.e.f. 18-1-2010).

194\. Ins. by Act 15 of 1933, S. 9.

195\. Subs. for workman by Act 45 of 2009, S. 5 (w.e.f. 18-1-2010).

196\. Subs. for workman by Act 45 of 2009, S. 5 (w.e.f. 18-1-2010).

197\. Subs. for workman by Act 45 of 2009, S. 5 (w.e.f. 18-1-2010).

198\. Subs. for workman by Act 45 of 2009, S. 5 (w.e.f. 18-1-2010).

199\. Subs. for workman by Act 45 of 2009, S. 5 (w.e.f. 18-1-2010).

200\. Subs. for workman by Act 45 of 2009, S. 5 (w.e.f. 18-1-2010).

201\. Subs. for workman by Act 45 of 2009, S. 5 (w.e.f. 18-1-2010).

202\. Subs. for workman by Act 45 of 2009, S. 5 (w.e.f. 18-1-2010).

203\. Subs. for workman by Act 45 of 2009, S. 5 (w.e.f. 18-1-2010).

204\. Subs. for workman by Act 45 of 2009, S. 5 (w.e.f. 18-1-2010).

205\. Subs. for workman by Act 45 of 2009, S. 5 (w.e.f. 18-1-2010).

206\. Subs. for workman by Act 45 of 2009, S. 5 (w.e.f. 18-1-2010).

207\. Subs. by Act 30 of 1995, S. 7 (w.e.f. 15-9-1995).

208\. Ins. by Act 8 of 1959, S. 10 (w.e.f. 1-6-1959).

209\. Subs. for workmen by Act 45 of 2009, S. 5 (w.e.f. 18-1-2010).

210\. The word registered omitted by Act 15 of 1933, S. 10.

211\. Subs. by Act 8 of 1959, S. 11, for six months (w.e.f. 1-6-1959).

212\. Added by Act 8 of 1959, S. 11 (w.e.f. 1-6-1959).

213\. Ins. by the A.O. 1950.

214\. Subs. by Act 22 of 1984, S. 4, w.e.f. 1-7-1984.

215\. Original cl. (4) omitted by Act 9 of 1938, S. 7.

216\. Original cl. (5) renumbered as cl. (4) by Act 9 of 1938, S. 7.

217\. Subs. by Act 7 of 1924, S. 2 and Sch. I, for monthly payment .

218\. The words and letters in Part A States and Part C States omitted by Act 3 of 1951, S. 3 and Sch.

219\. Subs. by Act 1 of 1942, S. 2, for cl. (5)(w.e.f. 3-9-1939). Cl. (5) was ins. by Act 42 of 1939, S. 2.

220\. Ss. 15-A and 15-B ins. by Act 30 of 1995, S. 8 (w.e.f. 15-9-1995).

221\. Subs. for workmen by Act 45 of 2009, S. 5 (w.e.f. 18-1-2010).

222\. Subs. for workmen by Act 45 of 2009, S. 5 (w.e.f. 18-1-2010).

223\. Subs. for workman by Act 45 of 2009, S. 5 (w.e.f. 18-1-2010).

224\. Subs. for workman by Act 45 of 2009, S. 5 (w.e.f. 18-1-2010).

225\. Subs. for workman by Act 45 of 2009, S. 5 (w.e.f. 18-1-2010).

226\. Subs. for workman by Act 45 of 2009, S. 5 (w.e.f. 18-1-2010).

227\. The words G.G. in C have been substituted by the A.O. 1937 and the A.O. 1950, to read as above.

228\. Subs. for workmen by Act 45 of 2009, S. 5 (w.e.f. 18-1-2010).

229\. The words G.G. in C have been substituted by the A.O. 1937 and the A.O. 1950, to read as above.

230\. Subs. for workman by Act 45 of 2009, S. 5 (w.e.f. 18-1-2010).

231\. Ins. by Act 11 of 2017, S. 2 (w.e.f. 15-5-2017).

232\. Ins. by Act 15 of 1933, S. 11.

233\. Subs. for Section 16, by Act 11 of 2017, S. 3(i)(w.e.f. 15-5-2017).

234\. Ins. by Act 11 of 2017, S. 3(ii)(w.e.f. 15-5-2017).

235\. Subs. for which may extend to five thousand rupees by Act 11 of 2017, S. 3(iii)(w.e.f. 15-5-2017).

236\. Subs. by Act 64 of 1962.

237\. Subs. for workman by Act 45 of 2009, S. 5 (w.e.f. 18-1-2010).

238\. Subs. by Act 15 of 1933, S. 12, for the Commissioner .

239\. Ins. by Act 45 of 2009, S. 8 (w.e.f. 18-1-2010).

240\. Subs. for workmen's by Act 45 of 2009, S. 5 (w.e.f. 18-1-2010).

241\. The word Local omitted by Act 64 of 1962.

242\. Ins. by Act 15 of 1933, S. 13.

243\. The word Local omitted by Act 64 of 1962.

244\. Original sub-sections (2) and (3) renumbered as sub-sections (3) and (4) by Act 15 of 1933, S. 13.

245\. Renumbered by Act 15 of 1933, S. 13.

246\. Subs. by Act 30 of 1995, S. 10 (w.e.f. 1-10-1996).

247\. Subs. for workman by Act 45 of 2009, S. 5 (w.e.f. 18-1-2010).

248\. Subs. for workman by Act 45 of 2009, S. 5 (w.e.f. 18-1-2010).

249\. Subs. for workman by Act 45 of 2009, S. 5 (w.e.f. 18-1-2010).

250\. Subs. by Act 9 of 1938, S. 9, for certain words.

251\. Ins. by Act 9 of 1938, S. 9.

252\. Second Proviso omitted by Act 30 of 1995, S. 10 (w.e.f. 1-10-1996).

253\. Ins. by Act 15 of 1933, S. 14.

254\. Subs. by Act 30 of 1995, S. 11 (w.e.f. 15-9-1995).

255\. Ins. by Act 15 of 1933, S. 15.

256\. Subs. by Act 15 of 1933, S. 15, for Where any such question has arisen, the application .

257\. Ins. by Act 15 of 1933, S. 15.

258\. Subs. by Act 37 of 1925, S. 2 and Sch. I, for on .

259\. Ins. by Act 15 of 1933, S. 16.

260\. Subs. for workman by Act 45 of 2009, S. 5 (w.e.f. 18-1-2010).

261\. Added by Act 5 of 1929, S. 5.

262\. Subs. by Act 30 of 1995 (w.e.f. 15-9-1995).

263\. Subs. by Act 8 of 1959, S. 14, for the former section (w.e.f. 1-6-1959).

264\. Ins. by Act 45 of 2009, S. 9 (w.e.f. 18-1-2010).

265\. Subs. by Act 5 of 1929, S. 6, for to a person under a legal disability .

266\. The words or to a dependant , repealed by Act 7 of 1924, S. 3 and Sch. II.

267\. Cl. (b) omitted by Act 5 of 1929, S. 6.

268\. Subs. by Act 5 of 1929, S. 6 for to a person under any legal disability .

269\. The words or to any dependant repealed by Act 7 of 1924, S. 3 and Sch. II.

270\. Subs. by Act 7 of 1924, S. 2 and Sch. I, for or may make such order .

271\. Subs. for workmen by Act 45 of 2009, S. 5 (w.e.f. 18-1-2010).

272\. Ins. by Act 8 of 1959, S. 15 (w.e.f. 1-6-1959).

273\. Subs. for workman by Act 45 of 2009, S. 5 (w.e.f. 18-1-2010).

274\. Subs. for three hundred rupees by Act 11 of 2017, S. 4 (w.e.f. 15-5-2017).

275\. Added by Act 15 of 1933, S. 17.

276\. Subs. by Act 30 of 1995 (w.e.f. 15-9-1995).

277\. Omitted by Act 11 of 2017, S. 5 (w.e.f. 15-5-2017). Prior to omission it read as: 30-A. Withholding of certain payments pending decision of appeal. Where an employer makes an appeal under clause (a) of sub-section (1) of Section 30, the Commissioner may, and if so directed by the High Court shall, pending the decision of the appeal, withhold payment of any sum in deposit with him.

278\. The words G.-G. in C. successively subs. by the A. O. 1937 and the A. O. 1950, to read as above.

279\. Subs. for workman by Act 45 of 2009, S. 5 (w.e.f. 18-1-2010).

280\. Subs. for workman by Act 45 of 2009, S. 5 (w.e.f. 18-1-2010).

281\. The words and at the end of cl. (h) and the original cl. (i) omitted by the A. O. 1937.

282\. The original cl. (i) omitted by the A. O. 1937.

283\. Cls. (a) to (f) of S. 33 after being relettered as cls. (i) to (n) respectively were added to S. 32 and the rest of S. 33 was omitted by the A. O. 1937.

284\. The word and ins. by the A. O. 1937, omitted by Act 58 of 1960, S. 3 and Sch. II.

285\. Ins. by Act 8 of 1959, S. 16 (w.e.f. 1-6-1959).

286\. Ins. by Act 4 of 1986 (w.e.f. 15-5-1986).

287\. Subs. by the A. O. 1937 for Ss. 32 and 33 .

288\. The words and figures or S. 33 omitted by the A. O. 1937.

289\. The words the Gazette of India or omitted by the A. O. 1937.

290\. The words as the case may be omitted by the A. O. 1937.

291\. Ins. by Act 15 of 1933, S. 20.

292\. S. 35 renumbered as sub-section (1) of that section by Act 7 of 1937, S. 2.

293\. The words and letter to any Part B State or omitted by Act 3 of 1951, S. 3 and Sch.

294\. Subs. by Act 22 of 1984, S. 5 (w.e.f. 1-7-1984).

295\. Subs. by Act 7 of 1937, S. 2, for paid to .

296\. Subs. by Act 7 of 1937, S. 2, for for the benefit of .

297\. Subs. by Act 22 of 1984, S. 5 (w.e.f. 1-7-1984).

298\. Ins. by Act 7 of 1937, S. 2.

299\. Subs. by Act 3 of 1951, S. 3 and Sch. for a Part A State or Part C State .

300\. Subs. by Act 7 of 1937, S. 2, for awarded .

301\. Subs. for workmen's by Act 45 of 2009, S. 5 (w.e.f. 18-1-2010).

302\. The words and letter in any Part B State omitted by Act 3 of 1951, S. 3 and Sch.

303\. The word or omitted by Act 36 of 1957, S. 3 and Sch. II.

304\. Subs. by Act 22 of 1984, S. 5 (w.e.f. 1-7-1984).

305\. Subs. by Act 7 of 1937, S. 2, for and applicable for the benefit of .

306\. Subs. by Act 3 of 1951, S. 3 and Sch. for a Part A State or Part C State .

307\. Added by Act 7 of 1937, S. 2.

308\. Added by Act 64 of 1962.

309\. Subs. by Act 65 of 1976 (w.e.f. 21-5-1976).

310\. Subs. by Act 8 of 1959, S. 17, for the original Sch. (w.e.f. 1-6-1959).

311\. Subs. by Act 64 of 1962.

312\. A new heading inserted and entries at Serial Nos. 7 to 54 renumbered as Serial Nos. 1 to 48 by Act 64 of 1962.

313\. Subs. by Act 30 of 1995, S. 14 (w.e.f. 15-9-1995).

314\. Subs. by Act 30 of 1995, S. 14 (w.e.f. 15-9-1995).

315\. Subs. by Act 30 of 1995, S. 14 (w.e.f. 15-9-1995).

316\. Subs. by Act 30 of 1995, S. 14 (w.e.f. 15-9-1995).

317\. Ins. by Act 30 of 1995, S. 14 (w.e.f. 15-9-1995).

318\. Subs. by Act 30 of 1995, S. 14 (w.e.f. 15-9-1995).

319\. Subs. by Act 30 of 1995, S. 14 (w.e.f. 15-9-1995).

320\. Subs. by Act 30 of 1995, S. 14 (w.e.f. 15-9-1995).

321\. Subs. by Act 30 of 1995, S. 14 (w.e.f. 15-9-1995).

322\. Subs. by Act 30 of 1995, S. 14 (w.e.f. 15-9-1995).

323\. Subs. by Act 30 of 1995, S. 14 (w.e.f. 15-9-1995).

324\. Subs. by Act 30 of 1995, S. 14 (w.e.f. 15-9-1995).

325\. Subs. by Act 30 of 1995, S. 14 (w.e.f. 15-9-1995).

326\. Subs. by Act 30 of 1995, S. 14 (w.e.f. 15-9-1995).

327\. Ins. by Act 30 of 1995, S. 14 (w.e.f. 15-9-1995).

328\. Added by Act 58 of 1960, S. 3 and Sch. II.

329\. Subs. for Section 2(1)(n) by Act 45 of 2009, S. 10 (w.e.f. 18-1-2010).

330\. Subs. for Section 2(1)(n) by Act 45 of 2009, S. 10 (w.e.f. 18-1-2010).

331\. Subs. for workmen by Act 45 of 2009, S. 5 (w.e.f. 18-1-2010).

332\. Subs. for workmen by Act 45 of 2009, S. 5 (w.e.f. 18-1-2010).

333\. Subs. for Section 2(1)(n) by Act 45 of 2009, S. 10 (w.e.f. 18-1-2010).

334\. Subs. by Act 15 of 1933, S. 21, for the former cls. (i) to (xiii).

335\. Subs. by Act 8 of 1959, S. 18, for cls. (i) to (ix)(w.e.f. 1-6-1959).

336\. Subs. for employed, otherwise than in a clerical capacity or on a railway by Act 45 of 2009, S. 10 (w.e.f. 18-1-2010).

337\. Ins. by Act 30 of 1995, S. 15 (w.e.f. 15-9-1995).

338\. The words otherwise than in a clerical capacity omitted by Act 45 of 2009, S. 10 (w.e.f. 18-1-2010).

339\. Added by Act 64 of 1962.

340\. The words wherein or within the precincts whereof twenty or more persons are so employed omitted by Act 45 of 2009, S. 10 (w.e.f. 18-1-2010).

341\. The word or omitted by Act 64 of 1962.

342\. Added by Act 64 of 1962.

343\. The words other than clerical work omitted by Act 45 of 2009, S. 10 (w.e.f. 18-1-2010).

344\. Clause (b) omitted by Act 45 of 2009, S. 10 (w.e.f. 18-1-2010). Prior to omission it read as: (b) any ship not included in sub-clause (a) of twenty-five tons net tonnage or over; or

345\. The words or sub-clause (b) omitted by Act 45 of 2009, S. 10 (w.e.f. 18-1-2010).

346\. Subs. by Act 30 of 1995, S. 15 (w.e.f. 15-9-1995).

347\. The words otherwise than in a clerical capacity omitted by Act 45 of 2009, S. 10 (w.e.f. 18-1-2010).

348\. Subs. by Act 30 of 1995, S. 15 (w.e.f. 15-9-1995).

349\. Ins. by Act 8 of 1959, S. 18 (w.e.f. 1-6-1959).

350\. The words otherwise than in a clerical capacity omitted by Act 45 of 2009, S. 10 (w.e.f. 18-1-2010).

351\. The words in which on any one day of the preceding twelve months more than twenty-five persons have been employed omitted by Act 45 of 2009, S. 10 (w.e.f. 18-1-2010).

352\. Subs. by Act 8 of 1959, S. 18, for twenty (w.e.f. 1-6-1959).

353\. Subs. by Act 45 of 2009, S. 10 (w.e.f. 18-1-2010).

354\. Subs. by Act 30 of 1995, S. 15 (w.e.f. 15-9-1995).

355\. The words otherwise than in a clerical capacity omitted by Act 45 of 2009, S. 10 (w.e.f. 18-1-2010).

356\. Ins. by Act 9 of 1938, S. 11.

357\. Original clause (xxiii) re-numbered (xxv) by Act 9 of 1938, S. 11.

358\. Ins. by Act 9 of 1938, S. 11.

359\. The words and in which on any one day of the preceding twelve months ten or more persons have been so employed omitted by Act 45 of 2009, S. 10 (w.e.f. 18-1-2010).

360\. The words in which on any one day of the preceding twelve months fifty or more persons have been so employed omitted by Act 45 of 2009, S. 10 (w.e.f. 18-1-2010).

361\. Ins. by Act 8 of 1959, S. 18 (w.e.f. 1-6-1959).

362\. Subs. by Act 30 of 1995, S. 15 (w.e.f. 15-9-1995).

363\. The words otherwise than in a clerical capacity omitted by Act 45 of 2009, S. 10 (w.e.f. 18-1-2010).

364\. Ins. by Act 30 of 1995, S. 15 (w.e.f. 15-9-1995).

365\. The words in which on any one day of the preceding twelve months more than twenty-five persons have been employed omitted by Act 45 of 2009, S. 10 (w.e.f. 18-1-2010).

366\. The words in which on any one day of the preceding twelve months more than twenty-five persons have been employed omitted by Act 45 of 2009, S. 10 (w.e.f. 18-1-2010).

367\. Ins. by G.S.R. 381, dt. 3-11-1987.

368\. Omitted by Act 45 of 2009, S. 10 (w.e.f. 18-1-2010). Prior to omission it read as: Explanation. In this Schedule, the preceding twelve months relates in any particular case to the twelve months ending with the day on which the accident in such case occurred.

369\. Subs. by Act 22 of 1984, S. 6 (w.e.f. 1-7-1984).

370\. Entries at Serial Nos. 25 to 27 ins. by Act 30 of 1995, S. 16 (w.e.f. 15-9-1995).

371\. Ins. by S.O. 2615, dt. 15-9-1987.

372\. Subs. by Act 22 of 1984, S. 7 (w.e.f. 1-7-1984).

373\. Subs. for workman by Act 45 of 2009, S. 5 (w.e.f. 18-1-2010).

374\. Subs. for workman by Act 45 of 2009, S. 5 (w.e.f. 18-1-2010).
